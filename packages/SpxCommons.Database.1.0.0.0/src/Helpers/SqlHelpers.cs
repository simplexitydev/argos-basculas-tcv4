﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using SpxCommons.Collections.Extensions;

namespace SpxCommons.Database.Helpers
{
    /// <summary>
    /// Sql Helpers.
    /// </summary>
    public static class SqlHelpers
    {
        /// <summary>
        /// Escape an SQL sequence.
        /// </summary>
        /// <param name="sql">SQL string to escape.</param>
        /// <returns>An SQL escaped string.</returns>
        public static string EscapeSql(string sql)
        {
            return sql.Replace("'", "''");
        }

        /// <summary>
        /// Execute a (non-query) stored procedure on a given SqlConnection.
        /// </summary>
        /// <param name="sqlConnection">The connection to the database service.</param>
        /// <param name="procedureName">The stored procedure name.</param>
        /// <param name="parameters">Parameters of the stored procedure as a dictionary. The dictionary is from parameter name to value.</param>
        /// <returns>Returns integer result returned by the stored procedure.</returns>
        public static int ExecStoredProcedure(SqlConnection sqlConnection, string procedureName, IDictionary<string, object> parameters)
        {
            SqlCommand sqlCommand = new SqlCommand(procedureName, sqlConnection);
            sqlCommand.CommandType = CommandType.StoredProcedure;

            foreach (KeyValuePair<string, object> param in parameters)
            {
                sqlCommand.Parameters.AddWithValue(param.Key, param.Value);
            }

            return sqlCommand.ExecuteNonQuery();
        }

        /// <summary>
        /// Insert a record in a table.
        /// </summary>
        /// <param name="sqlConnection">The connection to the database service.</param>
        /// <param name="tableName">The name of the table.</param>
        /// <param name="record">Fields of the stored procedure as a dictionary. The dictionary is from field name to value.</param>
        /// <returns>Returns integer result returned by the sql insert statement.</returns>
        public static int InsertRecord(SqlConnection sqlConnection, string tableName, IDictionary<string, object> record)
        {
            return InsertRecord(sqlConnection, tableName, record.Keys, record.Values);
        }

        /// <summary>
        /// Insert a record in a table.
        /// </summary>
        /// <param name="sqlConnection">The connection to the database service.</param>
        /// <param name="tableName">The name of the table.</param>
        /// <param name="columnNames">Fields to be inserted into.</param>
        /// <param name="columnValues">Values to be inserted. The columnValues[i] is assigned to column which name is columnNames[i].</param>
        /// <returns>Returns integer result returned by the sql insert statement.</returns>
        private static int InsertRecord(SqlConnection sqlConnection, string tableName, IEnumerable<string> columnNames, IEnumerable<object> columnValues)
        {
            IEnumerable<string> colNames = columnNames as string[] ?? columnNames.ToArray();
            IEnumerable<object> colValues = columnValues as object[] ?? columnValues.ToArray();

            if(colNames.Count() != colValues.Count())
                throw new ArgumentException();

            IEnumerable<string> paramNames = colNames.Select(s => "@" + s).ToArray();
            IDictionary<string, Type> columnTypes = GetTableColumnTypes(sqlConnection, tableName, colNames);

            string sqlString = "INSERT INTO "+ EscapeSql(tableName) + "(" + colNames.ToCsvString() + ") VALUES (" + paramNames.ToCsvString() + ")";
            SqlCommand sqlCommand = new SqlCommand(sqlString, sqlConnection);

            for(int i=0; i < paramNames.Count(); i++)
            {
                object paramValue = Convert.ChangeType(colValues.ElementAt(i), columnTypes[colNames.ElementAt(i)]);
                sqlCommand.Parameters.AddWithValue(paramNames.ElementAt(i), paramValue);
            }

            return sqlCommand.ExecuteNonQuery();
        }

        /// <summary>
        /// Get column types of from a table.
        /// </summary>
        /// <param name="sqlConnection">The connection to the database service.</param>
        /// <param name="tableName">The name of the table.</param>
        /// <param name="columnNames">Column to be queried.</param>
        /// <returns>A dictionary which matches each column name to its type. If any of the given names is not found then an ArgumentException is thrown.</returns>
        public static IDictionary<string, Type> GetTableColumnTypes(SqlConnection sqlConnection, string tableName, IEnumerable<string> columnNames = null)
        {
            IEnumerable<string> colNames = columnNames  != null ?  (columnNames as string[] ?? columnNames.ToArray()) : null;
            IDictionary<string, Type> columnTypes = new Dictionary<string, Type>();
            string sqlQuery;

            if (columnNames == null)
                sqlQuery = "SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName";
            else
                sqlQuery =
                    "SELECT COLUMN_NAME, DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = @tableName AND COLUMN_NAME IN " + colNames.Select(EscapeSql).ToCsvString("', '", "('", "')");

            SqlCommand sqlCommand = new SqlCommand(sqlQuery, sqlConnection);
            sqlCommand.Parameters.AddWithValue("@tableName", tableName);
            SqlDataReader dataReader = sqlCommand.ExecuteReader();

            while (dataReader.Read())
            {
                columnTypes.Add((string) dataReader["COLUMN_NAME"], SqlTypeNameToType((string) dataReader["DATA_TYPE"]));
            }
            dataReader.Close();

            if(columnNames != null && columnTypes.Count != colNames.Count())
                throw new ArgumentException();

            return columnTypes;
        }

        /// <summary>
        /// Gets a Type given its SQL name.
        /// </summary>
        /// <param name="type">Type SQL name.</param>
        /// <returns>The type which matches the given name. If no type is found, an ArgumentException is thrown. </returns>
        public static Type SqlTypeNameToType(string type)
        {
            string[] tokens = type.Split(new char[] { '(', ')' }, StringSplitOptions.RemoveEmptyEntries);
            string typeFamily = tokens[0].ToLowerInvariant();
            string size = tokens.Length > 1 ? tokens[1] : string.Empty;

            switch (typeFamily)
            {
                case "bigint":
                    return typeof(long);
                case "binary":
                    return size == "1" ? typeof(byte) : typeof(byte[]);
                case "bit":
                    return typeof(bool);
                case "char":
                    return size == "1" ? typeof(char) : typeof(string);
                case "datetime":
                    return typeof(DateTime);
                case "decimal":
                    return typeof(decimal);
                case "float":
                    return typeof(double);
                case "image":
                    return typeof(byte[]);
                case "int":
                    return typeof(int);
                case "money":
                    return typeof(double);
                case "nchar":
                    return size == "1" ? typeof(char) : typeof(string);
                case "ntext":
                    return typeof(string);
                case "nvarchar":
                    return typeof(string);
                case "real":
                    return typeof(float);
                case "uniqueidentifier":
                    return typeof(Guid);
                case "smalldatetime":
                    return typeof(DateTime);
                case "smallint":
                    return typeof(short);
                case "smallmoney":
                    return typeof(float);
                case "sql_variant":
                    return typeof(string);
                case "text":
                    return typeof(string);
                case "timestamp":
                    return typeof(TimeSpan);
                case "tinyint":
                    return typeof(byte);
                case "varbinary":
                    return typeof(byte[]);
                case "varchar":
                    return typeof(string);
                case "variant":
                    return typeof(string);
                case "xml":
                    return typeof(string);
                default:
                    throw new ArgumentException(string.Format("There is no .Net type specified for mapping T-SQL type '{0}'.", typeFamily));
            }
        }

        /// <summary>
        /// Calls a function using a cured sql connection. 
        /// In other words, it makes sure to have a valid sql connection before calling a function passing this sql connection as parameter.
        /// </summary>
        /// <typeparam name="T">Return type of the function to be called.</typeparam>
        /// <param name="sqlConnection">The sql connection to be cured.</param>
        /// <param name="sqlConnectionString">The sql connection string to be used if the current connection is invalid.</param>
        /// <param name="function">Function to be called after curing the sql connection.</param>
        /// <returns>Whatever the called function returns.</returns>
        public static T CureSqlConnection<T>(SqlConnection sqlConnection, string sqlConnectionString, Func<SqlConnection, T> function)
        {
            if (sqlConnection == null || (sqlConnection.State != ConnectionState.Open && sqlConnection.State != ConnectionState.Closed))
            {
                using (sqlConnection = new SqlConnection(sqlConnectionString))
                {
                    sqlConnection.Open();
                    return function(sqlConnection);
                }
            }
            else if (sqlConnection.State == ConnectionState.Closed)
            {
                try
                {
                    sqlConnection.Open();
                    return function(sqlConnection);
                }
                finally
                {
                    sqlConnection.Close();
                }

            }
            else //if (conexionSql.State == ConnectionState.Open)
            {
                return function(sqlConnection);
            }
        }

        /// <summary>
        /// Calls an action using a cured sql connection. 
        /// In other words, it makes sure to have a valid sql connection before calling an action passing this sql connection as parameter.
        /// </summary>
        /// <param name="sqlConnection">The sql connection to be cured.</param>
        /// <param name="sqlConnectionString">The sql connection string to be used if the current connection is invalid.</param>
        /// <param name="action">Action to be called after curing the sql connection.</param>
        public static void CureSqlConnection(SqlConnection sqlConnection, string sqlConnectionString, Action<SqlConnection> action)
        {
            CureSqlConnection(sqlConnection, sqlConnectionString,
                curatedSqlConnection =>
                {
                    action(curatedSqlConnection);
                    return 0;
                });
        }
    }
}
