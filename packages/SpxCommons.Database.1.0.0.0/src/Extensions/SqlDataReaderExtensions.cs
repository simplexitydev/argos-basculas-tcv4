﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace SpxCommons.Database.Extensions
{
    /// <summary>
    /// SqlDataReader Extensions
    /// </summary>
    public static class SqlDataReaderExtensions
    {
        /// <summary>
        /// Reads the current 'row' in a SqlDataReader and transforms it into a dictionary of column names into column values.
        /// </summary>
        /// <param name="dataReader">Data reader to be used.</param>
        /// <param name="replaceDbNulls">True if DbNull elements should be replaced by 'plain' null value.</param>
        /// <param name="closeDataReader">True if the data reader should be closed after using it.</param>
        /// <returns>A dictionary of column names into column values.</returns>
        public static IDictionary<string, object> ToDictionary(this SqlDataReader dataReader, bool replaceDbNulls = true, bool closeDataReader = false)
        {
            IDictionary<string, object> dictionary = new Dictionary<string, object>(dataReader.FieldCount);

            for (int i = 0; i < dataReader.FieldCount; i++)
            {
                object value = dataReader.GetValue(i);
                if (replaceDbNulls && Convert.IsDBNull(value)) value = null;
                
                dictionary.Add(dataReader.GetName(i), value);
            }

            if(closeDataReader) dataReader.Close();

            return dictionary;
        }

        /// <summary>
        /// Reads a SqlDataReader and transforms it into an enumerable of dictionaries of column names into column values.
        /// </summary>
        /// <param name="dataReader">Data reader to be used.</param>
        /// <param name="replaceDbNulls">True if DbNull elements should be replaced by 'plain' null value.</param>
        /// <param name="closeDataReader">True if the data reader should be closed after using it.</param>
        /// <returns>An enumerable of dictionaries of column names into column values.</returns>
        public static IEnumerable<IDictionary<string, object>> ToDictionaryEnumerable(this SqlDataReader dataReader, bool replaceDbNulls = true, bool closeDataReader = true)
        {
            while (dataReader.Read())
                yield return dataReader.ToDictionary(replaceDbNulls);

            if (closeDataReader) dataReader.Close();
        }
    }
}
