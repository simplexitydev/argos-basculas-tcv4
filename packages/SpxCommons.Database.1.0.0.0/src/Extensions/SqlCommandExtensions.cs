﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace SpxCommons.Database.Extensions
{
    /// <summary>
    /// SqlCommand Extensions.
    /// </summary>
    public static class SqlCommandExtensions
    {
        /// <summary>
        /// Fill parameters of an sql command using a dictionary.
        /// </summary>
        /// <param name="sqlCommand">The sql command.</param>
        /// <param name="parameterValues">Dictionary mapping parameter names (without '@') to its values.</param>
        public static void FillParameters(this SqlCommand sqlCommand, IDictionary<string, object> parameterValues)
        {
            foreach (SqlParameter sqlParameter in sqlCommand.Parameters)
            {
                sqlParameter.Value = parameterValues[sqlParameter.ParameterName.Substring(1)] ?? DBNull.Value;
            }
        }

        /// <summary>
        /// Derive every '@' starting word as a parameter.
        /// </summary>
        /// <param name="sqlCommand">The sql command.</param>
        public static void DeriveParameters(this SqlCommand sqlCommand)
        {
            foreach (Match ocurrencia in Regex.Matches(sqlCommand.CommandText, @"@\w+"))
            {
                if (!sqlCommand.Parameters.Contains(ocurrencia.Value))
                {
                    SqlParameter sqlParameter = new SqlParameter {ParameterName = ocurrencia.Value};
                    sqlCommand.Parameters.Add(sqlParameter);
                }
            }
        }
    }
}
