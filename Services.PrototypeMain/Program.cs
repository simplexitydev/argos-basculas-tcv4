﻿using System;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;

namespace Services.PrototypeMain
{
  internal class Program
  {
    private const string HostName = "10.7.1.69";
    private const int Port = 4001;
    private const int InputStreamBufferSize = 128;
    private const int ConnectionRetryDelay = 1000;

    private static void Main(string[] args)
    {
      var buffer = new byte[InputStreamBufferSize];
      TcpClient client = null;

      IObservable<Stream> streams = Observable.Defer(() =>
        {
          if (client != null) client.Close();
          client = new TcpClient();

          Func<string, int, IObservable<Unit>> connectFactory =
            Observable.FromAsyncPattern<string, int>(client.BeginConnect, client.EndConnect);
          return connectFactory(HostName, Port).Select(_ => client.GetStream());
        });

      IObservable<byte[]> rawMessages = streams
        .Select(stream => Observable.FromAsyncPattern<byte[], int, int, int>(stream.BeginRead, stream.EndRead))
        .SelectMany(readFactory => Observable.Defer(() => readFactory(buffer, 0, buffer.Length)).Repeat())
        .Retry(TimeSpan.FromMilliseconds(ConnectionRetryDelay))
        .TakeWhile(length => length != 0)
        .Repeat()
        .SelectMany(buffer.Take)
        .WindowUntil<byte>(0x0D)
        .SelectMany(Observable.ToArray);

      rawMessages.Subscribe(
        data => Console.WriteLine(Encoding.ASCII.GetString(data)),
        Console.WriteLine,
        () => Console.WriteLine("OnCompleted"));


      Console.ReadKey();
    }
  }

  public static class ObservableExtensions
  {
    public static IObservable<IObservable<T>> WindowUntil<T>(this IObservable<T> source, T separator)
      where T : IEquatable<T>
    {
      IObservable<T> src = source.Publish().RefCount();
      return src.Window(src.Where(it => it.Equals(separator)));
    }

    public static IObservable<T> Retry<T>(this IObservable<T> source, TimeSpan delay)
    {
      return source
        .Catch<T, Exception>(ex => Observable.Empty<T>().Delay(delay).Concat(Observable.Throw<T>(ex)))
        .Retry();
    }
  }
}

// ALTERNATIVE APPROACH
//Observable.Using(() => new TcpClient(), client =>
//{
//    Func<string, int, IObservable<Unit>> connectFactory = Observable.FromAsyncPattern<string, int>(client.BeginConnect, client.EndConnect);

//    return connectFactory("localhost", 23)
//        .Select(_ => client.GetStream())
//        .Select(stream => Observable.FromAsyncPattern<byte[], int, int, int>(stream.BeginRead, stream.EndRead))
//        .SelectMany(readFactory => Observable.Defer(() => readFactory(buffer, 0, buffer.Length)).Repeat())
//        .TakeWhile(length => length != 0);
//})
//.Retry(TimeSpan.FromMilliseconds(ConnectionRetryDelay))
//.Repeat()
//.SelectMany(buffer.Take)
//.WindowUntil<byte>(0x20)
//.SelectMany(Observable.ToArray)
//.Repeat()