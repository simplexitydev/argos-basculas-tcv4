﻿var configAggregatorApp = angular.module('configAggregatorApp', ['ngRoute', 'configAggregatorControllers']);

configAggregatorApp.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider.
        when('/', {
            templateUrl: 'partials/machine-list.html',
            controller: 'MachineListController'
        }).
        when('/scaleConfigurations/new/:IpEndPoint', {
            templateUrl: 'partials/scale-edit.html',
            controller: 'ScaleNewController'
        }).
        when('/scaleConfigurations/edit/:scaleCode/:IpEndPoint', {
            templateUrl: 'partials/scale-edit.html',
            controller: 'ScaleEditController'
        }).
        when('/controllerMachines/:ipEndPoint', {
            templateUrl: 'partials/machine-detail.html',
            controller: 'MachineDetailController'
        }).
        otherwise({
            redirectTo: '/'
        });
    }]);