﻿var configAggregatorControllers = angular.module('configAggregatorControllers', []);

configAggregatorControllers.controller('MachineListController', ['$scope', '$http',
    function ($scope, $http) {
        $scope.MachineList = [];
        $scope.MachineListIsLoading = true;
        $scope.ErrorMessage = null;
        
        $http.get('/api/controllerMachines/').
            success(function (data) {
                $scope.MachineList = data;
                $scope.MachineListIsLoading = false;
            }).
            error(function () {
                $scope.ErrorMessage = 'No se pudo contactar al servicio central de configuración de básculas. Verifique la conexión y refresque esta página.';
            });
    }]);

configAggregatorControllers.controller('MachineDetailController', ['$scope', '$routeParams', '$http', '$window', '$location',
    function ($scope, $routeParams, $http, $window, $location) {
        $scope.IpEndPoint = $routeParams.ipEndPoint;
        $scope.ScaleList = [];
        $scope.ScaleListIsLoading = true;
        $scope.ImportBtn = { Class: 'btn-primary', IsLoading: false };
        $scope.ExportBtn = { Class: 'btn-primary', IsLoading: false };
        $scope.RestartBtn = { Class: 'btn-warning', IsLoading: false };
        $scope.ResponseStatuses = { Visible: false, Class: '', Statuses: [] };
        $scope.AddNonExistentScales = false;

        $scope.ShowDefaultCommunicationError = function() {
            $scope.ResponseStatuses = {
                Visible: true,
                Class: 'alert-danger',
                Statuses: [{ Key: 'Error:', Value: 'No se pudo contactar al servicio central de configuración de básculas.' }]
            };
        };

        $scope.LoadScaleList = function () {
            $scope.ScaleList = [];
            $scope.ScaleListIsLoading = true;
            
            $http.get('/api/scaleConfigurations/?machineIpEndPoint=' + $scope.IpEndPoint).
                success(function (data) {
                    $scope.ScaleList = data;
                    $scope.ScaleListIsLoading = false;
                }).
                error(function () {
                    $scope.ScaleList = [];
                    $scope.ScaleListIsLoading = false;
                    $scope.ShowDefaultCommunicationError();
                });
        };
        $scope.LoadScaleList();
        
        $scope.ImportControllerMachineConfiguration = function (ipEndPoint, addNonExistentScales) {
            $scope.ImportBtn.Class = 'btn-info';
            $scope.ExportBtn.Class = 'btn-primary';
            $scope.RestartBtn.Class = 'btn-warning';
            $scope.ResponseStatuses.Visible = false;
            $scope.ImportBtn.IsLoading = true;
            
            $http.get('/api/controllerMachineConfiguration/' + ipEndPoint + '?addNonExistentScales=' + addNonExistentScales).
                success(function (data) {
                    $scope.ImportBtn.Class = 'btn-success';
                    $scope.ImportBtn.IsLoading = false;
                    
                    $scope.ResponseStatuses = {
                        Visible: true,
                        Class: 'alert-success',
                        Statuses: data
                    };
                    
                    $scope.LoadScaleList();
                }).
                error(function () {
                    $scope.ImportBtn.Class = 'btn-danger';
                    $scope.ImportBtn.IsLoading = false;
                    
                    $scope.ShowDefaultCommunicationError();
                });
        };

        $scope.ExportControllerMachineConfiguration = function (ipEndPoint) {
            $scope.ImportBtn.Class = 'btn-primary';
            $scope.ExportBtn.Class = 'btn-info';
            $scope.RestartBtn.Class = 'btn-warning';
            $scope.ResponseStatuses.Visible = false;
            $scope.ExportBtn.IsLoading = true;
                       
            $http.put('/api/controllerMachineConfiguration/' + ipEndPoint).
                success(function (data) {
                    if (data[0].Key === $scope.IpEndPoint && data[0].Value === 'OK.') {
                        $scope.ExportBtn.Class = 'btn-success';
                        $scope.ExportBtn.IsLoading = false;
                    } else {
                        $scope.ExportBtn.Class = 'btn-danger';
                        $scope.ExportBtn.IsLoading = false;
                        $scope.ResponseStatuses = {
                            Visible: true,
                            Class: 'alert-danger',
                            Statuses: data
                        };
                    }
                    $scope.ButtonLoadingImage = false;
                }).
                error(function () {
                    $scope.ExportBtn.Class = 'btn-danger';
                    $scope.ExportBtn.IsLoading = false;
                    $scope.ShowDefaultCommunicationError();
                });
        };

        $scope.RestartControllerMachine = function(ipEndPoint) {
            if (confirm('Está seguro de reiniciar el servicio de basculación en '+ ipEndPoint +'?')) {
                $scope.ImportBtn.Class = 'btn-primary';
                $scope.ExportBtn.Class = 'btn-primary';
                $scope.RestartBtn.Class = 'btn-info';
                $scope.ResponseStatuses.Visible = false;
                $scope.RestartBtn.IsLoading = true;
                
                $http.post('/api/restartRequest/' + ipEndPoint).
                    success(function (data) {
                        if (data[0].Key === $scope.IpEndPoint && data[0].Value === 'OK.') {
                            $scope.RestartBtn.Class = 'btn-success';
                            $scope.RestartBtn.IsLoading = false;
                        } else {
                            $scope.RestartBtn.Class = 'btn-danger';
                            $scope.RestartBtn.IsLoading = false;
                            $scope.ResponseStatuses = {
                                Visible: true,
                                Class: 'alert-danger',
                                Statuses: data
                            };
                        }
                    }).
                    error(function () {
                        $scope.RestartBtn.Class = 'btn-danger';
                        $scope.RestartBtn.IsLoading = false;
                        $scope.ShowDefaultCommunicationError();
                    });
            }
        };

        $scope.DeleteScale = function(scaleCode) {
            if (confirm('Está seguro de eliminar la báscula ' + scaleCode + '?')) {
                $scope.ImportBtn.Class = 'btn-primary';
                $scope.ExportBtn.Class = 'btn-primary';
                $scope.RestartBtn.Class = 'btn-warning';
                $scope.ResponseStatuses.Visible = false;
                $scope.ScaleListIsLoading = true;
                
                $http.delete('/api/scaleConfigurations/' + scaleCode)
                    .success(function () {
                        $scope.ScaleListIsLoading = false;
                        $scope.LoadScaleList();
                    })
                    .error(function () {
                        $scope.ScaleListIsLoading = false;
                        $scope.ResponseStatuses = {
                            Visible: true,
                            Class: 'alert-danger',
                            Statuses: [{ Key: 'Error:', Value: 'No se pudo contactar al servicio central de configuración de básculas.' }]
                        };
                    });
            }
        };
        
        $scope.go = function (path) {
            $location.path(path);
        };
        
        $scope.goBack = function () {
            $window.history.back();
        };
    }]);

configAggregatorControllers.controller('ScaleEditController', ['$scope', '$routeParams', '$http', '$window', '$location',
    function ($scope, $routeParams, $http, $window, $location) {
        $scope.PanelTitle = 'Editar Báscula';
        $scope.CodeEditDisabled = true;
        $scope.IsLoading = true;
        $scope.ShowForm = false;
        $scope.ErrorMessage = null;
        $scope.IpEndPoint = $routeParams.IpEndPoint;
        
        $http.get("/api/scaleConfigurations/" + $routeParams.scaleCode)
            .success(function (data) {
                $scope.IsLoading = false;
                data.CommunicationMode = data.CommunicationMode != null ? data.CommunicationMode.toLowerCase() : null;
                data.SerialConfiguration.PortName = data.SerialConfiguration.PortName != null ? data.SerialConfiguration.PortName.toUpperCase() : null;
                data.SerialConfiguration.StopBits = data.SerialConfiguration.StopBits != null ? data.SerialConfiguration.StopBits.toLowerCase() : null;
                data.SerialConfiguration.Parity = data.SerialConfiguration.Parity != null ? data.SerialConfiguration.Parity.toLowerCase() : null;
                $scope.Scale = data;
                $scope.ShowForm = true;
            })
            .error(function () {
                $scope.IsLoading = false;
                
                if (status == 404) {
                    $scope.ErrorMessage = 'No se pudo encontrar la báscula solicitada';
                }
                else {
                    $scope.ErrorMessage = 'No se pudo contactar al servicio central de configuración de básculas. Verifique la conexión y refresque esta página.';
                }
            });
        
        $scope.Save = function (scale, ipEndPoint) {
            $scope.IsLoading = true;
            $scope.ShowForm = false;
            $scope.ErrorMessage = null;
            
            $http.put("/api/scaleConfigurations/", scale)
                .success(function (data) {
                    $scope.go('/controllerMachines/' + ipEndPoint);
                })
                .error(function () {
                    $scope.IsLoading = false;
                    $scope.ShowForm = true;
                    $scope.ErrorMessage = 'No se pudo contactar al servicio central de configuración de básculas. Verifique la conexión y reintente.';
                });
        };
        
        $scope.go = function (path) {
            $location.path(path);
        };
        
        $scope.goBack = function () {
            $window.history.back();
        };
    }]);

configAggregatorControllers.controller('ScaleNewController', ['$scope', '$routeParams', '$http', '$window', '$location',
    function ($scope, $routeParams, $http, $window, $location) {
        $scope.PanelTitle = 'Nueva Báscula';
        $scope.CodeEditDisabled = false;
        $scope.IsLoading = false;
        $scope.ShowForm = true;
        $scope.IpEndPoint = $routeParams.IpEndPoint;

        $scope.Scale = {
            Enabled: true, 
            WeightPrecision: 0, 
            CommunicationMode: 'socket',
            SerialConfiguration: {
                PortName: 'COM1',
                BaudRate: 9600,
                DataBits: 8,
                StopBits: 'none',
                Parity: 'none'
            },
            SocketConfiguration: {
                Port: 0
            }
        };

        $scope.Save = function (scale, ipEndPoint) {
            $scope.IsLoading = true;
            $scope.ShowForm = false;
            $scope.ErrorMessage = null;
            scale.ControllerMachineIpEndPoint = ipEndPoint;

            $http.put("/api/scaleConfigurations/", scale)
                .success(function(data) {
                    $scope.go('/controllerMachines/' + ipEndPoint);
                })
                .error(function() {
                    $scope.IsLoading = false;
                    $scope.ShowForm = true;
                    $scope.ErrorMessage = 'No se pudo contactar al servicio central de configuración de básculas. Verifique la conexión y reintente.';
                });
        };
        
        $scope.go = function (path) {
            $location.path(path);
        };
        
        $scope.goBack = function () {
            $window.history.back();
        };
    }]);