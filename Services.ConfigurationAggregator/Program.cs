﻿using System;
using System.Data.Entity;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using Services.ConfigurationAggregator.Services;
using Services.ConfigurationAggregator.Services.DbContexts;
using Services.ConfigurationAggregator.Services.Models;
using Topshelf;
using AutoMapper;

namespace Services.ConfigurationAggregator
{
    /// <summary>
    /// Main Class
    /// </summary>
    class Program
    {
        /// <summary>
        /// Main Program
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //EntityFramework configuration.
            Database.SetInitializer<ScaleConfigurationContext>(null);

            //AutoMapper configuration.
            Mapper.CreateMap<ScaleConfiguration, ScaleConfiguration>();

            //Windows service configuration and start.
            HostFactory.Run(hostConfig =>
            {
                hostConfig.Service<WinService>(configuradorServicio =>
                {
                    configuradorServicio.ConstructUsing(_ => new WinService());
                    configuradorServicio.WhenStarted(instance => instance.Start());
                    configuradorServicio.WhenStopped(instance => instance.Stop());
                });

                hostConfig.RunAsLocalSystem();

                hostConfig.SetDescription("Simplexity Scale Configuration Aggregator");
                hostConfig.SetDisplayName("Simplexity Scale Configuration Aggregator");
                hostConfig.SetServiceName("SpxScaleConfigAggregator");
            });
        }
    }

    /// <summary>
    /// Windows Service
    /// </summary>
    class WinService
    {
        /// <summary>
        /// Web Service Host
        /// </summary>
        private WebServiceHost _host;

        /// <summary>
        /// Windows Service Start
        /// </summary>
        public void Start()
        {
            _host = new WebServiceHost(typeof(ConfigurationAggregatorService), new Uri("http://localhost:" + Properties.Settings.Default.Port + "/"));
            ServiceEndpoint apisep = _host.AddServiceEndpoint(typeof(IConfigurationAggregatorService), new WebHttpBinding(), "api");
            ServiceEndpoint wwwsep = _host.AddServiceEndpoint(typeof(IStaticFileService), new WebHttpBinding(), "www");

            // Make JSON the default response format. Also, make XML/JSON format selection automatic.
            WebHttpBehavior whb = apisep.Behaviors.Find<WebHttpBehavior>();
            if(whb != null)
            {
                whb.AutomaticFormatSelectionEnabled = true;
                whb.DefaultOutgoingResponseFormat = WebMessageFormat.Json;
            }
            else
            {
                whb = new WebHttpBehavior {
                    AutomaticFormatSelectionEnabled = true
                    , DefaultOutgoingResponseFormat = WebMessageFormat.Json
                };
                apisep.Behaviors.Add(whb);
            }

            _host.Open();
        }

        /// <summary>
        /// Windows Service Stop
        /// </summary>
        public void Stop()
        {
            _host.Close();
        }
    }
}
