﻿using System.Data.Entity;
using Services.ConfigurationAggregator.Properties;
using Services.ConfigurationAggregator.Services.Models;

namespace Services.ConfigurationAggregator.Services.DbContexts
{
    public class ScaleConfigurationContext : System.Data.Entity.DbContext
    {
        /// <summary>
        /// Scale configurations repository.
        /// </summary>
        public DbSet<ScaleConfiguration> ScaleConfigurations { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ScaleConfigurationContext() : base("Name = " + Settings.Default.DbContextSourceName)
        {
        }

        /// <summary>
        /// ORM Setup
        /// </summary>
        /// <param name="modelBuilder">Model builder instance.</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //SCALES CONFIGURATION
            modelBuilder.Properties()
                .Where(prop => prop.DeclaringType == typeof(ScaleConfiguration))
                .Configure(propConfig => propConfig.HasColumnName("Sca" + propConfig.ClrPropertyInfo.Name));

            modelBuilder.Properties()
                .Where(prop => prop.DeclaringType == typeof(SerialConfiguration))
                .Configure(propConfig => propConfig.HasColumnName("ScaSerial" + propConfig.ClrPropertyInfo.Name));

            modelBuilder.Properties()
                .Where(prop => prop.DeclaringType == typeof(SocketConfiguration))
                .Configure(propConfig => propConfig.HasColumnName("ScaSocket" + propConfig.ClrPropertyInfo.Name));

            modelBuilder.Entity<ScaleConfiguration>()
                .ToTable("AdScales")
                .HasKey(s => s.Code);

            modelBuilder.Entity<ScaleConfiguration>()
                .Property(s => s.ControllerMachineIpEndPoint).HasColumnName("ScaRequestIP");

            //SUBSIDIARIES
            modelBuilder.Entity<Subsidiary>()
                .ToTable("AdSubsidiaries")
                .HasKey(s => s.Code);
            
            modelBuilder.Entity<Subsidiary>()
                .Property(s => s.Code).HasColumnName("SubCode");

            modelBuilder.Entity<Subsidiary>()
                .Property(s => s.Name).HasColumnName("SubName");

            modelBuilder.Entity<Subsidiary>()
                .HasMany(s => s.Scales)
                .WithMany(s => s.Subsidiaries)
                .Map(m => {
                    m.ToTable("AdScaleSubsidiary");
                    m.MapLeftKey("SasSubsidiary_SubCode");
                    m.MapRightKey("SasScale_ScaCode");
                });
        }
    }
}
