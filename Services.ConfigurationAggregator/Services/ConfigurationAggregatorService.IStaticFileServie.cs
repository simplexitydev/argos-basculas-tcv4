﻿using System.IO;
using System.ServiceModel.Web;

namespace Services.ConfigurationAggregator.Services
{
    public partial class ConfigurationAggregatorService : IStaticFileService
    {
        [WebInvoke(Method = "GET", BodyStyle = WebMessageBodyStyle.Bare, UriTemplate = "/{*content}")]
        public Stream StaticContent(string content)
        {
            OutgoingWebResponseContext response = WebOperationContext.Current.OutgoingResponse;
            string path = "wui/" + (string.IsNullOrEmpty(content) ? "index.html" : content);
            string extension = Path.GetExtension(path);
            string contentType = string.Empty;

            switch (extension)
            {
                case ".htm":
                case ".html":
                    contentType = "text/html";
                    break;
                case ".gif":
                    contentType = "image/gif";
                    break;
                case ".jpg":
                    contentType = "image/jpeg";
                    break;
                case ".png":
                    contentType = "image/png";
                    break;
                case ".js":
                    contentType = "application/javascript";
                    break;
                case ".css":
                    contentType = "text/css";
                    break;
            }

            if (File.Exists(path) && !string.IsNullOrEmpty(contentType))
            {
                response.ContentType = contentType;
                response.StatusCode = System.Net.HttpStatusCode.OK;
                return File.Open(path, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            }
            else
            {
                response.StatusCode = System.Net.HttpStatusCode.NotFound;
                return null;
            }
        }
    }
}
