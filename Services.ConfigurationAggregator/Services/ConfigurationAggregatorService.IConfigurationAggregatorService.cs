﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Globalization;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel.Web;
using System.Threading.Tasks;
using Common.Logging;
using ScaleService;
using Services.ConfigurationAggregator.Services.DbContexts;
using Services.ConfigurationAggregator.Services.Models;
using Services.ConfigurationAggregator.Services.Models.DataTransferObjects;
using AutoMapper;
using System.Data.Entity.Validation;

namespace Services.ConfigurationAggregator.Services
{
    public partial class ConfigurationAggregatorService : IConfigurationAggregatorService
    {
        /// <summary>
        /// Logger
        /// </summary>
        private readonly ILog _logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// Entity framework repository.
        /// </summary>
        private readonly ScaleConfigurationContext _dbContext = new ScaleConfigurationContext();

        /// <summary>
        /// Make sure if current format is Xml then Content-Type HTTP Header has "application/xml" as value.
        /// </summary>
        private static void FixXmlResponseContentType()
        {
            if (WebOperationContext.Current != null && WebOperationContext.Current.OutgoingResponse.Format == WebMessageFormat.Xml)
                WebOperationContext.Current.OutgoingResponse.ContentType = "application/xml";
        }

        public IEnumerable<ScaleConfiguration> GetScaleConfigurations(string machineIpEndPoint)
        {
            _logger.Trace(l => l("GetScaleConfigurations (machineIpEndPoint = " + machineIpEndPoint + ")"));

            FixXmlResponseContentType();

            IEnumerable<ScaleConfiguration> scaleConfigurations;
            
            if (machineIpEndPoint == null)
                scaleConfigurations = _dbContext.ScaleConfigurations;
            else
                scaleConfigurations = _dbContext.ScaleConfigurations.Where(s => s.ControllerMachineIpEndPoint == machineIpEndPoint);

            return scaleConfigurations.ToArray().Select(Mapper.Map<ScaleConfiguration>);
        }

        public ScaleConfiguration GetScaleConfigurationsByCode(string code)
        {
            _logger.Trace(l => l("GetScaleConfigurationsByCode (code = " + code + ")"));

            FixXmlResponseContentType();

            ScaleConfiguration scaleConfiguration = _dbContext.ScaleConfigurations.Find(code);

            if(scaleConfiguration == null)
            {
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
                return null;
            }

            return Mapper.Map<ScaleConfiguration>(scaleConfiguration);
        }      

        public void SetScaleConfiguration(ScaleConfiguration scaleConfiguration)
        {
            _logger.Trace(l => l("SetScaleConfiguration (scaleConfiguration.Code = " + scaleConfiguration.Code + ")"));

            ScaleConfiguration originalScaleConfiguration = _dbContext.ScaleConfigurations.Find(scaleConfiguration.Code);

            if (originalScaleConfiguration != null)
                _dbContext.Entry(originalScaleConfiguration).CurrentValues.SetValues(scaleConfiguration);
            else
                _dbContext.ScaleConfigurations.Add(scaleConfiguration);

            try
            {
                _dbContext.SaveChanges();
            }
            catch(DbEntityValidationException ex)
            {
                _logger.Warn(ex.Message, ex);
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                else
                    throw;
            }
            catch(DbUpdateConcurrencyException ex)
            {
                _logger.Warn(ex.Message, ex);
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Conflict;
                else
                    throw;
            }
        }


        public void DeleteScaleConfiguration(string code)
        {
            _logger.Trace(l => l("DeleteScaleConfiguration (code = " + code + ")"));

            ScaleConfiguration scaleConfiguration = _dbContext.ScaleConfigurations.Find(code);
            if(scaleConfiguration == null)
            {
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.NotFound;
            }
            else
            {
                _dbContext.ScaleConfigurations.Remove(scaleConfiguration);
                _dbContext.SaveChanges();
            }
        }

        public IEnumerable<ControllerMachine> GetAllControllerMachines()
        {
            _logger.Trace(l => l("GetAllControllerMachines ()"));

            FixXmlResponseContentType();

            return _dbContext.ScaleConfigurations
                .Include(s => s.Subsidiaries)
                .Select(s => new {s.ControllerMachineIpEndPoint, s.Subsidiaries})
                .ToArray()
                .GroupBy(s => s.ControllerMachineIpEndPoint, s => s.Subsidiaries)
                .Select(s => new ControllerMachine(){
                    IpEndPoint = s.Key,
                    Subsidiaries = s.SelectMany(t => t).Distinct().Select(t => new Subsidiary(){Code = t.Code, Name = t.Name})
                });
        }

        public IDictionary<string, string> ImportControllerMachineConfiguration(string ipEndPoint, bool addNonExistentScales = false)
        {
            _logger.Trace(l => l("ImportControllerMachineConfiguration (ipEndPoint = " + ipEndPoint + ", addNonExistentScales = " + addNonExistentScales + ")"));

            if (ipEndPoint == null)
            {
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                else
                    throw new ArgumentNullException();
            }

            FixXmlResponseContentType();


            IDictionary<string, string> results = new Dictionary<string, string>();
            object resultsLock = new object();
            object dbContextLock = new object();

            IEnumerable<string> controllerMachines = ipEndPoint == "all" ? 
                _dbContext.ScaleConfigurations.Select(s => s.ControllerMachineIpEndPoint).Distinct().ToArray()
                : new[] { ipEndPoint };

            Parallel.ForEach(controllerMachines, machineIpEndPoint =>
            {
                ScaleServiceClient client = new ScaleServiceClient(machineIpEndPoint);
                ScaleDTO[] machineScales;

                try
                {
                    machineScales = client.GetScales();
                }
                catch (WebException ex)
                {
                    _logger.Warn(ex.Message, ex);
                    lock (resultsLock)
                    {
                        results.Add(machineIpEndPoint, machineIpEndPoint + " is unreachable.");
                    }
                    return;
                }

                foreach (ScaleDTO machineScale in machineScales)
                {
                    ScaleConfiguration scaleConfig;
                    
                    lock (dbContextLock)
                    {
                        scaleConfig = _dbContext.ScaleConfigurations.Find(machineScale.Code);
                    }

                    if (scaleConfig == null)
                    {
                        if(!addNonExistentScales)
                        {
                            lock (resultsLock)
                            {
                                results.Add(machineIpEndPoint + " -> " + machineScale.Code, "Scale " + machineScale.Code + " is not registered in the aggregator database and adding non-existant scales is disabled. This scale configuration will be ignored.");
                            }
                            continue;
                        }

                        scaleConfig = new ScaleConfiguration();
                        _dbContext.ScaleConfigurations.Add(scaleConfig);
                        scaleConfig.Code = machineScale.Code;
                        scaleConfig.Description = machineScale.Code;
                        scaleConfig.ControllerMachineIpEndPoint = ipEndPoint;
                    }
                    else if (scaleConfig.ControllerMachineIpEndPoint != machineIpEndPoint)
                    {
                        _logger.Warn(l => l("Machine " + machineIpEndPoint + " is not the controller machine of scale " + scaleConfig.Code + ". Machine " + scaleConfig.ControllerMachineIpEndPoint + " is. This scale configuration will be ignored."));
                        lock (resultsLock)
                        {
                            results.Add(machineIpEndPoint + " -> " + scaleConfig.Code, "Machine " + machineIpEndPoint + " is not the controller machine of scale " + scaleConfig.Code + ". Machine " + scaleConfig.ControllerMachineIpEndPoint + " is. This scale configuration will be ignored.");
                        }
                        continue;
                    }

                    scaleConfig.CommunicationMode = machineScale.ComunicationMode;
                    scaleConfig.Enabled = machineScale.Enabled;
                    scaleConfig.Protocol = machineScale.CurrentProtocol;
                    scaleConfig.WeightPrecision = machineScale.WeightVariation;

                    if (machineScale.SocketInfo != null)
                    {
                        scaleConfig.SocketConfiguration.Address = machineScale.SocketInfo.Address;
                        scaleConfig.SocketConfiguration.Port = int.Parse(machineScale.SocketInfo.Port);
                    }

                    if (machineScale.ComPortInfo != null)
                    {
                        scaleConfig.SerialConfiguration.PortName = machineScale.ComPortInfo.PortName;
                        scaleConfig.SerialConfiguration.BaudRate = (short)machineScale.ComPortInfo.BaudRate;
                        scaleConfig.SerialConfiguration.DataBits = (byte)machineScale.ComPortInfo.DataBits;
                        scaleConfig.SerialConfiguration.StopBits = machineScale.ComPortInfo.StopBits;
                        scaleConfig.SerialConfiguration.Parity = machineScale.ComPortInfo.Parity;
                    }

                    lock(resultsLock)
                    {
                        results.Add(machineIpEndPoint + " -> " + scaleConfig.Code, "OK.");
                    }
                }
            });

            try
            {
                _dbContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                _logger.Warn(ex.Message, ex);
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.Conflict;
                else
                    throw;
            }

            return results;
        }

        public IDictionary<string, string> ExportControllerMachineConfiguration(string ipEndPoint)
        {
            _logger.Trace(l => l("ExportControllerMachineConfiguration (ipEndPoint = " + ipEndPoint + ")"));

            if (ipEndPoint == null)
            {
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                else
                    throw new ArgumentNullException();
            }

            IDictionary<string, string> results = new Dictionary<string, string>();
            object resultsLock = new object();

            ILookup<string, ScaleConfiguration> scaleConfigsByControllerMachine = 
                (ipEndPoint == "all"? _dbContext.ScaleConfigurations : _dbContext.ScaleConfigurations.Where(s =>s.ControllerMachineIpEndPoint == ipEndPoint))
                .ToLookup(s => s.ControllerMachineIpEndPoint);

            Parallel.ForEach(scaleConfigsByControllerMachine, scaleConfigs =>
            {
                ICollection<ScaleDTO> machineScaleCollection = new List<ScaleDTO>(scaleConfigs.Count());

                foreach (ScaleConfiguration scaleConfig in scaleConfigs)
                {
                    ScaleDTO machineScale = new ScaleDTO();
                    machineScale.SocketInfo = new SocketDTO();
                    machineScale.ComPortInfo = new ComPortDTO();

                    machineScale.Code = scaleConfig.Code;
                    machineScale.NewCode = scaleConfig.Code;
                    machineScale.ComunicationMode = scaleConfig.CommunicationMode;
                    machineScale.Enabled = scaleConfig.Enabled;
                    machineScale.EnabledSpecified = true;
                    machineScale.CurrentProtocol = scaleConfig.Protocol;
                    machineScale.WeightVariation = scaleConfig.WeightPrecision;
                    machineScale.WeightVariationSpecified = true;

                    machineScale.SocketInfo.Address = scaleConfig.SocketConfiguration.Address;
                    machineScale.SocketInfo.Port = scaleConfig.SocketConfiguration.Port.GetValueOrDefault().ToString(CultureInfo.InvariantCulture);

                    machineScale.ComPortInfo.PortName = scaleConfig.SerialConfiguration.PortName;
                    machineScale.ComPortInfo.BaudRate = scaleConfig.SerialConfiguration.BaudRate.GetValueOrDefault();
                    machineScale.ComPortInfo.BaudRateSpecified = true;
                    machineScale.ComPortInfo.DataBits = scaleConfig.SerialConfiguration.DataBits.GetValueOrDefault();
                    machineScale.ComPortInfo.DataBitsSpecified = true;
                    machineScale.ComPortInfo.StopBits = scaleConfig.SerialConfiguration.StopBits;
                    machineScale.ComPortInfo.Parity = scaleConfig.SerialConfiguration.Parity;

                    machineScaleCollection.Add(machineScale);
                }

                ScaleServiceClient client = new ScaleServiceClient(scaleConfigs.Key);
                try
                {
                    client.SetScalesConfiguration(machineScaleCollection.ToArray());
                    lock (resultsLock)
                    {
                        results.Add(scaleConfigs.Key, "OK.");
                    }
                }
                catch (WebException ex)
                {
                    _logger.Warn(ex.Message, ex);
                    lock (resultsLock)
                    {
                        results.Add(scaleConfigs.Key, scaleConfigs.Key + " is unreachable.");
                    }
                }
                
            });

            return results;
        }

        public IDictionary<string, string> RestartControllerMachine(string ipEndPoint)
        {
            _logger.Trace(l => l("RestartControllerMachine (ipEndPoint = " + ipEndPoint + ")"));

            if (ipEndPoint == null)
            {
                if (WebOperationContext.Current != null)
                    WebOperationContext.Current.OutgoingResponse.StatusCode = HttpStatusCode.BadRequest;
                else
                    throw new ArgumentNullException();
            }

            IDictionary<string, string> results = new Dictionary<string, string>();
            object resultsLock = new object();

            IEnumerable<string> controllerMachines = ipEndPoint == "all" ?
                _dbContext.ScaleConfigurations.Select(s => s.ControllerMachineIpEndPoint).Distinct().ToArray()
                : new[] { ipEndPoint };
            

            Parallel.ForEach(controllerMachines, machineIpEndPoint =>
            {
                ScaleServiceClient client = new ScaleServiceClient(machineIpEndPoint);
                try
                {
                    client.Restart();
                    lock (resultsLock)
                    {
                        results.Add(machineIpEndPoint, "OK.");
                    }
                }
                catch (WebException ex)
                {
                    _logger.Warn(ex.Message, ex);
                    lock (resultsLock)
                    {
                        results.Add(machineIpEndPoint, machineIpEndPoint + " is unreachable.");
                    }
                }
            });

            return results;
        }
    }
}
