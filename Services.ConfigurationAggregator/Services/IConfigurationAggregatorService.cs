﻿using System.Collections.Generic;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using Services.ConfigurationAggregator.Services.Models;
using Services.ConfigurationAggregator.Services.Models.DataTransferObjects;

namespace Services.ConfigurationAggregator.Services
{
    /// <summary>
    /// Configuration aggregator service that concentrates all configuration data from scales.
    /// </summary>
    [ServiceContract]
    public interface IConfigurationAggregatorService
    {
        /// <summary>
        /// Get configurations for scales in associated with a given machine ip end point.
        /// </summary>
        /// <param name="machineIpEndPoint">Machine ip end point, or null if all scales should be returned.</param>
        /// <returns>An enumerable of scale configurations</returns>
        [OperationContract]
        [WebGet(UriTemplate = "scaleConfigurations/?machineIpEndPoint={machineIpEndPoint}")]
        IEnumerable<ScaleConfiguration> GetScaleConfigurations(string machineIpEndPoint);

        /// <summary>
        /// Get configuration for a single scale.
        /// </summary>
        /// <param name="code">Code of the scale to get.</param>
        /// <returns>The configuration for the requested scale.</returns>
        [OperationContract]
        [WebGet(UriTemplate = "scaleConfigurations/{code}")]
        ScaleConfiguration GetScaleConfigurationsByCode(string code);

        /// <summary>
        /// Add or update a scale configuration.
        /// </summary>
        /// <param name="scaleConfiguration">Scale configuration to add/update.</param>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "scaleConfigurations/")]
        void SetScaleConfiguration(ScaleConfiguration scaleConfiguration);

        /// <summary>
        /// Delete an scale configurarion.
        /// </summary>
        /// <param name="code">Code of the scale to delete.</param>
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "scaleConfigurations/{code}")]
        void DeleteScaleConfiguration(string code);

        /// <summary>
        /// Get an enumerable of controller machine ip end points.
        /// </summary>
        /// <returns>An enumerable of controller machine ip end points.</returns>
        [OperationContract]
        [WebGet(UriTemplate = "controllerMachines/")]
        IEnumerable<ControllerMachine> GetAllControllerMachines();

        /// <summary>
        /// Get configuration from a remote controller machine and store it on the aggregator.
        /// </summary>
        /// <param name="ipEndPoint">Controller machine ip end point.</param>
        /// <param name="addNonExistentScales">When true and importing scales non-existent in the aggregator database, these scales are added. Otherwise, these scales are ignored.</param>
        /// <returns>Dictionary mapping machines ip end points and scale codes TO operation result.</returns>
        [OperationContract]
        [WebGet(UriTemplate = "controllerMachineConfiguration/{ipEndPoint}?addNonExistentScales={addNonExistentScales}")]
        IDictionary<string, string> ImportControllerMachineConfiguration(string ipEndPoint, bool addNonExistentScales = false);

        /// <summary>
        /// Set configuration on a remote controller machine using the aggregator data.
        /// </summary>
        /// <param name="ipEndPoint">Controller machine ip end point.</param>
        /// <returns>Dictionary mapping machines ip end points TO operation result.</returns>
        [OperationContract]
        [WebInvoke(Method = "PUT", UriTemplate = "controllerMachineConfiguration/{ipEndPoint}")]
        IDictionary<string, string> ExportControllerMachineConfiguration(string ipEndPoint);


        /// <summary>
        /// Request a machine restart.
        /// </summary>
        /// <param name="ipEndPoint">Controller machine ip end point.</param>
        /// <returns>Dictionary mapping machines ip end points TO operation result.</returns>
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "restartRequest/{ipEndPoint}")]
        IDictionary<string, string> RestartControllerMachine(string ipEndPoint);
    }
}
