﻿using System.IO;
using System.ServiceModel;

namespace Services.ConfigurationAggregator.Services
{
    /// <summary>
    /// HTTP static file web server interface.
    /// </summary>
    [ServiceContract]
    public interface IStaticFileService
    {
        [OperationContract]
        Stream StaticContent(string content);
    }
}
