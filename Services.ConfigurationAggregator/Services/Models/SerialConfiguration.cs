﻿using System.ComponentModel;

namespace Services.ConfigurationAggregator.Services.Models
{
    public class SerialConfiguration
    {
        public string PortName { get; set; }

        [DefaultValue(9600)]
        public int? BaudRate { get; set; }

        [DefaultValue(8)]
        public byte? DataBits { get; set; }

        [DefaultValue("None")]
        public string StopBits { get; set; }

        [DefaultValue("None")]
        public string Parity { get; set; }
    }
}
