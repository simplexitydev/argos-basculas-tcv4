﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Services.ConfigurationAggregator.Services.Models
{
    public class ScaleConfiguration
    {
        public ScaleConfiguration()
        {
            SocketConfiguration = new SocketConfiguration();
            SerialConfiguration = new SerialConfiguration();
        }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Enabled { get; set; }
        public string ControllerMachineIpEndPoint { get; set; }
        public string Protocol { get; set; }
        public double WeightPrecision { get; set; }
        public string CommunicationMode { get; set; }
        public SocketConfiguration SocketConfiguration { get; set; }
        public SerialConfiguration SerialConfiguration { get; set; }

        [IgnoreDataMember]
        public virtual ICollection<Subsidiary> Subsidiaries { get; set; }
    }
}
