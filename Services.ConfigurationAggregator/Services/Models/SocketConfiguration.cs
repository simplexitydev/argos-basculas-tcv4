﻿namespace Services.ConfigurationAggregator.Services.Models
{
    public class SocketConfiguration
    {
        public string Address { get; set; }
        public int? Port { get; set; }
    }
}
