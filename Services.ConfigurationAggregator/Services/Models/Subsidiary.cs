﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Services.ConfigurationAggregator.Services.Models
{
    /// <summary>
    /// Subsidiary
    /// </summary>
    public class Subsidiary
    {
        /// <summary>
        /// Subsidiary Code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Subsidiary Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Subsidiary Scales
        /// </summary>
        [IgnoreDataMember]
        public virtual ICollection<ScaleConfiguration> Scales { get; set; }
    }
}
