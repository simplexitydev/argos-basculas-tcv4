﻿using System.Collections.Generic;

namespace Services.ConfigurationAggregator.Services.Models.DataTransferObjects
{
    public class ControllerMachine
    {
        public string IpEndPoint { get; set; }
        public IEnumerable<Subsidiary> Subsidiaries { get; set; }
    }
}
