using System.IO.Ports;

namespace Services.ConfigurationAggregator.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    /// <summary>
    /// Migration to add/remove scale configuration data columns to AdScales table.
    /// </summary>
    public partial class AddConfigurationToScalesTable : DbMigration
    {
        private const string _scalesTable = "AdScales";

        public override void Up()
        {
            AddColumn(_scalesTable, "ScaEnabled", c => c.Boolean(nullable: false, defaultValue: false));
            AddColumn(_scalesTable, "ScaProtocol", c => c.String());
            AddColumn(_scalesTable, "ScaWeightPrecision", c => c.Double(nullable: false, defaultValue: 0));
            AddColumn(_scalesTable, "ScaCommunicationMode", c => c.String(maxLength: 8));

            AddColumn(_scalesTable, "ScaSerialPortName", c => c.String(maxLength: 8));
            AddColumn(_scalesTable, "ScaSerialBaudRate", c => c.Int(defaultValue: 9600));
            AddColumn(_scalesTable, "ScaSerialDataBits", c => c.Byte(defaultValue: 8));
            AddColumn(_scalesTable, "ScaSerialStopBits", c => c.String(maxLength: 12, defaultValue: "None"));
            AddColumn(_scalesTable, "ScaSerialParity", c => c.String(maxLength: 4, defaultValue: "None"));

            AddColumn(_scalesTable, "ScaSocketAddress", c => c.String(maxLength:64));
            AddColumn(_scalesTable, "ScaSocketPort", c => c.Int(defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn(_scalesTable, "ScaEnabled");
            DropColumn(_scalesTable, "ScaProtocol");
            DropColumn(_scalesTable, "ScaWeightPrecision");
            DropColumn(_scalesTable, "ScaCommunicationMode");

            DropColumn(_scalesTable, "ScaSerialPortName");
            DropColumn(_scalesTable, "ScaSerialBaudRate");
            DropColumn(_scalesTable, "ScaSerialDataBits");
            DropColumn(_scalesTable, "ScaSerialStopBits");
            DropColumn(_scalesTable, "ScaSerialParity");

            DropColumn(_scalesTable, "ScaSocketAddress");
            DropColumn(_scalesTable, "ScaSocketPort");
        }
    }
}
