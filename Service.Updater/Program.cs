﻿using System.ServiceProcess;

namespace Simplexity.Scales.Service.Updater
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main()
    {

#if DEBUG

      // Debug code: this allows the process to run as a non-service.
      // It will kick off the service start point, but never kill it.
      // Shut down the debugger to exit

      var mainService = new UpdaterService();
      mainService.StartService();

      ////Put a breakpoint on the following line to always catch your service when it has finished its work
      System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);

#else
      var servicesToRun = new ServiceBase[] 
        { 
          new UpdaterService() 
        };
      ServiceBase.Run(servicesToRun);
#endif

    }
  }
}
