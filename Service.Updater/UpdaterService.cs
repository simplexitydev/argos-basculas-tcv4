﻿using System;
using System.IO;
using System.Reflection;
using System.ServiceProcess;
using wyDay.Controls;
using System.Timers;
using Timer = System.Timers.Timer;

namespace Simplexity.Scales.Service.Updater
{
  public partial class UpdaterService : ServiceBase
  {
    static AutomaticUpdaterBackend auBackend;
    private Timer _timer;
    private int _interval;

    public UpdaterService()
    {
      InitializeComponent();
    }

    protected override void OnStart(string[] args)
    {
      StartService();
    }

    public void StartService()
    {
      auBackend = new AutomaticUpdaterBackend
      {
        //TODO: set a unique string.
        // For instance, "appname-companyname"
        GUID = "Simplexity-Scales-Updater",

        // With UpdateType set to Automatic, you're still in
        // charge of checking for updates, but the
        // AutomaticUpdaterBackend continues with the
        // downloading and extracting automatically.
        UpdateType = UpdateType.Automatic,

        // We set the service name that will be used by wyUpdate
        // to restart this service on update success or failure.
        ServiceName = this.ServiceName
        //ServiceName = "Simplexity.Scales.Service"
      };

      auBackend.ReadyToBeInstalled += auBackend_ReadyToBeInstalled;
      auBackend.UpdateSuccessful += auBackend_UpdateSuccessful;
      auBackend.UpToDate += auBackend_UpToDate;
      auBackend.UpdateFailed += auBackend_UpdateFailed;
      auBackend.BeforeChecking += auBackend_BeforeChecking;
      auBackend.BeforeDownloading += auBackend_BeforeDownloading;
      auBackend.BeforeExtracting += auBackend_BeforeExtracting;
      auBackend.Cancelled += auBackend_Cancelled;
      auBackend.CheckingFailed += auBackend_CheckingFailed;
      auBackend.ClosingAborted += auBackend_ClosingAborted;
      auBackend.DownloadingFailed += auBackend_DownloadingFailed;
      auBackend.ExtractingFailed += auBackend_ExtractingFailed;
      auBackend.UpdateAvailable += auBackend_UpdateAvailable;
      auBackend.UpdateStepMismatch += auBackend_UpdateStepMismatch;

      //TODO: use the failed events for logging & error reporting:
      // CheckingFailed, DownloadingFailed, ExtractingFailed, UpdateFailed

      // Initialize() and AppLoaded() must be called after events have been set.
      // Note: If there's a pending update to be installed, wyUpdate will be
      //       started, then it will talk back and say "ready to install,
      //       you can close now" at which point your app will be closed.
      auBackend.Initialize();
      auBackend.AppLoaded();


      _interval = 300000;

      _timer = new Timer();
      _timer.Elapsed += TimerElapsed;
      _timer.Interval = _interval;
      _timer.Enabled = true;
      _timer.AutoReset = true;
    
    }

    private void TimerElapsed(object sender, ElapsedEventArgs e)
    {
       _timer.Enabled = false;
      CheckForUpdates();
      //Task.Factory.StartNew(CheckForUpdates);
       _timer.Enabled = true;

    }

    protected override void OnStop()
    {

    }

    static void CheckForUpdates()
    {
      WriteToLog("/**********************************************/", true);
      WriteToLog("Checking for updates", true);
      var result = false;

      if (!auBackend.ClosingForInstall && auBackend.UpdateStepOn == UpdateStepOn.Nothing)
      {
        result = auBackend.ForceCheckForUpdate();
      }
    }

    void auBackend_UpdateStepMismatch(object sender, EventArgs e)
    {
      WriteToLog("auBackend_UpdateStepMismatch", true);
    }

    void auBackend_UpdateAvailable(object sender, EventArgs e)
    {
      WriteToLog("auBackend_UpdateAvailable", true);
    }

    void auBackend_ExtractingFailed(object sender, FailArgs e)
    {
      WriteToLog("auBackend_ExtractingFailed. Reason\r\nTitle: " + e.ErrorTitle + "\r\nMessage: " + e.ErrorMessage, true);
    }

    void auBackend_DownloadingFailed(object sender, FailArgs e)
    {
      WriteToLog("auBackend_DownloadingFailed. Reason\r\nTitle: " + e.ErrorTitle + "\r\nMessage: " + e.ErrorMessage, true);
    }

    void auBackend_ClosingAborted(object sender, EventArgs e)
    {
      WriteToLog("auBackend_ClosingAborted", true);
    }

    void auBackend_CheckingFailed(object sender, FailArgs e)
    {
      WriteToLog("auBackend_CheckingFailed. Reason\r\nTitle: " + e.ErrorTitle + "\r\nMessage: " + e.ErrorMessage, true);
    }

    void auBackend_Cancelled(object sender, EventArgs e)
    {
      WriteToLog("auBackend_Cancelled", true);
    }

    void auBackend_BeforeExtracting(object sender, BeforeArgs e)
    {
      WriteToLog("auBackend_BeforeExtracting", true);
    }

    void auBackend_BeforeDownloading(object sender, BeforeArgs e)
    {
      WriteToLog("auBackend_BeforeDownloading", true);
    }

    void auBackend_BeforeChecking(object sender, BeforeArgs e)
    {
      WriteToLog("auBackend_BeforeChecking", true);
    }

    static void auBackend_UpdateFailed(object sender, FailArgs e)
    {
      //TODO: Notify the admin, or however you want to handle the failure
      WriteToLog("Update failed. Reason\r\nTitle: " + e.ErrorTitle + "\r\nMessage: " + e.ErrorMessage, true);
    }

    static void auBackend_UpToDate(object sender, SuccessArgs e)
    {
      WriteToLog("Already up-to-date!", true);
    }

    static void auBackend_UpdateSuccessful(object sender, SuccessArgs e)
    {
      WriteToLog("Successfully updated to version " + e.Version, true);
      WriteToLog("Changes: ", true);
      WriteToLog(auBackend.Changes, true);
    }

    static void auBackend_ReadyToBeInstalled(object sender, EventArgs e)
    {
      WriteToLog("auBackend_ReadyToBeInstalled, auBackend.UpdateStepOn = " + UpdateStepOn.UpdateReadyToInstall, true);

      // ReadyToBeInstalled event is called when either the UpdateStepOn == UpdateDownloaded or UpdateReadyToInstall
      if (auBackend.UpdateStepOn == UpdateStepOn.UpdateReadyToInstall)
      {
        //TODO: Delay the installation of the update until it's appropriate for your app.

        //TODO: Do any "spin-down" operations. auBackend.InstallNow() will
        //      exit this process using Environment.Exit(0), so run
        //      cleanup functions now (close threads, close running programs, release locked files, etc.)

        // here we'll just close immediately to install the new version
        WriteToLog("About to install the new version.", true);
        auBackend.InstallNow();
      }
    }

    static void WriteToLog(string message, bool append)
    {
      var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
      var dateTime = DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss");

      using (var outfile = new StreamWriter(path + @"\UpdaterService.txt", append))
      {
        outfile.WriteLine(dateTime + ": " + message);
      }
    }

  }
}
