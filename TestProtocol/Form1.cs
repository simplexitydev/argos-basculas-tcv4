﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Simplexity.Scales.Domain.Main.Communication;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Protocols;

namespace TestProtocol
{
  public partial class Form1 : Form
  {
    private CommunicationPort _communicationPort;
    delegate void SetTextCallback(TextBox textBox, string text);
    private readonly IDecryptable _protocol;

    public Form1()
    {
      _protocol = new ToledoInd310();
      _communicationPort = new CommunicationPort();
      InitializeComponent();
    }

    private void Form1Load(object sender, EventArgs e)
    {
      InicializarControl();
    }
    private void InicializarControl()
    {

      var serialPorts = SerialPort.GetPortNames();

      if (serialPorts.Any())
      {
        cbxComName.DataSource = SerialPort.GetPortNames();
        cbxComName.SelectedItem = cbxComName.Items[0];
      }


      cbxBaudRate.SelectedItem = cbxBaudRate.Items[5];
      cbxDataSize.SelectedItem = cbxDataSize.Items[0];

      cbxStopBits.DataSource = Enum.GetValues(typeof(StopBits)).Cast<StopBits>().Select(v => v.ToString()).ToArray();
      cbxStopBits.SelectedItem = cbxStopBits.Items[0];
      cbxParity.DataSource = Enum.GetValues(typeof(Parity)).Cast<Parity>().Select(v => v.ToString()).ToArray();
      cbxParity.SelectedItem = cbxParity.Items[0];
      var listProtocols = new List<string>();
      var decryptTypes = TypesImplementingInterface(typeof(IDecryptable));
      listProtocols.AddRange(decryptTypes.Where(IsRealClass).Select(type => type.Name));
      cbxProtocol.DataSource = listProtocols;
    }

    private IEnumerable<Type> TypesImplementingInterface(Type desiredType)
    {
      return AppDomain
             .CurrentDomain
             .GetAssemblies()
             .SelectMany(assembly => assembly.GetTypes())
             .Where(desiredType.IsAssignableFrom);
    }

    private bool IsRealClass(Type testType)
    {
      return testType.IsAbstract == false
           && testType.IsGenericTypeDefinition == false
           && testType.IsInterface == false;
    }

    private void BtnOpenClick(object sender, EventArgs e)
    {
      if (btnOpen != null)
        switch (btnOpen.Text)
        {
          case "Open":
            btnOpen.Text = "Close";
            if (_communicationPort.IsOpen()) return;
            _communicationPort = new CommunicationPort
                                   {
                                     PortName = cbxComName.SelectedItem.ToString(),
                                     BaudRate = Convert.ToInt32((cbxBaudRate.SelectedItem.ToString())),
                                     DataBits = Convert.ToInt32((cbxDataSize.SelectedItem.ToString())),
                                     StopBits = cbxStopBits.SelectedItem.ToString(),
                                     Parity = cbxParity.SelectedItem.ToString(),
                                     CurrentTransmissionType = ComConstants.TransmissionType.Hex,
                                     //MaxDataLength = 20,
                                     EndByte = _protocol.EndByte
                                   };
            _communicationPort.FrameReceived += CommunicationPortFrameReceived;
            _communicationPort.Connect();
            break;
          case "Close":
            btnOpen.Text = "Open";
            if (!_communicationPort.IsOpen()) return;
            _communicationPort.Close();
            break;
        }
    }
    
    private void CommunicationPortFrameReceived(byte[] frame)
    {
      SetText(textReceivedData, Encoding.ASCII.GetString(frame));
      var decrypFrame = _protocol.Decrypt(frame);
      if(decrypFrame == null)
      {
        SetText(textReceivedData, "Trama No valida");
        return;
      }
      var mensaje = string.Format("Weigth={0}  Units= {1} State= {2} Mode= {3}",
                                  decrypFrame.Weight,
                                  decrypFrame.Unit,
                                  decrypFrame.State,
                                  decrypFrame.Mode
                                  );
      SetText(textReceivedData, mensaje);
    }

    private void SetText(TextBox textBox, string text)
    {
      // InvokeRequired required compares the thread ID of the
      // calling thread to the thread ID of the creating thread.
      // If these threads are different, it returns true.
      if (textBox.InvokeRequired)
      {
        var d = new SetTextCallback(SetText);
        this.Invoke(d, new object[] { textBox, text });
      }
      else
      {
        textBox.Text += text;
      }
    }

    private void BtnClearClick(object sender, EventArgs e)
    {
      textReceivedData.Text = String.Empty;
    }
  }
}
