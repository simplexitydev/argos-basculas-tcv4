﻿namespace TestProtocol
{
  partial class Form1
  {
    /// <summary>
    /// Variable del diseñador requerida.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Limpiar los recursos que se estén utilizando.
    /// </summary>
    /// <param name="disposing">true si los recursos administrados se deben eliminar; false en caso contrario.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Código generado por el Diseñador de Windows Forms

    /// <summary>
    /// Método necesario para admitir el Diseñador. No se puede modificar
    /// el contenido del método con el editor de código.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.btnClear = new System.Windows.Forms.Button();
      this.textReceivedData = new System.Windows.Forms.TextBox();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.groupBoxSerial = new System.Windows.Forms.GroupBox();
      this.cbxProtocol = new System.Windows.Forms.ComboBox();
      this.label6 = new System.Windows.Forms.Label();
      this.cbxParity = new System.Windows.Forms.ComboBox();
      this.cbxStopBits = new System.Windows.Forms.ComboBox();
      this.btnOpen = new System.Windows.Forms.Button();
      this.cbxDataSize = new System.Windows.Forms.ComboBox();
      this.cbxBaudRate = new System.Windows.Forms.ComboBox();
      this.cbxComName = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox4.SuspendLayout();
      this.groupBox1.SuspendLayout();
      this.groupBoxSerial.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.btnClear);
      this.groupBox4.Controls.Add(this.textReceivedData);
      this.groupBox4.Location = new System.Drawing.Point(313, 12);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(447, 274);
      this.groupBox4.TabIndex = 3;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Received Data";
      // 
      // btnClear
      // 
      this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnClear.Location = new System.Drawing.Point(144, 236);
      this.btnClear.Name = "btnClear";
      this.btnClear.Size = new System.Drawing.Size(154, 26);
      this.btnClear.TabIndex = 21;
      this.btnClear.Text = "Clear";
      this.btnClear.UseVisualStyleBackColor = true;
      this.btnClear.Click += new System.EventHandler(this.BtnClearClick);
      // 
      // textReceivedData
      // 
      this.textReceivedData.Location = new System.Drawing.Point(7, 29);
      this.textReceivedData.Multiline = true;
      this.textReceivedData.Name = "textReceivedData";
      this.textReceivedData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.textReceivedData.Size = new System.Drawing.Size(434, 201);
      this.textReceivedData.TabIndex = 0;
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.groupBoxSerial);
      this.groupBox1.Location = new System.Drawing.Point(12, 12);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(283, 274);
      this.groupBox1.TabIndex = 2;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Config";
      // 
      // groupBoxSerial
      // 
      this.groupBoxSerial.Controls.Add(this.cbxProtocol);
      this.groupBoxSerial.Controls.Add(this.label6);
      this.groupBoxSerial.Controls.Add(this.cbxParity);
      this.groupBoxSerial.Controls.Add(this.cbxStopBits);
      this.groupBoxSerial.Controls.Add(this.btnOpen);
      this.groupBoxSerial.Controls.Add(this.cbxDataSize);
      this.groupBoxSerial.Controls.Add(this.cbxBaudRate);
      this.groupBoxSerial.Controls.Add(this.cbxComName);
      this.groupBoxSerial.Controls.Add(this.label2);
      this.groupBoxSerial.Controls.Add(this.label5);
      this.groupBoxSerial.Controls.Add(this.label4);
      this.groupBoxSerial.Controls.Add(this.label3);
      this.groupBoxSerial.Controls.Add(this.label1);
      this.groupBoxSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBoxSerial.Location = new System.Drawing.Point(19, 19);
      this.groupBoxSerial.Name = "groupBoxSerial";
      this.groupBoxSerial.Size = new System.Drawing.Size(246, 249);
      this.groupBoxSerial.TabIndex = 0;
      this.groupBoxSerial.TabStop = false;
      this.groupBoxSerial.Text = "Serial";
      // 
      // cbxProtocol
      // 
      this.cbxProtocol.FormattingEnabled = true;
      this.cbxProtocol.Location = new System.Drawing.Point(119, 169);
      this.cbxProtocol.Name = "cbxProtocol";
      this.cbxProtocol.Size = new System.Drawing.Size(113, 24);
      this.cbxProtocol.TabIndex = 19;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label6.Location = new System.Drawing.Point(14, 172);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(58, 16);
      this.label6.TabIndex = 18;
      this.label6.Text = "Protocol";
      // 
      // cbxParity
      // 
      this.cbxParity.FormattingEnabled = true;
      this.cbxParity.Location = new System.Drawing.Point(119, 139);
      this.cbxParity.Name = "cbxParity";
      this.cbxParity.Size = new System.Drawing.Size(113, 24);
      this.cbxParity.TabIndex = 15;
      // 
      // cbxStopBits
      // 
      this.cbxStopBits.FormattingEnabled = true;
      this.cbxStopBits.Location = new System.Drawing.Point(119, 112);
      this.cbxStopBits.Name = "cbxStopBits";
      this.cbxStopBits.Size = new System.Drawing.Size(113, 24);
      this.cbxStopBits.TabIndex = 14;
      // 
      // btnOpen
      // 
      this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnOpen.Location = new System.Drawing.Point(64, 217);
      this.btnOpen.Name = "btnOpen";
      this.btnOpen.Size = new System.Drawing.Size(113, 26);
      this.btnOpen.TabIndex = 17;
      this.btnOpen.Text = "Open";
      this.btnOpen.UseVisualStyleBackColor = true;
      this.btnOpen.Click += new System.EventHandler(this.BtnOpenClick);
      // 
      // cbxDataSize
      // 
      this.cbxDataSize.FormattingEnabled = true;
      this.cbxDataSize.Items.AddRange(new object[] {
            "7",
            "8"});
      this.cbxDataSize.Location = new System.Drawing.Point(119, 85);
      this.cbxDataSize.Name = "cbxDataSize";
      this.cbxDataSize.Size = new System.Drawing.Size(113, 24);
      this.cbxDataSize.TabIndex = 13;
      // 
      // cbxBaudRate
      // 
      this.cbxBaudRate.FormattingEnabled = true;
      this.cbxBaudRate.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
      this.cbxBaudRate.Location = new System.Drawing.Point(119, 58);
      this.cbxBaudRate.Name = "cbxBaudRate";
      this.cbxBaudRate.Size = new System.Drawing.Size(113, 24);
      this.cbxBaudRate.TabIndex = 12;
      // 
      // cbxComName
      // 
      this.cbxComName.FormattingEnabled = true;
      this.cbxComName.Location = new System.Drawing.Point(119, 31);
      this.cbxComName.Name = "cbxComName";
      this.cbxComName.Size = new System.Drawing.Size(113, 24);
      this.cbxComName.TabIndex = 11;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(14, 61);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(72, 16);
      this.label2.TabIndex = 10;
      this.label2.Text = "Baud Rate";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.Location = new System.Drawing.Point(14, 142);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(42, 16);
      this.label5.TabIndex = 9;
      this.label5.Text = "Parity";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(14, 115);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(54, 16);
      this.label4.TabIndex = 7;
      this.label4.Text = "Bit Stop";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(14, 88);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(66, 16);
      this.label3.TabIndex = 5;
      this.label3.Text = "Data Size";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(14, 34);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(73, 16);
      this.label1.TabIndex = 1;
      this.label1.Text = "ComName";
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(763, 327);
      this.Controls.Add(this.groupBox4);
      this.Controls.Add(this.groupBox1);
      this.Name = "Form1";
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.Form1Load);
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.groupBox1.ResumeLayout(false);
      this.groupBoxSerial.ResumeLayout(false);
      this.groupBoxSerial.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.Button btnClear;
    private System.Windows.Forms.TextBox textReceivedData;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBoxSerial;
    private System.Windows.Forms.ComboBox cbxParity;
    private System.Windows.Forms.ComboBox cbxStopBits;
    private System.Windows.Forms.Button btnOpen;
    private System.Windows.Forms.ComboBox cbxDataSize;
    private System.Windows.Forms.ComboBox cbxBaudRate;
    private System.Windows.Forms.ComboBox cbxComName;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.ComboBox cbxProtocol;
    private System.Windows.Forms.Label label6;
  }
}

