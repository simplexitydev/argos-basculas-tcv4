﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using ScaleService;
using ScaleWebTest.Properties;
using SpxCommons.Database.Extensions;
using System.Linq;
using System.Web;

namespace ScaleWebTest.Controllers
{
    public class HomeController : Controller
    {
        private const string _getRegisteredScalesQuery = @"SELECT
	            ScaCode, ScaDescription, ScaRequestIP, SubCode, SubName
            FROM 
	            adScales 
	            JOIN adScaleSubsidiary ON (ScaCode = SasScale_ScaCode)
	            JOIN adSubsidiaries ON (SasSubsidiary_SubCode = SubCode)";

        // GET: /Home/
        public ActionResult Index(string hostName = null, string orderBy = null)
        {
          //validar en el index la cookie
         

          if (hostName == null || !new Regex(@"(((\d{1,3}\.){1,3}\d{1,3})|(\w+))(:(\d)+)?", RegexOptions.ExplicitCapture).Match(hostName).Success)
            {
                if (hostName != null) TempData["form-error"] = "IP Invalida.";
                IEnumerable<IDictionary<string, object>> scales;

                try
                {
                    SqlConnection sqlConnection;

                    //var httpCookie = Request.Cookies["Session"];
                    //if (httpCookie == null)
                    //{
                    //    //no esta con dentro de la app
                    //    TempData["central-error"] = "No se pudo acceder al servidor central error 1.";
                    //    scales = Enumerable.Empty<IDictionary<string, object>>(); ;
                    //    ViewBag.QueryTime = "";
                    //    return View(scales);
                    //}

                    //if (!ValidateCok(httpCookie.Value.ToString(CultureInfo.InvariantCulture)))
                    //{
                    //    TempData["central-error"] = "No se pudo acceder al servidor central error 2.";
                    //    scales = Enumerable.Empty<IDictionary<string, object>>(); ;
                    //    ViewBag.QueryTime = "";
                    //    return View(scales);
                    //}

                    if (Settings.Default.ExecutionEnvironment.ToLower() == "production")
                        sqlConnection = new SqlConnection(Settings.Default.ProductionDbConnectionString);
                    else
                        sqlConnection = new SqlConnection(Settings.Default.DebugDbConnectionString);

                    sqlConnection.Open();
                    SqlCommand sqlCommand = new SqlCommand(_getRegisteredScalesQuery, sqlConnection);
                    SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
                    Stopwatch stopwatch = new Stopwatch();

                    stopwatch.Start();
                    scales = sqlDataReader.ToDictionaryEnumerable().ToArray();
                    stopwatch.Stop();
                    ViewBag.QueryTime = stopwatch.Elapsed.ToString();

                    orderBy = orderBy ?? (string) Session["orderBy"];
                    if (orderBy != null && new []{"ScaCode","ScaDescription","ScaRequestIP","SubCode","SubName"}.Contains(orderBy) )
                    {
                        scales = scales.OrderBy(s => s[orderBy]);
                        Session["orderBy"] = orderBy;
                    }
                }
                catch (Exception ex)
                {
                    TempData["central-error"] = "No se pudo acceder al servidor central.";
                    scales = Enumerable.Empty<IDictionary<string, object>>(); ;
                    ViewBag.QueryTime = "";
                }

                return View(scales);
            }
            else
            {
                Session["hostName"] = hostName;
                return RedirectToAction("Index", "Scales");
            }
        }

      private bool ValidateCok(string SesCode)
      {
        SqlConnection sqlConnection;
        if (Settings.Default.ExecutionEnvironment.ToLower() == "production")
          sqlConnection = new SqlConnection(Settings.Default.ProductionDbConnectionString);
        else
          sqlConnection = new SqlConnection(Settings.Default.DebugDbConnectionString);
        sqlConnection.Open();
        var SqlQuery = @"select * from adSessions where SesCode='" + SesCode + "'";
        SqlCommand sqlCommand = new SqlCommand(SqlQuery, sqlConnection);
        SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
        if (!sqlDataReader.HasRows)
        {
          return false;
        }
        return true;
      }
    }
}
