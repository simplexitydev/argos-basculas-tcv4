﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Web.Mvc;
using ScaleService;
using ScaleWebTest.Properties;

namespace ScaleWebTest.Controllers
{
    public class ProtocolsController : Controller
    {
        //
        // GET: /Protocols/

        public ActionResult Index()
        {
            string host = Session["hostName"] as string;
            if (host == null) return RedirectToAction("Index", "Home");

            try
            {
                Stopwatch queryTime = new Stopwatch();
                ScaleServiceClient client = new ScaleServiceClient(host);

              //  var httpCookie = Request.Cookies["Session"];
              //  if (httpCookie == null)
              //  {
              //    //no esta con dentro de la app
              //    TempData["central-error"] = "No se pudo acceder al servidor central.";
                  
              //    //ViewBag.QueryTime = "";
              //    return RedirectToAction("Index", "Home");
              //  }
              //if (!ValidateCok(httpCookie.Value.ToString(CultureInfo.InvariantCulture)))
              //{
              //  TempData["central-error"] = "No se pudo acceder al servidor central.";

              //  //ViewBag.QueryTime = "";
              //  return RedirectToAction("Index", "Home");
              //}
                IEnumerable<string> availableProtocols = client.GetAvailableProtocols();

                queryTime.Start();
               
                queryTime.Stop();
                ViewBag.QueryTime = queryTime.Elapsed.ToString();
                return View(availableProtocols);
            }
            catch (Exception)
            {
                TempData["error"] = "No se pudo conectar al computador de báscula (" + host + ") especificado.";
                return RedirectToAction("Index", "Home");
            }
            
        }
        private bool ValidateCok(string SesCode)
        {
          SqlConnection sqlConnection;
          if (Settings.Default.ExecutionEnvironment.ToLower() == "production")
            sqlConnection = new SqlConnection(Settings.Default.ProductionDbConnectionString);
          else
            sqlConnection = new SqlConnection(Settings.Default.DebugDbConnectionString);
          sqlConnection.Open();
          var SqlQuery = @"select * from adSessions where SesCode='" + SesCode + "'";
          SqlCommand sqlCommand = new SqlCommand(SqlQuery, sqlConnection);
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
          if (!sqlDataReader.HasRows)
          {
            return false;
          }
          return true;
        }

    }
}
