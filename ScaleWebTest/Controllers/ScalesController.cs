﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Web.Mvc;
using ScaleService;
using System.Linq;
using ScaleWebTest.Properties;

namespace ScaleWebTest.Controllers
{
    public class ScalesController : Controller
    {
        //
        // GET: /Scales/

        public ActionResult Index()
        {
            string host = Session["hostName"] as string;
            if (host == null) return RedirectToAction("Index", "Home");

            try
            {
              //var httpCookie = Request.Cookies["Session"];
              //if (httpCookie == null)
              //{
              //  TempData["error"] = "No se pudo conectar al computador de báscula (" + host + ") especificado.";
              //  return RedirectToAction("Index", "Home");
              //}


              //if (!ValidateCok(httpCookie.Value.ToString(CultureInfo.InvariantCulture)))
              //{
              //  TempData["error"] = "No se pudo conectar al computador de báscula (" + host + ") especificado.";
              //  return RedirectToAction("Index", "Home");
              //}

              Stopwatch queryTime = new Stopwatch();
                ScaleServiceClient client = new ScaleServiceClient(host);

                queryTime.Start();
                IEnumerable<ScaleDTO> scales = client.GetScales();
                queryTime.Stop();
                ViewBag.QueryTime = queryTime.Elapsed.ToString();

                return View(scales);
            }
            catch (Exception)
            {
                TempData["error"] = "No se pudo conectar al computador de báscula (" + host +") especificado.";
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult GetWeight(string id)
        {
            string host = Session["hostName"] as string;
            if (host == null) return RedirectToAction("Index", "Home");

            try
            {
                Stopwatch queryTime = new Stopwatch();
                ScaleServiceClient client = new ScaleServiceClient(host);

                queryTime.Start();
                WeightDTO weight = client.GetWeight(id);
                queryTime.Stop();
                if (weight == null) return RedirectToAction("Index");

                ViewBag.QueryTime = queryTime.Elapsed.ToString();
                ViewBag.Id = id;
                return View(weight);
            }
            catch (Exception)
            {
                TempData["error"] = "No se pudo conectar al computador de báscula (" + host + ") especificado.";
                return RedirectToAction("Index", "Home");
            }

        }

        public ActionResult GetWeightJSON(string host, string id)
        {
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                stopwatch.Start();
                ScaleServiceClient client = new ScaleServiceClient(host);
                WeightDTO weight = client.GetWeight(id);
                stopwatch.Stop();
                return Json(weight.Weight + " en " + stopwatch.Elapsed + ".", JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                stopwatch.Stop();
                return Json("Sin Conexión en " + stopwatch.Elapsed + ".", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Edit(string id)
        {
            string host = Session["hostName"] as string;
            if (host == null) return RedirectToAction("Index", "Home");

            try
            {
                ScaleServiceClient client = new ScaleServiceClient(host);
                ScaleDTO scale = client.GetScales().FirstOrDefault(s => s.Code == id);
                if (scale == null) return RedirectToAction("Index");

                ViewBag.Protocols = client.GetAvailableProtocols();

                return View(scale);
            }
            catch (Exception)
            {
                TempData["error"] = "No se pudo conectar al computador de báscula (" + host + ") especificado.";
                return RedirectToAction("Index", "Home");
            }
            
        }

        [HttpPost]
        public ActionResult Edit(ScaleDTO scale)
        {
            string host = Session["hostName"] as string;
            if (host == null) return RedirectToAction("Index", "Home");

            try
            {
                ScaleServiceClient client = new ScaleServiceClient(host);
                scale.EnabledSpecified = true;
                scale.WeightVariationSpecified = true;
                scale.ComPortInfo.BaudRateSpecified = true;
                scale.ComPortInfo.DataBitsSpecified = true;

                MessageDTO message = client.SetScalesConfiguration(new ScaleDTO[] { scale });

                TempData["info"] = "[" + message.TypeEnum + " - " + message.TransactionNumber + "] " + message.Message;
                return RedirectToAction("Index");
            }
            catch (Exception)
            {
                TempData["error"] = "No se pudo conectar al computador de báscula (" + host + ") especificado.";
                return RedirectToAction("Index", "Home");
            }
        }

        public  ActionResult Restart()
        {
            string host = Session["hostName"] as string;
            if (host == null) return RedirectToAction("Index", "Home");

            try
            {
                ScaleServiceClient client = new ScaleServiceClient(host);
                MessageDTO message = client.Restart();

                TempData["info"] = "[" + message.TypeEnum + " - " + message.TransactionNumber + "] " + message.Message;
                return RedirectToAction("Index", "Home");
            }
            catch (Exception)
            {
                TempData["error"] = "No se pudo conectar al computador de báscula (" + host + ") especificado.";
                return RedirectToAction("Index", "Home");
            }
        }
        private bool ValidateCok(string SesCode)
        {
          SqlConnection sqlConnection;
          if (Settings.Default.ExecutionEnvironment.ToLower() == "production")
            sqlConnection = new SqlConnection(Settings.Default.ProductionDbConnectionString);
          else
            sqlConnection = new SqlConnection(Settings.Default.DebugDbConnectionString);
          sqlConnection.Open();
          var SqlQuery = @"select * from adSessions where SesCode='" + SesCode + "'";
          SqlCommand sqlCommand = new SqlCommand(SqlQuery, sqlConnection);
          SqlDataReader sqlDataReader = sqlCommand.ExecuteReader(CommandBehavior.CloseConnection);
          if (!sqlDataReader.HasRows)
          {
            return false;
          }
          return true;
        }
    }
}
