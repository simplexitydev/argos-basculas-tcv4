﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using System.IO;

namespace Simplexity.Scales.UI.Main
{
  public class TcpSimpleClient
  {

    #region [ Def's Variables ]

    //Utilizado para enviar datos al Servidor y recibir datos del mismo
    private Stream Stm;
    //Direccion del objeto de la clase Servidor
    //Puerto donde escucha el objeto de la clase Servidor
    public bool envIMg = false;
    public double Trama = 0;
    public bool termino = false;
    TcpClient tcpClnt;
    public Thread TcpThReconnect;
    //Se encarga de escuchar mensajes enviados por el Servidor
    public Thread TcpThd;
    public double sizeF = 0;
    public double indF = 0;
    private bool _shconnect;
    #endregion


    #region "EVENTOS"
    public delegate void ConexionTerminadaEventHandler();
    public event ConexionTerminadaEventHandler ConexionTerminada;

    public event DatosRecibidosEventHandler DatosRecibidos;
    public delegate void DatosRecibidosEventHandler(string datos);
    public event DatosRecibidosIMGEventHandler DatosRecibidosIMG;
    public delegate void DatosRecibidosIMGEventHandler(byte[] arr);
    public event errSockEventHandler errSock;
    public delegate void errSockEventHandler();
    public event conectokEventHandler conectok;
    public delegate void conectokEventHandler();

    public string LastError { get; set; }

    public void OnConexionTerminada()
    {
      ConexionTerminadaEventHandler handler = ConexionTerminada;
      if (handler != null) handler();
    }

    /// <summary>
    /// determina si el socket se encuentra conectado
    /// </summary>
    public bool IsConnected
    {
      get { return tcpClnt.Connected; }
      set { _tcpClnt = value; }
    }

    public byte[] b;
    byte[] BufferDeLectura;
    #endregion

    int numPaq = 768;
    private bool _tcpClnt;

    #region "PROPIEDADES"

    public string IPDelHost { get; set; }
    public string PuertoDelHost { get; set; }

    #endregion



    #region "METODOS"

    public TcpSimpleClient()
    {
      tcpClnt = new TcpClient();
      _shconnect = false;
    }

    public bool Conectar()
    {


      tcpClnt = new TcpClient();
      tcpClnt.SendTimeout = 1000;
      tcpClnt.ReceiveTimeout = 1000;
      //Me conecto al objeto de la clase Servidor,

      //  determinado por las propiedades IPDelHost y PuertoDelHost
      try
      {
        tcpClnt.LingerState.Enabled = false;
        tcpClnt.Connect(new IPEndPoint(IPAddress.Parse(IPDelHost), Convert.ToInt32(PuertoDelHost)));
        termino = false;

      }
      catch (Exception ex)
      {
        LastError = ex.ToString();
        if (errSock != null)
        {
          errSock();
        }
        Terminar();
        return false;

      }


      Stm = tcpClnt.GetStream();


      //Creo e inicio un thread para que escuche los mensajes enviados por el Servidor

      TcpThd = new Thread(LeerSocket);
      TcpThd.Start();
      if (conectok != null)
      {
        conectok();
      }




      IsConnected = true;
      return true;
    }

    public void Terminar()
    {
      #region Cerrar Buffer de salida

      try
      {
        Stm.Close();
      }
      catch
      {
      }
      #endregion

      termino = true;
      try
      {
        TcpThd.Abort();
        TcpThd = null;

      }
      catch (Exception ex)
      {
      }
      try
      {
        TcpThReconnect.Abort();
        TcpThReconnect = null;
      }
      catch (Exception)
      {


      }
      try
      {
        tcpClnt.Close();
      }
      catch (Exception)
      {


      }

      IsConnected = false;
      tcpClnt = new TcpClient();
      OnConexionTerminada();
    }

    public void Verifica_Conect()
    {
      while (_shconnect)
      {
        System.Threading.Thread.Sleep(2500);
        if (!tcpClnt.Connected)
        {
          //se procede a la cnx
          tcpClnt.Connect(new IPEndPoint(IPAddress.Parse(IPDelHost), Convert.ToInt32(PuertoDelHost)));
        }
      }


    }

    public bool EnviarDatos(string Datos)
    {
      bool result = false;

      byte[] BufferDeEscritura = null;

      BufferDeEscritura = Encoding.ASCII.GetBytes(Datos);


      if ((Stm != null))
      {
        //Envio los datos al Servidor
        try
        {
          //

          if (tcpClnt.Connected)
          {
            Stm.Write(BufferDeEscritura, 0, BufferDeEscritura.Length);
            result = tcpClnt.Connected;
          }
          else
          {
            IsConnected = false;
            result = false;
            LastError = "Error de cnx";
            if (errSock != null)
            {

              errSock();
            }
            this.Terminar();
          }

        }
        catch (Exception ex)
        {
          IsConnected = false;
          result = false;
          LastError = ex.ToString();
          if (errSock != null)
          {

            errSock();
          }
          this.Terminar();
        }


      }
      return result;
    }



    #endregion



    #region "FUNCIONES PRIVADAS"


    private void LeerSocket()
    {
      bool paso = false;



      while (!termino)
      {
        try
        {
          if (envIMg == false)
          {
            
            BufferDeLectura = new byte[100];

            //Me quedo esperando a que llegue algun mensaje
            Thread.Sleep(500);
            Stm.Read(BufferDeLectura, 0, BufferDeLectura.Length);



            //Genero el evento DatosRecibidos, ya que se han recibido datos desde el Servidor
            //RaiseEvent DatosRecibidosIMG(BufferDeLectura)
            if (DatosRecibidos != null)
            {
              DatosRecibidos(Encoding.Default.GetString(BufferDeLectura, 0, BufferDeLectura.Length));
            }


          }
          else
          {

            BufferDeLectura = new byte[numPaq];


            Stm.Read(BufferDeLectura, 0, numPaq);


            if (DatosRecibidosIMG != null)
            {
              DatosRecibidosIMG(BufferDeLectura);
            }
            //envIMg = False
            Trama = 0;
            sizeF = 0;
            indF = 0;
            paso = false;


          }


        }
        catch (Exception e)
        {
          //MsgBox(Err.Description)
          //Call Me.terminar()
          //RaiseEvent errSock()
          break; // TODO: might not be correct. Was : Exit While

        }

      }



      //Finalizo la conexion, por lo tanto genero el evento correspondiente

      if (ConexionTerminada != null)
      {
        ConexionTerminada();
      }

    }

    #endregion

  }
}
