﻿using System;
using System.Windows.Forms;

namespace Simplexity.Scales.UI.Main
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {

      //StartScales();
      //StartService();

      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);
      //Application.Run(new frmScaleService());
      Application.Run(new frmScalesProtocol());




    }

  }
}
