﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Protocols;
using Simplexity.Scales.UI.Main.Properties;

namespace Simplexity.Scales.UI.Main
{
  public partial class frmScalesProtocol : Form
  {

    private TcpSimpleClient client;
    private List<string> lista;
    private Thread hilo1;
    private bool termina;

    public frmScalesProtocol()
    {
      InitializeComponent();
      termina = false;
      
    }


    /// <summary>
    /// Analiza el buffer de entrada de protocolos
    /// </summary>
    public void HiloAnalizar()
    {
      while (!termina)
      {
        if (lista != null)
        {
          //hay que analizar la trama
          if (lista.Count>0)
          {
            var primero = lista.FirstOrDefault();
            //procesa el primero 
            foreach (String odn in (ICollection)Domain.Main.Spring.GetObjectDefinitionNames())
            {
              var check = (IDecryptable)Domain.Main.Spring.GetObject(odn, ScaleConstants.SpringContext);
              if (primero != null && (check.Regex != null && check.Regex.Match(primero).Success))
              {

				var a = check.Decrypt(System.Text.Encoding.ASCII.GetBytes(primero));

				if (!listBox1.Items.Contains(odn))
                {
                  UpdateListBox(odn + " Weight: " + a.Weight.ToString());
                }
              }
            }
            ////elimina elprimaero
            lista.Remove(primero);
          }
        }
      }
    }

    private void button1_Click(object sender, EventArgs e)
    {
      lista = new List<string>();
      if(!button1.Text.Equals(Resources.frmScalesProtocol_button1_Click_Conectar))
      {
        if (client != null)
          client.Terminar();
        button1.Text = Resources.frmScalesProtocol_button1_Click_Conectar;
      }
      else
      {
        listBox1.Items.Clear();
        richTextBox1.Text = string.Empty;
        button1.Text = Resources.frmScalesProtocol_button1_Click_Desconectar;
        client = new TcpSimpleClient {IPDelHost = textBox1.Text, PuertoDelHost = textBox2.Text};
        client.DatosRecibidos += FrameReceived;
        client.Conectar();
        termina = false;
        hilo1 = new Thread(HiloAnalizar);
        hilo1.Start();
      }
    }

    private void UpdateTextBox(string text)
    {     
      Invoke((MethodInvoker) delegate
        {

          richTextBox1.Text += text;
          
        });
    }

    public void UpdateListBox(string text)

    {     
      Invoke((MethodInvoker) delegate
        {
          listBox1.Items.Add(text);
        });
    }

    /// <summary>
    /// Se recibe protocolos y se almacena en un buffer
    /// </summary>
    /// <param name="data"></param>
    private void FrameReceived(string data)
    {
      //Thread.Sleep(1000);
      lista.Add(data.Replace("\0",string.Empty));
      UpdateTextBox(data);
    }

    private void button3_Click(object sender, EventArgs e)
    {
      termina = true;
      Application.DoEvents();
      richTextBox1.Text = String.Empty;
      listBox1.Items.Clear();
    }
  }
}
