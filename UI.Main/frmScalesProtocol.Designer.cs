﻿namespace Simplexity.Scales.UI.Main
{
  partial class frmScalesProtocol
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.tabControl1 = new System.Windows.Forms.TabControl();
      this.tabPage1 = new System.Windows.Forms.TabPage();
      this.listBox1 = new System.Windows.Forms.ListBox();
      this.label8 = new System.Windows.Forms.Label();
      this.button3 = new System.Windows.Forms.Button();
      this.button1 = new System.Windows.Forms.Button();
      this.textBox2 = new System.Windows.Forms.TextBox();
      this.label2 = new System.Windows.Forms.Label();
      this.textBox1 = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.richTextBox1 = new System.Windows.Forms.RichTextBox();
      this.richTextBox2 = new System.Windows.Forms.RichTextBox();
      this.cbxComName = new System.Windows.Forms.ComboBox();
      this.cbxBaudRate = new System.Windows.Forms.ComboBox();
      this.cbxDataSize = new System.Windows.Forms.ComboBox();
      this.cbxStopBits = new System.Windows.Forms.ComboBox();
      this.cbxParity = new System.Windows.Forms.ComboBox();
      this.label3 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label7 = new System.Windows.Forms.Label();
      this.tabPage2 = new System.Windows.Forms.TabPage();
      this.tabControl1.SuspendLayout();
      this.tabPage1.SuspendLayout();
      this.tabPage2.SuspendLayout();
      this.SuspendLayout();
      // 
      // tabControl1
      // 
      this.tabControl1.Controls.Add(this.tabPage1);
      this.tabControl1.Controls.Add(this.tabPage2);
      this.tabControl1.Location = new System.Drawing.Point(2, 2);
      this.tabControl1.Name = "tabControl1";
      this.tabControl1.SelectedIndex = 0;
      this.tabControl1.Size = new System.Drawing.Size(678, 441);
      this.tabControl1.TabIndex = 5;
      // 
      // tabPage1
      // 
      this.tabPage1.Controls.Add(this.listBox1);
      this.tabPage1.Controls.Add(this.label8);
      this.tabPage1.Controls.Add(this.button3);
      this.tabPage1.Controls.Add(this.button1);
      this.tabPage1.Controls.Add(this.textBox2);
      this.tabPage1.Controls.Add(this.label2);
      this.tabPage1.Controls.Add(this.textBox1);
      this.tabPage1.Controls.Add(this.label1);
      this.tabPage1.Controls.Add(this.richTextBox1);
      this.tabPage1.Location = new System.Drawing.Point(4, 22);
      this.tabPage1.Name = "tabPage1";
      this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage1.Size = new System.Drawing.Size(670, 415);
      this.tabPage1.TabIndex = 0;
      this.tabPage1.Text = "TCP Client";
      this.tabPage1.UseVisualStyleBackColor = true;
      // 
      // listBox1
      // 
      this.listBox1.FormattingEnabled = true;
      this.listBox1.Location = new System.Drawing.Point(16, 169);
      this.listBox1.Name = "listBox1";
      this.listBox1.Size = new System.Drawing.Size(206, 238);
      this.listBox1.TabIndex = 14;
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label8.ForeColor = System.Drawing.Color.Red;
      this.label8.Location = new System.Drawing.Point(12, 145);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(150, 20);
      this.label8.TabIndex = 13;
      this.label8.Text = "Protocolos posibles:";
      // 
      // button3
      // 
      this.button3.Location = new System.Drawing.Point(16, 75);
      this.button3.Name = "button3";
      this.button3.Size = new System.Drawing.Size(100, 23);
      this.button3.TabIndex = 12;
      this.button3.Text = "Limpiar";
      this.button3.UseVisualStyleBackColor = true;
      this.button3.Click += new System.EventHandler(this.button3_Click);
      // 
      // button1
      // 
      this.button1.Location = new System.Drawing.Point(122, 75);
      this.button1.Name = "button1";
      this.button1.Size = new System.Drawing.Size(100, 23);
      this.button1.TabIndex = 10;
      this.button1.Text = "Conectar";
      this.button1.UseVisualStyleBackColor = true;
      this.button1.Click += new System.EventHandler(this.button1_Click);
      // 
      // textBox2
      // 
      this.textBox2.Location = new System.Drawing.Point(151, 48);
      this.textBox2.Name = "textBox2";
      this.textBox2.Size = new System.Drawing.Size(71, 20);
      this.textBox2.TabIndex = 9;
      this.textBox2.Text = "4001";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(148, 31);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(41, 13);
      this.label2.TabIndex = 8;
      this.label2.Text = "Puerto:";
      // 
      // textBox1
      // 
      this.textBox1.Location = new System.Drawing.Point(16, 49);
      this.textBox1.Name = "textBox1";
      this.textBox1.Size = new System.Drawing.Size(128, 20);
      this.textBox1.TabIndex = 7;
      this.textBox1.Text = "10.5.1.25";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(13, 31);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(55, 13);
      this.label1.TabIndex = 6;
      this.label1.Text = "Dirección:";
      // 
      // richTextBox1
      // 
      this.richTextBox1.Location = new System.Drawing.Point(253, 3);
      this.richTextBox1.Name = "richTextBox1";
      this.richTextBox1.Size = new System.Drawing.Size(411, 406);
      this.richTextBox1.TabIndex = 5;
      this.richTextBox1.Text = "";
      // 
      // richTextBox2
      // 
      this.richTextBox2.Location = new System.Drawing.Point(256, 3);
      this.richTextBox2.Name = "richTextBox2";
      this.richTextBox2.Size = new System.Drawing.Size(411, 406);
      this.richTextBox2.TabIndex = 6;
      this.richTextBox2.Text = "";
      // 
      // cbxComName
      // 
      this.cbxComName.FormattingEnabled = true;
      this.cbxComName.Location = new System.Drawing.Point(22, 32);
      this.cbxComName.Name = "cbxComName";
      this.cbxComName.Size = new System.Drawing.Size(113, 21);
      this.cbxComName.TabIndex = 16;
      // 
      // cbxBaudRate
      // 
      this.cbxBaudRate.FormattingEnabled = true;
      this.cbxBaudRate.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
      this.cbxBaudRate.Location = new System.Drawing.Point(22, 72);
      this.cbxBaudRate.Name = "cbxBaudRate";
      this.cbxBaudRate.Size = new System.Drawing.Size(113, 21);
      this.cbxBaudRate.TabIndex = 17;
      // 
      // cbxDataSize
      // 
      this.cbxDataSize.FormattingEnabled = true;
      this.cbxDataSize.Items.AddRange(new object[] {
            "7",
            "8"});
      this.cbxDataSize.Location = new System.Drawing.Point(22, 112);
      this.cbxDataSize.Name = "cbxDataSize";
      this.cbxDataSize.Size = new System.Drawing.Size(113, 21);
      this.cbxDataSize.TabIndex = 18;
      // 
      // cbxStopBits
      // 
      this.cbxStopBits.FormattingEnabled = true;
      this.cbxStopBits.Location = new System.Drawing.Point(22, 152);
      this.cbxStopBits.Name = "cbxStopBits";
      this.cbxStopBits.Size = new System.Drawing.Size(113, 21);
      this.cbxStopBits.TabIndex = 19;
      // 
      // cbxParity
      // 
      this.cbxParity.FormattingEnabled = true;
      this.cbxParity.Location = new System.Drawing.Point(22, 192);
      this.cbxParity.Name = "cbxParity";
      this.cbxParity.Size = new System.Drawing.Size(113, 21);
      this.cbxParity.TabIndex = 20;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(19, 16);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(0, 13);
      this.label3.TabIndex = 21;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(19, 56);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(0, 13);
      this.label4.TabIndex = 22;
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Location = new System.Drawing.Point(19, 96);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(0, 13);
      this.label5.TabIndex = 23;
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(19, 136);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(0, 13);
      this.label6.TabIndex = 24;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(19, 176);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(0, 13);
      this.label7.TabIndex = 25;
      // 
      // tabPage2
      // 
      this.tabPage2.Controls.Add(this.label7);
      this.tabPage2.Controls.Add(this.label6);
      this.tabPage2.Controls.Add(this.label5);
      this.tabPage2.Controls.Add(this.label4);
      this.tabPage2.Controls.Add(this.label3);
      this.tabPage2.Controls.Add(this.cbxParity);
      this.tabPage2.Controls.Add(this.cbxStopBits);
      this.tabPage2.Controls.Add(this.cbxDataSize);
      this.tabPage2.Controls.Add(this.cbxBaudRate);
      this.tabPage2.Controls.Add(this.cbxComName);
      this.tabPage2.Controls.Add(this.richTextBox2);
      this.tabPage2.Location = new System.Drawing.Point(4, 22);
      this.tabPage2.Name = "tabPage2";
      this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
      this.tabPage2.Size = new System.Drawing.Size(670, 415);
      this.tabPage2.TabIndex = 1;
      this.tabPage2.Text = "Serial";
      this.tabPage2.UseVisualStyleBackColor = true;
      // 
      // frmScalesProtocol
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(682, 446);
      this.Controls.Add(this.tabControl1);
      this.Name = "frmScalesProtocol";
      this.Text = "frmScalesProtocol";
      this.tabControl1.ResumeLayout(false);
      this.tabPage1.ResumeLayout(false);
      this.tabPage1.PerformLayout();
      this.tabPage2.ResumeLayout(false);
      this.tabPage2.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.TabControl tabControl1;
    private System.Windows.Forms.TabPage tabPage1;
    private System.Windows.Forms.TextBox textBox2;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox textBox1;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.RichTextBox richTextBox1;
    private System.Windows.Forms.Button button1;
    private System.Windows.Forms.Button button3;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.ListBox listBox1;
    private System.Windows.Forms.TabPage tabPage2;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.ComboBox cbxParity;
    private System.Windows.Forms.ComboBox cbxStopBits;
    private System.Windows.Forms.ComboBox cbxDataSize;
    private System.Windows.Forms.ComboBox cbxBaudRate;
    private System.Windows.Forms.ComboBox cbxComName;
    private System.Windows.Forms.RichTextBox richTextBox2;
  }
}