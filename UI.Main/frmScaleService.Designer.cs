﻿namespace Simplexity.Scales.UI.Main
{
  partial class frmScaleService
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.chkEnabled = new System.Windows.Forms.CheckBox();
      this.chkSerial = new System.Windows.Forms.CheckBox();
      this.groupBoxTCP = new System.Windows.Forms.GroupBox();
      this.btnStopService = new System.Windows.Forms.Button();
      this.textPort = new System.Windows.Forms.TextBox();
      this.textIpAddress = new System.Windows.Forms.TextBox();
      this.label13 = new System.Windows.Forms.Label();
      this.label6 = new System.Windows.Forms.Label();
      this.label12 = new System.Windows.Forms.Label();
      this.groupBoxSerial = new System.Windows.Forms.GroupBox();
      this.cbxParity = new System.Windows.Forms.ComboBox();
      this.cbxStopBits = new System.Windows.Forms.ComboBox();
      this.btnOpen = new System.Windows.Forms.Button();
      this.cbxDataSize = new System.Windows.Forms.ComboBox();
      this.cbxBaudRate = new System.Windows.Forms.ComboBox();
      this.cbxComName = new System.Windows.Forms.ComboBox();
      this.label2 = new System.Windows.Forms.Label();
      this.label5 = new System.Windows.Forms.Label();
      this.label4 = new System.Windows.Forms.Label();
      this.label3 = new System.Windows.Forms.Label();
      this.label1 = new System.Windows.Forms.Label();
      this.groupBox4 = new System.Windows.Forms.GroupBox();
      this.btnClear = new System.Windows.Forms.Button();
      this.textReceivedData = new System.Windows.Forms.TextBox();
      this.groupBox5 = new System.Windows.Forms.GroupBox();
      this.numTolerance = new System.Windows.Forms.NumericUpDown();
      this.txtName = new System.Windows.Forms.TextBox();
      this.label7 = new System.Windows.Forms.Label();
      this.cbxNameScale = new System.Windows.Forms.ComboBox();
      this.cbxProtocol = new System.Windows.Forms.ComboBox();
      this.label11 = new System.Windows.Forms.Label();
      this.textCode = new System.Windows.Forms.TextBox();
      this.label10 = new System.Windows.Forms.Label();
      this.label9 = new System.Windows.Forms.Label();
      this.label8 = new System.Windows.Forms.Label();
      this.btnSave = new System.Windows.Forms.Button();
      this.btnStartService = new System.Windows.Forms.Button();
      this.groupBox6 = new System.Windows.Forms.GroupBox();
      this.btnRestartService = new System.Windows.Forms.Button();
      this.btnTest = new System.Windows.Forms.Button();
      this.groupBox1.SuspendLayout();
      this.groupBoxTCP.SuspendLayout();
      this.groupBoxSerial.SuspendLayout();
      this.groupBox4.SuspendLayout();
      this.groupBox5.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numTolerance)).BeginInit();
      this.groupBox6.SuspendLayout();
      this.SuspendLayout();
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.chkEnabled);
      this.groupBox1.Controls.Add(this.chkSerial);
      this.groupBox1.Controls.Add(this.groupBoxTCP);
      this.groupBox1.Controls.Add(this.groupBoxSerial);
      this.groupBox1.Location = new System.Drawing.Point(12, 18);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(283, 362);
      this.groupBox1.TabIndex = 0;
      this.groupBox1.TabStop = false;
      this.groupBox1.Text = "Config";
      // 
      // chkEnabled
      // 
      this.chkEnabled.AutoSize = true;
      this.chkEnabled.Location = new System.Drawing.Point(159, 19);
      this.chkEnabled.Name = "chkEnabled";
      this.chkEnabled.Size = new System.Drawing.Size(59, 17);
      this.chkEnabled.TabIndex = 3;
      this.chkEnabled.Text = "Enable";
      this.chkEnabled.UseVisualStyleBackColor = true;
      // 
      // chkSerial
      // 
      this.chkSerial.AutoSize = true;
      this.chkSerial.Location = new System.Drawing.Point(79, 20);
      this.chkSerial.Name = "chkSerial";
      this.chkSerial.Size = new System.Drawing.Size(52, 17);
      this.chkSerial.TabIndex = 2;
      this.chkSerial.Text = "Serial";
      this.chkSerial.UseVisualStyleBackColor = true;
      this.chkSerial.CheckedChanged += new System.EventHandler(this.chkSerial_CheckedChanged);
      // 
      // groupBoxTCP
      // 
      this.groupBoxTCP.Controls.Add(this.btnStopService);
      this.groupBoxTCP.Controls.Add(this.textPort);
      this.groupBoxTCP.Controls.Add(this.textIpAddress);
      this.groupBoxTCP.Controls.Add(this.label13);
      this.groupBoxTCP.Controls.Add(this.label6);
      this.groupBoxTCP.Controls.Add(this.label12);
      this.groupBoxTCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBoxTCP.Location = new System.Drawing.Point(19, 242);
      this.groupBoxTCP.Name = "groupBoxTCP";
      this.groupBoxTCP.Size = new System.Drawing.Size(246, 114);
      this.groupBoxTCP.TabIndex = 1;
      this.groupBoxTCP.TabStop = false;
      this.groupBoxTCP.Text = "TCP IP";
      // 
      // btnStopService
      // 
      this.btnStopService.Enabled = false;
      this.btnStopService.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnStopService.Location = new System.Drawing.Point(68, 84);
      this.btnStopService.Name = "btnStopService";
      this.btnStopService.Size = new System.Drawing.Size(113, 26);
      this.btnStopService.TabIndex = 16;
      this.btnStopService.Text = "Connect";
      this.btnStopService.UseVisualStyleBackColor = true;
      // 
      // textPort
      // 
      this.textPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.textPort.Location = new System.Drawing.Point(115, 55);
      this.textPort.Name = "textPort";
      this.textPort.Size = new System.Drawing.Size(117, 22);
      this.textPort.TabIndex = 16;
      // 
      // textIpAddress
      // 
      this.textIpAddress.Location = new System.Drawing.Point(115, 29);
      this.textIpAddress.Name = "textIpAddress";
      this.textIpAddress.Size = new System.Drawing.Size(117, 22);
      this.textIpAddress.TabIndex = 15;
      // 
      // label13
      // 
      this.label13.AutoSize = true;
      this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label13.Location = new System.Drawing.Point(14, 58);
      this.label13.Name = "label13";
      this.label13.Size = new System.Drawing.Size(32, 16);
      this.label13.TabIndex = 14;
      this.label13.Text = "Port";
      // 
      // label6
      // 
      this.label6.AutoSize = true;
      this.label6.Location = new System.Drawing.Point(14, 58);
      this.label6.Name = "label6";
      this.label6.Size = new System.Drawing.Size(32, 16);
      this.label6.TabIndex = 14;
      this.label6.Text = "Port";
      // 
      // label12
      // 
      this.label12.AutoSize = true;
      this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label12.Location = new System.Drawing.Point(14, 32);
      this.label12.Name = "label12";
      this.label12.Size = new System.Drawing.Size(70, 16);
      this.label12.TabIndex = 12;
      this.label12.Text = "IpAddress";
      // 
      // groupBoxSerial
      // 
      this.groupBoxSerial.Controls.Add(this.cbxParity);
      this.groupBoxSerial.Controls.Add(this.cbxStopBits);
      this.groupBoxSerial.Controls.Add(this.btnOpen);
      this.groupBoxSerial.Controls.Add(this.cbxDataSize);
      this.groupBoxSerial.Controls.Add(this.cbxBaudRate);
      this.groupBoxSerial.Controls.Add(this.cbxComName);
      this.groupBoxSerial.Controls.Add(this.label2);
      this.groupBoxSerial.Controls.Add(this.label5);
      this.groupBoxSerial.Controls.Add(this.label4);
      this.groupBoxSerial.Controls.Add(this.label3);
      this.groupBoxSerial.Controls.Add(this.label1);
      this.groupBoxSerial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBoxSerial.Location = new System.Drawing.Point(19, 33);
      this.groupBoxSerial.Name = "groupBoxSerial";
      this.groupBoxSerial.Size = new System.Drawing.Size(246, 204);
      this.groupBoxSerial.TabIndex = 0;
      this.groupBoxSerial.TabStop = false;
      this.groupBoxSerial.Text = "Serial";
      // 
      // cbxParity
      // 
      this.cbxParity.FormattingEnabled = true;
      this.cbxParity.Location = new System.Drawing.Point(119, 139);
      this.cbxParity.Name = "cbxParity";
      this.cbxParity.Size = new System.Drawing.Size(113, 24);
      this.cbxParity.TabIndex = 15;
      // 
      // cbxStopBits
      // 
      this.cbxStopBits.FormattingEnabled = true;
      this.cbxStopBits.Location = new System.Drawing.Point(119, 112);
      this.cbxStopBits.Name = "cbxStopBits";
      this.cbxStopBits.Size = new System.Drawing.Size(113, 24);
      this.cbxStopBits.TabIndex = 14;
      // 
      // btnOpen
      // 
      this.btnOpen.Enabled = false;
      this.btnOpen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnOpen.Location = new System.Drawing.Point(68, 169);
      this.btnOpen.Name = "btnOpen";
      this.btnOpen.Size = new System.Drawing.Size(113, 26);
      this.btnOpen.TabIndex = 17;
      this.btnOpen.Text = "Open";
      this.btnOpen.UseVisualStyleBackColor = true;
      this.btnOpen.Click += new System.EventHandler(this.BtnOpenClick);
      // 
      // cbxDataSize
      // 
      this.cbxDataSize.FormattingEnabled = true;
      this.cbxDataSize.Items.AddRange(new object[] {
            "7",
            "8"});
      this.cbxDataSize.Location = new System.Drawing.Point(119, 85);
      this.cbxDataSize.Name = "cbxDataSize";
      this.cbxDataSize.Size = new System.Drawing.Size(113, 24);
      this.cbxDataSize.TabIndex = 13;
      // 
      // cbxBaudRate
      // 
      this.cbxBaudRate.FormattingEnabled = true;
      this.cbxBaudRate.Items.AddRange(new object[] {
            "300",
            "600",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
      this.cbxBaudRate.Location = new System.Drawing.Point(119, 58);
      this.cbxBaudRate.Name = "cbxBaudRate";
      this.cbxBaudRate.Size = new System.Drawing.Size(113, 24);
      this.cbxBaudRate.TabIndex = 12;
      // 
      // cbxComName
      // 
      this.cbxComName.FormattingEnabled = true;
      this.cbxComName.Location = new System.Drawing.Point(119, 31);
      this.cbxComName.Name = "cbxComName";
      this.cbxComName.Size = new System.Drawing.Size(113, 24);
      this.cbxComName.TabIndex = 11;
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label2.Location = new System.Drawing.Point(14, 61);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(72, 16);
      this.label2.TabIndex = 10;
      this.label2.Text = "Baud Rate";
      // 
      // label5
      // 
      this.label5.AutoSize = true;
      this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label5.Location = new System.Drawing.Point(14, 142);
      this.label5.Name = "label5";
      this.label5.Size = new System.Drawing.Size(42, 16);
      this.label5.TabIndex = 9;
      this.label5.Text = "Parity";
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label4.Location = new System.Drawing.Point(14, 115);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(54, 16);
      this.label4.TabIndex = 7;
      this.label4.Text = "Bit Stop";
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label3.Location = new System.Drawing.Point(14, 88);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(66, 16);
      this.label3.TabIndex = 5;
      this.label3.Text = "Data Size";
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(14, 34);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(73, 16);
      this.label1.TabIndex = 1;
      this.label1.Text = "ComName";
      // 
      // groupBox4
      // 
      this.groupBox4.Controls.Add(this.btnClear);
      this.groupBox4.Controls.Add(this.textReceivedData);
      this.groupBox4.Location = new System.Drawing.Point(313, 18);
      this.groupBox4.Name = "groupBox4";
      this.groupBox4.Size = new System.Drawing.Size(447, 237);
      this.groupBox4.TabIndex = 1;
      this.groupBox4.TabStop = false;
      this.groupBox4.Text = "Received Data";
      // 
      // btnClear
      // 
      this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnClear.Location = new System.Drawing.Point(9, 202);
      this.btnClear.Name = "btnClear";
      this.btnClear.Size = new System.Drawing.Size(53, 26);
      this.btnClear.TabIndex = 21;
      this.btnClear.Text = "Clear";
      this.btnClear.UseVisualStyleBackColor = true;
      this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
      // 
      // textReceivedData
      // 
      this.textReceivedData.Location = new System.Drawing.Point(7, 29);
      this.textReceivedData.Multiline = true;
      this.textReceivedData.Name = "textReceivedData";
      this.textReceivedData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.textReceivedData.Size = new System.Drawing.Size(434, 167);
      this.textReceivedData.TabIndex = 0;
      // 
      // groupBox5
      // 
      this.groupBox5.Controls.Add(this.numTolerance);
      this.groupBox5.Controls.Add(this.txtName);
      this.groupBox5.Controls.Add(this.label7);
      this.groupBox5.Controls.Add(this.cbxNameScale);
      this.groupBox5.Controls.Add(this.cbxProtocol);
      this.groupBox5.Controls.Add(this.label11);
      this.groupBox5.Controls.Add(this.textCode);
      this.groupBox5.Controls.Add(this.label10);
      this.groupBox5.Controls.Add(this.label9);
      this.groupBox5.Controls.Add(this.label8);
      this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.groupBox5.Location = new System.Drawing.Point(315, 261);
      this.groupBox5.Name = "groupBox5";
      this.groupBox5.Size = new System.Drawing.Size(447, 119);
      this.groupBox5.TabIndex = 2;
      this.groupBox5.TabStop = false;
      this.groupBox5.Text = "Scale";
      // 
      // numTolerance
      // 
      this.numTolerance.Location = new System.Drawing.Point(297, 89);
      this.numTolerance.Name = "numTolerance";
      this.numTolerance.Size = new System.Drawing.Size(126, 22);
      this.numTolerance.TabIndex = 20;
      // 
      // txtName
      // 
      this.txtName.Location = new System.Drawing.Point(92, 60);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(331, 22);
      this.txtName.TabIndex = 20;
      // 
      // label7
      // 
      this.label7.AutoSize = true;
      this.label7.Location = new System.Drawing.Point(28, 67);
      this.label7.Name = "label7";
      this.label7.Size = new System.Drawing.Size(45, 16);
      this.label7.TabIndex = 21;
      this.label7.Text = "Name";
      // 
      // cbxNameScale
      // 
      this.cbxNameScale.FormattingEnabled = true;
      this.cbxNameScale.Location = new System.Drawing.Point(92, 31);
      this.cbxNameScale.Name = "cbxNameScale";
      this.cbxNameScale.Size = new System.Drawing.Size(118, 24);
      this.cbxNameScale.TabIndex = 19;
      this.cbxNameScale.SelectedIndexChanged += new System.EventHandler(this.cbxNameScale_SelectedIndexChanged);
      // 
      // cbxProtocol
      // 
      this.cbxProtocol.FormattingEnabled = true;
      this.cbxProtocol.Location = new System.Drawing.Point(92, 89);
      this.cbxProtocol.Name = "cbxProtocol";
      this.cbxProtocol.Size = new System.Drawing.Size(118, 24);
      this.cbxProtocol.TabIndex = 16;
      // 
      // label11
      // 
      this.label11.AutoSize = true;
      this.label11.Location = new System.Drawing.Point(220, 94);
      this.label11.Name = "label11";
      this.label11.Size = new System.Drawing.Size(70, 16);
      this.label11.TabIndex = 18;
      this.label11.Text = "Tolerance";
      // 
      // textCode
      // 
      this.textCode.Location = new System.Drawing.Point(297, 31);
      this.textCode.Name = "textCode";
      this.textCode.Size = new System.Drawing.Size(126, 22);
      this.textCode.TabIndex = 17;
      // 
      // label10
      // 
      this.label10.AutoSize = true;
      this.label10.Location = new System.Drawing.Point(225, 34);
      this.label10.Name = "label10";
      this.label10.Size = new System.Drawing.Size(41, 16);
      this.label10.TabIndex = 16;
      this.label10.Text = "Code";
      // 
      // label9
      // 
      this.label9.AutoSize = true;
      this.label9.Location = new System.Drawing.Point(28, 94);
      this.label9.Name = "label9";
      this.label9.Size = new System.Drawing.Size(58, 16);
      this.label9.TabIndex = 14;
      this.label9.Text = "Protocol";
      // 
      // label8
      // 
      this.label8.AutoSize = true;
      this.label8.Location = new System.Drawing.Point(28, 34);
      this.label8.Name = "label8";
      this.label8.Size = new System.Drawing.Size(43, 16);
      this.label8.TabIndex = 12;
      this.label8.Text = "Scale";
      // 
      // btnSave
      // 
      this.btnSave.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnSave.Location = new System.Drawing.Point(145, 19);
      this.btnSave.Name = "btnSave";
      this.btnSave.Size = new System.Drawing.Size(106, 26);
      this.btnSave.TabIndex = 14;
      this.btnSave.Text = "Save";
      this.btnSave.UseVisualStyleBackColor = true;
      this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
      // 
      // btnStartService
      // 
      this.btnStartService.Enabled = false;
      this.btnStartService.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnStartService.Location = new System.Drawing.Point(257, 19);
      this.btnStartService.Name = "btnStartService";
      this.btnStartService.Size = new System.Drawing.Size(106, 26);
      this.btnStartService.TabIndex = 15;
      this.btnStartService.Text = "Start Service";
      this.btnStartService.UseVisualStyleBackColor = true;
      this.btnStartService.Click += new System.EventHandler(this.btnStartService_Click);
      // 
      // groupBox6
      // 
      this.groupBox6.Controls.Add(this.btnRestartService);
      this.groupBox6.Controls.Add(this.btnTest);
      this.groupBox6.Controls.Add(this.btnSave);
      this.groupBox6.Controls.Add(this.btnStartService);
      this.groupBox6.Location = new System.Drawing.Point(12, 386);
      this.groupBox6.Name = "groupBox6";
      this.groupBox6.Size = new System.Drawing.Size(748, 54);
      this.groupBox6.TabIndex = 18;
      this.groupBox6.TabStop = false;
      // 
      // btnRestartService
      // 
      this.btnRestartService.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnRestartService.Location = new System.Drawing.Point(378, 19);
      this.btnRestartService.Name = "btnRestartService";
      this.btnRestartService.Size = new System.Drawing.Size(106, 26);
      this.btnRestartService.TabIndex = 20;
      this.btnRestartService.Text = "Restart Service";
      this.btnRestartService.UseVisualStyleBackColor = true;
      this.btnRestartService.Click += new System.EventHandler(this.btnRestartService_Click);
      // 
      // btnTest
      // 
      this.btnTest.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.btnTest.Location = new System.Drawing.Point(600, 19);
      this.btnTest.Name = "btnTest";
      this.btnTest.Size = new System.Drawing.Size(106, 26);
      this.btnTest.TabIndex = 19;
      this.btnTest.Text = "Test";
      this.btnTest.UseVisualStyleBackColor = true;
      this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
      // 
      // frmScaleService
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(787, 471);
      this.Controls.Add(this.groupBox6);
      this.Controls.Add(this.groupBox5);
      this.Controls.Add(this.groupBox4);
      this.Controls.Add(this.groupBox1);
      this.Name = "frmScaleService";
      this.Text = "ScalesConfig";
      this.Load += new System.EventHandler(this.FrmScaleServiceLoad);
      this.groupBox1.ResumeLayout(false);
      this.groupBox1.PerformLayout();
      this.groupBoxTCP.ResumeLayout(false);
      this.groupBoxTCP.PerformLayout();
      this.groupBoxSerial.ResumeLayout(false);
      this.groupBoxSerial.PerformLayout();
      this.groupBox4.ResumeLayout(false);
      this.groupBox4.PerformLayout();
      this.groupBox5.ResumeLayout(false);
      this.groupBox5.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.numTolerance)).EndInit();
      this.groupBox6.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.GroupBox groupBoxTCP;
    private System.Windows.Forms.GroupBox groupBoxSerial;
    private System.Windows.Forms.Label label5;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label6;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.GroupBox groupBox4;
    private System.Windows.Forms.TextBox textReceivedData;
    private System.Windows.Forms.GroupBox groupBox5;
    private System.Windows.Forms.Label label11;
    private System.Windows.Forms.Label label10;
    private System.Windows.Forms.Label label9;
    private System.Windows.Forms.Label label8;
    private System.Windows.Forms.Button btnSave;
    private System.Windows.Forms.Button btnStartService;
    private System.Windows.Forms.Button btnStopService;
    private System.Windows.Forms.TextBox textCode;
    private System.Windows.Forms.TextBox textPort;
    private System.Windows.Forms.TextBox textIpAddress;
    private System.Windows.Forms.ComboBox cbxComName;
    private System.Windows.Forms.Label label13;
    private System.Windows.Forms.Label label12;
    private System.Windows.Forms.ComboBox cbxParity;
    private System.Windows.Forms.ComboBox cbxStopBits;
    private System.Windows.Forms.ComboBox cbxDataSize;
    private System.Windows.Forms.ComboBox cbxBaudRate;
    private System.Windows.Forms.ComboBox cbxProtocol;
    private System.Windows.Forms.Button btnOpen;
    private System.Windows.Forms.GroupBox groupBox6;
    private System.Windows.Forms.ComboBox cbxNameScale;
    private System.Windows.Forms.CheckBox chkSerial;
    private System.Windows.Forms.Button btnTest;
    private System.Windows.Forms.CheckBox chkEnabled;
    private System.Windows.Forms.TextBox txtName;
    private System.Windows.Forms.Label label7;
    private System.Windows.Forms.NumericUpDown numTolerance;
    private System.Windows.Forms.Button btnRestartService;
    private System.Windows.Forms.Button btnClear;
  }
}