﻿using Simplexity.Scales.Domain.Main;

namespace Simplexity.Scales.UI.Main
{
  partial class frmScales
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.lblScaleData = new System.Windows.Forms.ListBox();
      this.chkConnected = new System.Windows.Forms.CheckBox();
      this.txtWeight = new System.Windows.Forms.TextBox();
      this.label1 = new System.Windows.Forms.Label();
      this.label2 = new System.Windows.Forms.Label();
      this.txtName = new System.Windows.Forms.TextBox();
      this.label3 = new System.Windows.Forms.Label();
      this.txtCode = new System.Windows.Forms.TextBox();
      this.txtState = new System.Windows.Forms.TextBox();
      this.label4 = new System.Windows.Forms.Label();
      this.btnWeight = new System.Windows.Forms.Button();
      this.btnReconnect = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // lblScaleData
      // 
      this.lblScaleData.FormattingEnabled = true;
      this.lblScaleData.Location = new System.Drawing.Point(12, 127);
      this.lblScaleData.Name = "lblScaleData";
      this.lblScaleData.Size = new System.Drawing.Size(200, 134);
      this.lblScaleData.TabIndex = 1;
      // 
      // chkConnected
      // 
      this.chkConnected.AutoSize = true;
      this.chkConnected.Location = new System.Drawing.Point(218, 127);
      this.chkConnected.Name = "chkConnected";
      this.chkConnected.Size = new System.Drawing.Size(84, 17);
      this.chkConnected.TabIndex = 2;
      this.chkConnected.Text = "Conectada?";
      this.chkConnected.UseVisualStyleBackColor = true;
      // 
      // txtWeight
      // 
      this.txtWeight.Location = new System.Drawing.Point(49, 274);
      this.txtWeight.Name = "txtWeight";
      this.txtWeight.Size = new System.Drawing.Size(112, 20);
      this.txtWeight.TabIndex = 3;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(12, 274);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(34, 13);
      this.label1.TabIndex = 4;
      this.label1.Text = "Peso:";
      // 
      // label2
      // 
      this.label2.AutoSize = true;
      this.label2.Location = new System.Drawing.Point(15, 19);
      this.label2.Name = "label2";
      this.label2.Size = new System.Drawing.Size(47, 13);
      this.label2.TabIndex = 6;
      this.label2.Text = "Nombre:";
      // 
      // txtName
      // 
      this.txtName.CausesValidation = false;
      this.txtName.Location = new System.Drawing.Point(68, 19);
      this.txtName.Name = "txtName";
      this.txtName.Size = new System.Drawing.Size(120, 20);
      this.txtName.TabIndex = 5;
      // 
      // label3
      // 
      this.label3.AutoSize = true;
      this.label3.Location = new System.Drawing.Point(15, 45);
      this.label3.Name = "label3";
      this.label3.Size = new System.Drawing.Size(43, 13);
      this.label3.TabIndex = 8;
      this.label3.Text = "Código:";
      // 
      // txtCode
      // 
      this.txtCode.Location = new System.Drawing.Point(68, 45);
      this.txtCode.Name = "txtCode";
      this.txtCode.Size = new System.Drawing.Size(120, 20);
      this.txtCode.TabIndex = 7;
      // 
      // txtState
      // 
      this.txtState.Location = new System.Drawing.Point(68, 71);
      this.txtState.Name = "txtState";
      this.txtState.Size = new System.Drawing.Size(120, 20);
      this.txtState.TabIndex = 9;
      // 
      // label4
      // 
      this.label4.AutoSize = true;
      this.label4.Location = new System.Drawing.Point(15, 71);
      this.label4.Name = "label4";
      this.label4.Size = new System.Drawing.Size(43, 13);
      this.label4.TabIndex = 10;
      this.label4.Text = "Estado:";
      // 
      // btnWeight
      // 
      this.btnWeight.Location = new System.Drawing.Point(12, 315);
      this.btnWeight.Name = "btnWeight";
      this.btnWeight.Size = new System.Drawing.Size(75, 23);
      this.btnWeight.TabIndex = 0;
      this.btnWeight.Text = "GetWeight";
      this.btnWeight.UseVisualStyleBackColor = true;
      this.btnWeight.Click += new System.EventHandler(this.btnWeight_Click);
      // 
      // btnReconnect
      // 
      this.btnReconnect.Location = new System.Drawing.Point(12, 344);
      this.btnReconnect.Name = "btnReconnect";
      this.btnReconnect.Size = new System.Drawing.Size(75, 23);
      this.btnReconnect.TabIndex = 11;
      this.btnReconnect.Text = "Reconnect";
      this.btnReconnect.UseVisualStyleBackColor = true;
      this.btnReconnect.Click += new System.EventHandler(this.btnReconnect_Click);
      // 
      // frmScales
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(589, 450);
      this.Controls.Add(this.btnReconnect);
      this.Controls.Add(this.label4);
      this.Controls.Add(this.txtState);
      this.Controls.Add(this.label3);
      this.Controls.Add(this.txtCode);
      this.Controls.Add(this.label2);
      this.Controls.Add(this.txtName);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.txtWeight);
      this.Controls.Add(this.chkConnected);
      this.Controls.Add(this.lblScaleData);
      this.Controls.Add(this.btnWeight);
      this.Name = "frmScales";
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.frmScales_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.ListBox lblScaleData;
    private System.Windows.Forms.CheckBox chkConnected;
    private System.Windows.Forms.TextBox txtWeight;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label label2;
    private System.Windows.Forms.TextBox txtName;
    private System.Windows.Forms.Label label3;
    private System.Windows.Forms.TextBox txtCode;
    private System.Windows.Forms.TextBox txtState;
    private System.Windows.Forms.Label label4;
    private System.Windows.Forms.Button btnWeight;
    private System.Windows.Forms.Button btnReconnect;
  }
}

