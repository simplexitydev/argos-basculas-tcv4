﻿using System;
using System.Windows.Forms;

namespace Simplexity.Scales.UI.Main
{
  public class ThreadedBinding : Binding
  { 
    // ctors to match Binding
    public ThreadedBinding(string propertyName, object dataSource,
                           string dataMember)
      : base(propertyName, dataSource, dataMember)
    {
    }

    public ThreadedBinding(string propertyName, object dataSource,
                           string dataMember, bool formattingEnabled)
      : base(propertyName, dataSource, dataMember, formattingEnabled)
    {
    }

    public ThreadedBinding(string propertyName, object dataSource,
                           string dataMember, bool formattingEnabled, DataSourceUpdateMode
                                                                        dataSourceUpdateMode)
      : base(propertyName, dataSource, dataMember, formattingEnabled,
             dataSourceUpdateMode)
    {
    }

    public ThreadedBinding(string propertyName, object dataSource,
                           string dataMember, bool formattingEnabled, DataSourceUpdateMode
                                                                        dataSourceUpdateMode, object nullValue)
      : base(propertyName, dataSource, dataMember, formattingEnabled,
             dataSourceUpdateMode, nullValue)
    {
    }

    public ThreadedBinding(string propertyName, object dataSource,
                           string dataMember, bool formattingEnabled, DataSourceUpdateMode
                                                                        dataSourceUpdateMode, object nullValue,
                           string formatString)
      : base(propertyName, dataSource, dataMember, formattingEnabled,
             dataSourceUpdateMode, nullValue, formatString)
    {
    }

    public ThreadedBinding(string propertyName, object dataSource,
                           string dataMember, bool formattingEnabled, DataSourceUpdateMode
                                                                        dataSourceUpdateMode, object nullValue,
                           string formatString, IFormatProvider
                                                  formatInfo)
      : base(propertyName, dataSource, dataMember, formattingEnabled,
             dataSourceUpdateMode, nullValue, formatString, formatInfo)
    {
    }

    // main purpose; detect x-thread operations and compensate
    protected override void OnBindingComplete(BindingCompleteEventArgs e)
    {
      base.OnBindingComplete(e);
      if (e.BindingCompleteContext == BindingCompleteContext.ControlUpdate
          && e.BindingCompleteState == BindingCompleteState.Exception
          && e.Exception is InvalidOperationException)
      {
        if (Control != null && Control.InvokeRequired)
        {
          Control.BeginInvoke(new MethodInvoker(ReadValue));
        }
      }
    }
  }
}