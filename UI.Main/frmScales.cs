﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using Simplexity.Scales.Domain.Main;
using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.UI.Main
{
  public partial class frmScales : Form
  {
    private List<Scale> _scales;
    private Scale _scale1;

    public frmScales()
    {
      InitializeComponent();
    }

    private void frmScales_Load(object sender, EventArgs e)
    {

      // Load Scales 

      // 1. Crear una collección de básculas de acuerdo al archivo de configuración

      //var scales = new List<Scale>();

      //_scale1 = new Scale()
      //  {
      //    Name = "Scale1",
      //    Code = "CDM500",
      //    ComunicationMode = ComunicationMode.Com,
      //    CommunicationPort = new CommunicationPort("COM1", 2400, "None", "One", 7, CommunicationPort.TransmissionType.Hex),
      //    CurrentProtocol = "CDM500",
      //    Variation = 10,
      //  };

      //var scale2 = new Scale
      //{
      //  ComunicationMode = ComunicationMode.Com,
      //  CommunicationPort = new CommunicationPort("COM2", 9600, "None", "One", 7, CommunicationPort.TransmissionType.Hex),
      //  CurrentProtocol = "LP2"
      //};

      /*var scale3 = new Scale
      {
        ComunicationMode = ComunicationMode.Socket,
        SocketConnection = new SocketConnection("10.10.10.10", 8050),
        CurrentProtocol = "LP1"
      };*/

      //scales.Add(scale1);
      //scales.Add(scale2);
      //scales.Add(scale3);

      // 2. Cada báscula se carga en un component grafico en un hilo independiente

      // 3. iniciamos todas las básculas automáticamente (conexión) o le damos la opción al usuario que lo haga.

      //foreach (var scale in scales)
      //{
      //  scale.Connect();
      //}

      //_scales = SingletonScales.Instance;
      //_scales[0].Connect();


      //scaleBindingSource.DataSource = _scales[0];

      //scaleBindingSource.DataSource = new BindingList<Scale>(new List<Scale>() { _scales[0] });
      //scaleBindingSource.Add(_scales[0]);

      txtName.DataBindings.Add(new ThreadedBinding("Text", _scales[0], "Name", true,
                                                   DataSourceUpdateMode.OnPropertyChanged));


    }

    void frmScales_PropertyChanged(object sender, PropertyChangedEventArgs e)
    {
      //scaleBindingSource.ResetBindings(false);
    }

    private void btnWeight_Click(object sender, EventArgs e)
    {
      _scales[0].Name = "pepepe";

      txtWeight.Text = String.Empty;

      var weight = _scales[0].GetWeight();

      if (_scales[0].State == ScaleConstants.State.Stable)
      {
        txtWeight.Text = weight.ToString();
      }

      txtState.Text = _scales[0].State.ToString();
    }

    private void btnReconnect_Click(object sender, EventArgs e)
    {
    }

    private void scaleBindingSource_DataSourceChanged(object sender, EventArgs e)
    {
      var xx = "";

    }


  }
}
