﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Windows.Forms;
using Simplexity.Scales.Domain.Main.Communication;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.DTOs;
using Simplexity.Scales.UI.Main.ScalesServices;

namespace Simplexity.Scales.UI.Main
{
  public partial class frmScaleService : Form
  {
    private readonly CommunicationPort _communicationPort;
    private readonly ScaleServiceClient _scaleservice;
    private List<ScaleDTO> _listScales;
    private SocketConnection _socketConnection;

    public frmScaleService()
    {
      InitializeComponent();
      _communicationPort = new CommunicationPort();
      _scaleservice = new ScaleServiceClient();
      _listScales = new List<ScaleDTO>();
    }

    private void FrmScaleServiceLoad(object sender, EventArgs e)
    {
      InicializarControl();
      GetConfigScale();
    }

    private void GetConfigScale()
    {
      var listProtocols = new List<string>();

      try
      {
        listProtocols = _scaleservice.GetAvailableProtocols();
      }
      catch (Exception ex)
      {
        string xx = ex.Message;
      }


      if (listProtocols == null) return;

      cbxProtocol.DataSource = listProtocols;

      _listScales = _scaleservice.GetScales();
      if (_listScales == null) return;


      cbxNameScale.DisplayMember = "Code";
      cbxNameScale.ValueMember = "Code";
      cbxNameScale.DataSource = _listScales;

      Actualizar(_listScales[0]);
    }

    private void Actualizar(ScaleDTO scaleDto)
    {
      cbxNameScale.SelectedItem = scaleDto.Code;
      chkEnabled.Checked = scaleDto.Enabled;

      textCode.Text = scaleDto.Code;
      numTolerance.Value = (decimal) (scaleDto.WeightVariation != null ? scaleDto.WeightVariation : 0);
      cbxProtocol.SelectedItem = scaleDto.CurrentProtocol;
      txtName.Text = scaleDto.Name;

      if (scaleDto.ComunicationMode.ToLower() == "com" && scaleDto.ComPortInfo != null)
      {
        chkSerial.Checked = true;
        EnabledSerialGroup();
        cbxComName.SelectedItem = scaleDto.ComPortInfo.PortName;
        cbxBaudRate.SelectedItem = scaleDto.ComPortInfo.BaudRate;
        cbxDataSize.SelectedItem = scaleDto.ComPortInfo.DataBits;
        cbxStopBits.SelectedItem = scaleDto.ComPortInfo.StopBits;
        cbxParity.SelectedItem = scaleDto.ComPortInfo.Parity;
      }

      if (scaleDto.ComunicationMode.ToLower() == "socket" && scaleDto.SocketInfo != null)
      {
        chkSerial.Checked = false;
        EnabledSerialGroup();
        textIpAddress.Text = scaleDto.SocketInfo.Address;
        textPort.Text = scaleDto.SocketInfo.Port;
      }
    }

    private void InicializarControl()
    {

      var serialPorts = SerialPort.GetPortNames();

      if (serialPorts.Any())
      {
        cbxComName.DataSource = SerialPort.GetPortNames();
        cbxComName.SelectedItem = cbxComName.Items[0];
      }
     
      
      cbxBaudRate.SelectedItem = cbxBaudRate.Items[5];
      cbxDataSize.SelectedItem = cbxDataSize.Items[0];

      cbxStopBits.DataSource = Enum.GetValues(typeof (StopBits)).Cast<StopBits>().Select(v => v.ToString()).ToArray();
      cbxStopBits.SelectedItem = cbxStopBits.Items[0];
      cbxParity.DataSource = Enum.GetValues(typeof (Parity)).Cast<Parity>().Select(v => v.ToString()).ToArray();
      cbxParity.SelectedItem = cbxParity.Items[0];
    }

    private void BtnOpenClick(object sender, EventArgs e)
    {
      if (btnOpen != null)
        switch (btnOpen.Text)
        {
          case "Open":
            btnOpen.Text = "Close";
            if (_communicationPort.IsOpen()) return;
            _communicationPort.PortName = cbxComName.SelectedItem.ToString();
            _communicationPort.BaudRate = Convert.ToInt32((cbxBaudRate.SelectedItem.ToString()));
            _communicationPort.DataBits = Convert.ToInt32((cbxDataSize.SelectedItem.ToString()));
            _communicationPort.StopBits = cbxStopBits.SelectedItem.ToString();
            _communicationPort.Parity = cbxParity.SelectedItem.ToString();
            _communicationPort.CurrentTransmissionType = ComConstants.TransmissionType.Hex;
            _communicationPort.EndByte = 0x0D;
            _communicationPort.FrameReceived += _communicationPort_FrameReceived;
            _communicationPort.Connect();
            break;
          case "Close":
            btnOpen.Text = "Open";
            if (!_communicationPort.IsOpen()) return;
            _communicationPort.Close();
            break;
        }
    }

    private void _communicationPort_FrameReceived(byte[] frame)
    {
    }

    private void cbxNameScale_SelectedIndexChanged(object sender, EventArgs e)
    {
      var selectedDTO = (ScaleDTO) cbxNameScale.SelectedItem;

      if (selectedDTO != null)
      {
        Actualizar(selectedDTO);
      }
    }

    private void btnSave_Click(object sender, EventArgs e)
    {
      DialogResult dialogResult;

      var scaleDTO = (ScaleDTO) cbxNameScale.SelectedItem;

      dialogResult = !chkEnabled.Checked
                       ? MessageBox.Show(
                         string.Format("Desea guardar los cambios e INACTIVAR la báscula {0}?", scaleDTO.Code),
                         "Salvar cambios?", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation,
                         MessageBoxDefaultButton.Button2)
                       : MessageBox.Show(
                         string.Format("Desea guardar los cambios e ACTIVAR la báscula {0}?", scaleDTO.Code),
                         "Salvar cambios?", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                         MessageBoxDefaultButton.Button2);

      if (dialogResult == DialogResult.Yes)
      {
        UpdateConfig(scaleDTO);
        UpdateScalesCodes(scaleDTO);
      }
    }

    private void chkSerial_CheckedChanged(object sender, EventArgs e)
    {
      EnabledSerialGroup();
    }

    private void EnabledSerialGroup()
    {
      groupBoxSerial.Enabled = chkSerial.Checked;
      groupBoxTCP.Enabled = !chkSerial.Checked;
    }


    private void UpdateConfig(ScaleDTO scaleDto)
    {
      if (scaleDto == null)
      {
        return;
      }

      scaleDto.Code = scaleDto.Code;
      scaleDto.NewCode = textCode.Text;
      scaleDto.CurrentProtocol = cbxProtocol.SelectedItem.ToString();
      scaleDto.Enabled = chkEnabled.Checked;
      scaleDto.Name = txtName.Text;
      scaleDto.WeightVariation = (double) numTolerance.Value;

      if (chkSerial.Checked)
      {
        if (cbxComName.Items.Count == 0)
        {
          MessageBox.Show(
            "No hay puertos COM configurados en este computador. Por favor verifiquelos e intente nuevamente.",
            "Salvar cambios", MessageBoxButtons.OK, MessageBoxIcon.Warning);
          return;
        }

        scaleDto.ComunicationMode = "com";
        scaleDto.SocketInfo = null;
        scaleDto.ComPortInfo = new ComPortDTO
          {
            BaudRate = int.Parse(cbxBaudRate.SelectedItem.ToString()),
            DataBits = int.Parse(cbxDataSize.SelectedItem.ToString()),
            Parity = cbxParity.SelectedItem.ToString(),
            PortName = cbxComName.SelectedItem.ToString(),
            StopBits = cbxStopBits.SelectedItem.ToString(),
            TransmissionType = "hex"
          };
      }
      else
      {
        scaleDto.ComunicationMode = "socket";
        scaleDto.ComPortInfo = null;
        scaleDto.SocketInfo = new SocketDTO
          {
            Address = textIpAddress.Text,
            Port = textPort.Text
          };
      }

      MessageDTO messageDTO = _scaleservice.SetScalesConfiguration(_listScales);

      if (messageDTO != null)
      {
        if (messageDTO.TypeEnum == MessagesConstants.Types.Information)
        {
          MessageBox.Show("Información actualizada con éxito. Por favor reinicie el servicio");
        }
        else
        {
          MessageBox.Show(messageDTO.Message);
        }
      }
    }


    private void btnStartService_Click(object sender, EventArgs e)
    {
      switch (btnStartService.Text)
      {
        case "Start Service":
          CheckServicesStatus("Simplexity.Scales.Service", ServiceControllerStatus.Running, 10000);
          btnStartService.Text = "Stop Service";
          break;
        case "Stop Service":
          CheckServicesStatus("Simplexity.Scales.Service", ServiceControllerStatus.Stopped, 10000);
          btnStartService.Text = "Start Service";
          break;
      }
    }


    /// <summary>
    /// Ckeck Service Status and try to Stat it up
    /// </summary>
    /// <param name="serviceName">Service Name</param>
    /// <param name="timeoutMilliseconds">Time to wait  service to start until give timeout</param>
    public void CheckServicesStatus(string serviceName, ServiceControllerStatus status, int timeoutMilliseconds)
    {
      var sc = new ServiceController(serviceName);
      TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

      try
      {
        if (status == ServiceControllerStatus.Running)
        {
          switch (sc.Status)
          {
            case ServiceControllerStatus.Paused:
              sc.Continue();
              sc.WaitForStatus(status, timeout);
              break;
            case ServiceControllerStatus.Stopped:
              sc.Start();
              sc.WaitForStatus(status, timeout);
              break;
          }
        }
        if (status == ServiceControllerStatus.Stopped)
        {
          switch (sc.Status)
          {
            case ServiceControllerStatus.Paused:
              sc.Continue();
              sc.WaitForStatus(status, timeout);
              break;
            case ServiceControllerStatus.Running:
              sc.Start();
              sc.WaitForStatus(status, timeout);
              break;
          }
        }
      }
      catch (InvalidOperationException)
      {
      }
    }


    /// <summary>
    /// Ckeck Service Status and try to Stat it up
    /// </summary>
    /// <param name="serviceName">Service Name</param>
    /// <param name="timeoutMilliseconds">Time to wait  service to start until give timeout</param>
    public void RestartService(string serviceName, int timeoutMilliseconds)
    {
      var sc = new ServiceController(serviceName);
      TimeSpan timeout = TimeSpan.FromMilliseconds(timeoutMilliseconds);

      try
      {
        switch (sc.Status)
        {
          case ServiceControllerStatus.Paused:
            sc.Continue();
            sc.WaitForStatus(ServiceControllerStatus.Running, timeout);
            break;
          case ServiceControllerStatus.Stopped:
            sc.Start();
            sc.WaitForStatus(ServiceControllerStatus.Running, timeout);
            break;
          case ServiceControllerStatus.Running:
            sc.Stop();
            sc.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
            sc.Start();
            sc.WaitForStatus(ServiceControllerStatus.Running, timeout);
            break;
        }
      }
      catch (InvalidOperationException)
      {
      }
    }

    private void UpdateScalesCodes(ScaleDTO scaleDto)
    {
      //cbxNameScale.ResetBindings();
    }

    private void btnRestartService_Click(object sender, EventArgs e)
    {
      btnRestartService.Enabled = false;

      RestartService("Simplexity.Scales.Service", 60000);

      _listScales = _scaleservice.GetScales();
      if (_listScales == null) return;


      cbxNameScale.DataSource = _listScales;
      cbxNameScale.ResetBindings();
      textReceivedData.Text = string.Empty;

      MessageBox.Show("Servicio reiniciado exitosamente");

      btnRestartService.Enabled = true;
    }

    private void btnTest_Click(object sender, EventArgs e)
    {

      WeightDTO weightDto;

      var scaleDTO = (ScaleDTO)cbxNameScale.SelectedItem;

      try
      {
        weightDto = _scaleservice.GetWeight(scaleDTO.Code);
      }
      catch (Exception exception)
      {

        MessageBox.Show(string.Format("El servicio retornó el siguiente mensaje: {0}", exception.Message), "Test báscula", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);

        return;

      }
      

      /*
      Unstable = 0,
      Stable = 1,
      Overloaded = 2,
      Disabled = 3,
      Undefined
      */
      var stringTest = new StringBuilder();

      switch (weightDto.State)
      {
        case 0:
          stringTest.AppendLine(string.Format("La báscula {0} no está estable", scaleDTO.Code));
          stringTest.AppendLine(weightDto.Weight.ToString());
          textReceivedData.Text = stringTest.ToString();
          break;
        case 1:
          stringTest.AppendLine(textReceivedData.Text);
          stringTest.AppendLine(weightDto.Weight.ToString());
          textReceivedData.Text = stringTest.ToString();
          break;
        case 2:
          MessageBox.Show(string.Format("La báscula {0} está sobrecargada. Peso: {1}", scaleDTO.Code, weightDto.Weight), "Test báscula", MessageBoxButtons.OK,
                          MessageBoxIcon.Exclamation);
          break;
        case 3:
          MessageBox.Show(string.Format("La báscula {0} está deshabilitada", scaleDTO.Code), "Test báscula", MessageBoxButtons.OK,
                          MessageBoxIcon.Exclamation);

          break;
        case 4:
          MessageBox.Show(string.Format("La báscula {0} retorno un estado desconocido", scaleDTO.Code), "Test báscula", MessageBoxButtons.OK,
                          MessageBoxIcon.Warning);


          break;


      }

    }

    private void btnClear_Click(object sender, EventArgs e)
    {
      textReceivedData.Text = String.Empty;
    }


  }
}