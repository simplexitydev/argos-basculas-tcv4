﻿using Spring.Context;
using Spring.Context.Support;

namespace Simplexity.Scales.Domain.Main
{
  public static class Spring
  {
    public static object GetObject(string objectName, string context)
    {
      //IApplicationContext ctx = new XmlApplicationContext("file://SpringConfiguration.xml");
      var ctx = ContextRegistry.GetContext();
      return ctx.GetObject(objectName);
    }

    public static object GetObjectDefinitionNames()
    {
      var ctx = ContextRegistry.GetContext();
      return ctx.GetObjectDefinitionNames();
    }
  }
}
