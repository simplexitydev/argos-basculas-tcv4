﻿using System.Collections.Generic;
using Simplexity.Scales.Domain.Main;

namespace Simplexity.Scales.Services.Main
{
  public sealed class SingletonScales
  {
    private static readonly List<Scale> _instance = new List<Scale>();

    private SingletonScales()
    {
    }

    public static List<Scale> Instance
    {
      get { return _instance; }
    }
  }
}