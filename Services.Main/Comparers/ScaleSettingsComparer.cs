﻿using System.Collections;
using Simplexity.Scales.Services.Main.Settings;

namespace Simplexity.Scales.Services.Main.Comparers
{
  /// <summary>
  /// SmtpServerSettingsComparer allows sorting of SmtpServerSettings.
  /// </summary>
  internal sealed class ScaleSettingsComparer : IComparer
  {
    #region ScaleSettingsSortColumns enum

    public enum ScaleSettingsSortColumns
    {
      None = 0,
      ExecuteOrder = 1,
      Code = 2
    }

    #endregion

    private readonly ScaleSettingsSortColumns _sortValue = ScaleSettingsSortColumns.None;

    public ScaleSettingsComparer(ScaleSettingsSortColumns sortValue)
    {
      _sortValue = sortValue;
    }

    #region IComparer Members

    public int Compare(object x, object y)
    {
      switch (_sortValue)
      {
        case ScaleSettingsSortColumns.None:
          return 0;

        case ScaleSettingsSortColumns.Code:
          return ((ScaleSettings) x).Code.CompareTo(((ScaleSettings) y).Code);
          //break;

        case ScaleSettingsSortColumns.ExecuteOrder:
          return ((ScaleSettings) x).ExecuteOrder.CompareTo(((ScaleSettings) y).ExecuteOrder);

        default:
          return ((ScaleSettings) x).Code.CompareTo(((ScaleSettings) y).Code);
          // break;
      }
    }

    #endregion
  }
}