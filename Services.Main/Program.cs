﻿using System;
using Topshelf;

namespace Simplexity.Scales.Services.Main
{
  class Program
  {
      private static void Main(string[] args)
	  {
          try
          {
              Console.WriteLine("INICIO SERVICIO!!");
              LogBase.LogMessageToFile("INICIO EL SERVICIO!!");
              HostFactory.Run(x =>
              {
                  x.Service<ScalesService>(s =>
                  {
                      s.ConstructUsing(name => new ScalesService());
                      s.WhenStarted(tc => tc.StartWindowsService());
                      s.WhenStopped(tc => tc.StopWindowsService());
                  });
                  // x.StartAutomaticallyDelayed();
                  x.StartAutomatically();
                  x.RunAsLocalSystem();
                  x.SetDescription("Servicio que se encarga de controlar las básculas");
                  x.SetDisplayName("Simplexity.Scales.Service");
                  x.SetServiceName("Simplexity.Scales.Service");
                  
              });
              Console.ReadLine();
          }
          catch (Exception ex)
          {
              LogBase.LogMessageToFile("Error Program.cs->" + ex.ToString());
          }
	  }
  }
}