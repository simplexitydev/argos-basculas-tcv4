﻿using System;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.DTOs;

namespace Simplexity.Scales.Services.Main.ErrorHandler
{
  public static class ExceptionHandler
  {
    public static MessageDTO HandleException(Exception ex)
    {
      var message = new MessageDTO {TypeEnum = MessagesConstants.Types.Error};

      var exManager = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();

      if (ex is InvalidOperationException)
      {
        exManager.HandleException(ex, "WCF InvalidOperation");

        message.Message = ex.Message;
      }
      else if (ex is ArgumentException || ex is FormatException)
      {
        exManager.HandleException(ex, "WCF ArgumentException");

        message.Message = ex.Message;
      }
      else
      {
        Exception newException;
        bool booResult = exManager.HandleException(ex, "WCF Exception Shielding", out newException);
        if (!booResult)
        {
          message.Message = "ConnectionError cannot be handled";
          return message;
        }
        message.Message = newException.Message;
      }
      return message;
    }
  }
}