﻿using System;
using System.Runtime.Serialization;

namespace Simplexity.Scales.Services.Main.ErrorHandler
{
  [DataContract]
  public class ServiceFaultException
  {
    [DataMember]
    public Guid FaultID { get; set; }

    [DataMember]
    public string FaultMessage { get; set; }
  }
}