﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace Simplexity.Scales.Services.Main
{
    public static class LogBase
    {
        public static string GetTempPath()
        {
            // var path = System.IO.Directory.GetCurrentDirectory();
            // var path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var path = ConfigurationManager.AppSettings["DirLogs"]; //  @"D:\ScaleService";
            return path;
        }

        /// <summary>
        /// Log de eventos. Queda en el directorio de la app + LogSPX
        /// </summary>
        /// <param name="msg">Mensaje</param>
        public static void LogMessageToFile(string msg)
        {
            System.IO.StreamWriter sw = null;
            try
            {
                //+ @"\LogSPX"
                var pathSel = GetTempPath() + @"\Logs";

                var verificaDir = Directory.Exists(pathSel);
                if (!verificaDir)
                {
                    Directory.CreateDirectory(pathSel);
                }

                pathSel = pathSel + @"\" + DateTime.Now.Year;

                verificaDir = Directory.Exists(pathSel);
                if (!verificaDir)
                {
                    Directory.CreateDirectory(pathSel);
                }

                pathSel = pathSel + @"\" + DateTime.Now.Month;
                verificaDir = Directory.Exists(pathSel);
                if (!verificaDir)
                {
                    Directory.CreateDirectory(pathSel);
                }

                pathSel = pathSel + @"\" + DateTime.Now.Day;
                verificaDir = Directory.Exists(pathSel);
                if (!verificaDir)
                {
                    Directory.CreateDirectory(pathSel);
                }

                sw = System.IO.File.AppendText(
                 pathSel + @"\" + DateTime.Now.ToString("yyyyMMdd") + ".txt");
                string logLine = System.String.Format(
                  "{0:G}: {1}.", System.DateTime.Now, msg);
                sw.WriteLine(logLine);
            }
            catch (Exception ex)
            {


            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }

        }
    }
}
