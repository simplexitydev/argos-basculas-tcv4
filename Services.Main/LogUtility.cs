﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Simplexity.Scales.Services.Main
{
    public static class LogUtility
    {
        public static void LogInfo(string cad, string module = "*")
        {

            Console.BackgroundColor = ConsoleColor.Blue;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("[ " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " ]");
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" " + cad + "\r\n");
        }
        public static void LogError(string cad, string module = "*")
        {
            Console.BackgroundColor = ConsoleColor.Red;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("[ " + DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + " ]");
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(" " + cad + "\r\n");
        }
    }
}
