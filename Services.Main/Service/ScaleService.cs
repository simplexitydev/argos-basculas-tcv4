﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Xml;
using Simplexity.Scales.Domain.Main;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.DTOs;
using Simplexity.Scales.Domain.Main.Protocols;
using Simplexity.Scales.Services.Main.ErrorHandler;

namespace Simplexity.Scales.Services.Main.Service
{
  [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
  public class ScaleService : IScaleService
  {
    private const string CurrentProcess = "Simplexity.Scales.Service";

    #region Public Methods

    public WeightDTO GetWeight(string scaleCode)
    {
      var weightDto = new WeightDTO();

      List<Scale> scales = SingletonScales.Instance;

      if (scales == null || scales.Count == 0)
      {
        weightDto.Message.Message = "No scales are configured and/or enabled in this computer.";
        weightDto.Message.TypeEnum = MessagesConstants.Types.Warning;
        return weightDto;
      }

      Scale scale = scales.FirstOrDefault(p => p.Code == scaleCode);

      if (scale == null)
      {
        weightDto.Message.Message =
          string.Format("Cannot find scale with code ({0}) configured and/or enabled in this computer.", scaleCode);
        weightDto.Message.TypeEnum = MessagesConstants.Types.Warning;
        return weightDto;
      }

      try
      {
        weightDto.Weight = scale.GetWeight();
        weightDto.State = (int) scale.State;
      }
      catch (Exception ex)
      {
        weightDto.Message = ExceptionHandler.HandleException(ex);
      }

      return weightDto;
    }

    public List<ScaleDTO> GetScales()
    {
      var availableScales = new List<ScaleDTO>();

      List<Scale> scales = SingletonScales.Instance;

      if (scales == null || scales.Count == 0)
      {
        return availableScales;
      }

      foreach (Scale scale in scales)
      {
        var scaleDTO = new ScaleDTO
          {
            Code = scale.Code,
            Enabled = scale.Enabled,
            Name = scale.Name,
            ComunicationMode = scale.ComunicationMode.ToString(),
            CurrentProtocol = scale.CurrentProtocol,
            AvailableProtocols = scale.AvailableProtocols,
            WeightVariation = scale.WeightVariation,
          };

        if (scale.CommunicationPort != null)
        {
          scaleDTO.ComPortInfo = new ComPortDTO
            {
              BaudRate = scale.CommunicationPort.BaudRate,
              DataBits = scale.CommunicationPort.DataBits,
              Parity = scale.CommunicationPort.Parity,
              PortName = scale.CommunicationPort.PortName,
              StopBits = scale.CommunicationPort.StopBits,
              TransmissionType = scale.CommunicationPort.CurrentTransmissionType.ToString()
            };
        }

        if (scale.SocketConnection != null)
        {
          scaleDTO.SocketInfo = new SocketDTO
            {
              Address = scale.SocketConnection.IpAddress,
              Port = scale.SocketConnection.Port.ToString()
            };
        }

        availableScales.Add(scaleDTO);
      }

      return availableScales;
    }

    public MessageDTO SetScalesConfiguration(List<ScaleDTO> scalesDTO)
    {
      var messageDTO = new MessageDTO();

      if (scalesDTO == null || scalesDTO.Count == 0)
      {
        messageDTO.Message = "No information was provided for the service. parameter scalesDTO is null or empty";
        messageDTO.TypeEnum = MessagesConstants.Types.Warning;
      }

      List<Scale> scales = SingletonScales.Instance;

      if (scales == null || scales.Count == 0)
      {
        messageDTO.Message = "No scales are configured and/or enabled in this computer.";
        messageDTO.TypeEnum = MessagesConstants.Types.Warning;
        return messageDTO;
      }

      foreach (ScaleDTO scaleDTO in scalesDTO)
      {
        Scale scale = scales.FirstOrDefault(p => p.Code == scaleDTO.Code);

        if (scale == null)
        {
          messageDTO.Message =
            string.Format("Cannot find scale with code ({0}) configured and/or enabled in this computer.", scaleDTO.Code);
          messageDTO.TypeEnum = MessagesConstants.Types.Warning;
          return messageDTO;
        }

        // Actualice las propiedades
        try
        {
          #region Update Settings

          if (!string.IsNullOrEmpty(scaleDTO.Enabled.ToString()))
          {
            UpdateScaleSettings(scale.Code, "enabled", scaleDTO.Enabled.ToString());
          }

          if (!string.IsNullOrEmpty(scaleDTO.Name))
          {
            UpdateScaleSettings(scale.Code, "name", scaleDTO.Name);
          }

          if (!string.IsNullOrEmpty(scaleDTO.CurrentProtocol))
          {
            UpdateScaleSettings(scale.Code, "protocol", scaleDTO.CurrentProtocol);
          }
          if (!string.IsNullOrEmpty(scaleDTO.WeightVariation.ToString()))
          {
            UpdateScaleSettings(scale.Code, "weightVariation", scaleDTO.WeightVariation.ToString());
          }


          if (scaleDTO.ComunicationMode.ToLower() == "com" && scaleDTO.ComPortInfo != null)
          {
            UpdateScaleSettings(scale.Code, "communicationMode", scaleDTO.ComunicationMode);
            UpdateScaleSettings(scale.Code, "comName", scaleDTO.ComPortInfo.PortName);
            UpdateScaleSettings(scale.Code, "baudRate", scaleDTO.ComPortInfo.BaudRate.ToString());
            UpdateScaleSettings(scale.Code, "parity", scaleDTO.ComPortInfo.Parity);
            UpdateScaleSettings(scale.Code, "dataBits", scaleDTO.ComPortInfo.DataBits.ToString());
            UpdateScaleSettings(scale.Code, "stopBits", scaleDTO.ComPortInfo.StopBits);
            UpdateScaleSettings(scale.Code, "transmissionType", scaleDTO.ComPortInfo.TransmissionType);

            ClearCommunicationMode(scaleDTO.Code, "socket");
          }
          if (scaleDTO.ComunicationMode.ToLower() == "socket" && scaleDTO.SocketInfo != null)
          {
            ClearCommunicationMode(scaleDTO.Code, "com");
            UpdateScaleSettings(scale.Code, "communicationMode", scaleDTO.ComunicationMode);
            UpdateScaleSettings(scale.Code, "socketIpAddress", scaleDTO.SocketInfo.Address);
            UpdateScaleSettings(scale.Code, "socketPort", scaleDTO.SocketInfo.Port);
          }

          UpdateScaleSettings(scale.Code, "code", scaleDTO.NewCode);
          ConfigurationManager.RefreshSection("ScaleConfiguration/Scale");

          #endregion
        }
        catch (Exception ex)
        {
          messageDTO = ExceptionHandler.HandleException(ex);
        }
      }

      return messageDTO;
    }

    public List<string> GetAvailableProtocols()
    {
      var list = new List<string>();

      IEnumerable<Type> decryptTypes = TypesImplementingInterface(typeof (IDecryptable));

      if (decryptTypes != null)

        foreach (Type type in decryptTypes)
        {
          if (IsRealClass(type))
          {
            list.Add(type.Name);
          }
        }

      return list;
    }

    public MessageDTO Restart()
    {
      var messageDTO = new MessageDTO();

      string location = Assembly.GetEntryAssembly().Location;
      string directoryPath = Path.GetDirectoryName(location);

      try
      {

        ReleaseSocketConnection();

        var startInfo = new ProcessStartInfo
          {FileName = directoryPath + @"\Restart.bat", Arguments = string.Format("{0} {1}", CurrentProcess, 10000)};

        Process process = Process.Start(startInfo);
        messageDTO.Message = "Restart command will take place in 10 seconds";
      }
      catch (Exception ex)
      {
        messageDTO = ExceptionHandler.HandleException(ex);
      }


      return messageDTO;
    }

    public List<AssemblyDTO> GetVersion()
    {
      var assemblies = new List<AssemblyDTO>();

      Assembly currentAssembly = Assembly.GetExecutingAssembly();
      FileVersionInfo fileVersionInfo = FileVersionInfo.GetVersionInfo(currentAssembly.Location);

      Version version = currentAssembly.GetName().Version;
      string assemblyVersion = String.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build,
                                             version.Revision);

      assemblies.Add(new AssemblyDTO
        {
          Name = currentAssembly.GetName().Name,
          FullName = currentAssembly.FullName,
          AssemblyVersion = assemblyVersion,
          FileVersion = fileVersionInfo.FileVersion
        });

      AssemblyName domainName =
        currentAssembly.GetReferencedAssemblies().FirstOrDefault(p => p.Name.Contains("Domain.Main"));

      if (domainName != null)

      {
        Assembly domainAssembly = Assembly.Load(domainName);
        fileVersionInfo = FileVersionInfo.GetVersionInfo(domainAssembly.Location);

        version = domainAssembly.GetName().Version;
        assemblyVersion = String.Format("{0}.{1}.{2}.{3}", version.Major, version.Minor, version.Build, version.Revision);

        assemblies.Add(new AssemblyDTO
          {
            Name = domainAssembly.GetName().Name,
            FullName = domainAssembly.FullName,
            AssemblyVersion = assemblyVersion,
            FileVersion = fileVersionInfo.FileVersion
          });
      }

      return assemblies;
    }

    #endregion

    #region Private Methods

    private void UpdateScaleSettings(string scaleCode, string keyName, string keyValue)
    {
      if (string.IsNullOrEmpty(scaleCode) || string.IsNullOrEmpty(scaleCode) || string.IsNullOrEmpty(keyValue))
      {
        return;
      }

      var xmlDoc = new XmlDocument();
      xmlDoc.Load(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);

      XmlNode node = xmlDoc.SelectSingleNode("//ScaleConfiguration/scale[@code='" + scaleCode + "']");

      if (node == null)
      {
        return;
      }

      if (node.Attributes[keyName] == null)
      {
        return;
      }

      node.Attributes[keyName].Value = keyValue;

      xmlDoc.Save(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile);
    }

    private IEnumerable<Type> TypesImplementingInterface(Type desiredType)
    {
      return AppDomain
        .CurrentDomain
        .GetAssemblies()
        .SelectMany(assembly => assembly.GetTypes())
        .Where(type => desiredType.IsAssignableFrom(type));
    }

    private bool IsRealClass(Type testType)
    {
      return testType.IsAbstract == false
             && testType.IsGenericTypeDefinition == false
             && testType.IsInterface == false;
    }

    private void ClearCommunicationMode(string scaleCode, string communicationMode)
    {
      switch (communicationMode)
      {
        case "com":
          UpdateScaleSettings(scaleCode, "communicationMode", string.Empty);
          UpdateScaleSettings(scaleCode, "comName", string.Empty);
          UpdateScaleSettings(scaleCode, "baudRate", string.Empty);
          UpdateScaleSettings(scaleCode, "parity", string.Empty);
          UpdateScaleSettings(scaleCode, "dataBits", string.Empty);
          UpdateScaleSettings(scaleCode, "stopBits", string.Empty);
          break;
        case "socket":
          UpdateScaleSettings(scaleCode, "communicationMode", string.Empty);
          UpdateScaleSettings(scaleCode, "socketIpAddress", string.Empty);
          UpdateScaleSettings(scaleCode, "socketPort", string.Empty);
          break;
      }
    }

    private void ReleaseSocketConnection()
    {
      foreach (var scale in SingletonScales.Instance)
      {
        scale.Disconnect();
      }
    }

    #endregion
  }
}