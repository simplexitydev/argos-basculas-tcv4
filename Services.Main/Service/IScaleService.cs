﻿using System.Collections.Generic;
using System.ServiceModel;
using Simplexity.Scales.Domain.Main.DTOs;

namespace Simplexity.Scales.Services.Main.Service
{
  // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface portName "IScaleService" in both code and config file together.
  [ServiceContract]
  public interface IScaleService
  {
    [OperationContract]
    WeightDTO GetWeight(string scaleCode);

    [OperationContract]
    List<ScaleDTO> GetScales();

    [OperationContract]
    MessageDTO SetScalesConfiguration(List<ScaleDTO> scales);

    [OperationContract]
    List<string> GetAvailableProtocols();

    [OperationContract]
    MessageDTO Restart();

    [OperationContract]
    List<AssemblyDTO> GetVersion();
  }
}