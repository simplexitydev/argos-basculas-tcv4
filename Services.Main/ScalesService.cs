﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.ServiceModel;
using System.Threading.Tasks;
using System.Timers;
using Simplexity.Scales.Domain.Main;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.DTOs;
using Simplexity.Scales.Services.Main.ErrorHandler;
using Simplexity.Scales.Services.Main.Service;
using Simplexity.Scales.Services.Main.Settings;

namespace Simplexity.Scales.Services.Main
{
  public class ScalesService
  {
    private const string _configSectionName = "ScaleConfiguration";
    private static ScaleConfiguration _scaleConfiguration;
    private static ScaleSettingsCollection _scaleSettingsCollection;
    private static ServiceHost _wcfServiceHost;
    private Timer _scalesConnectionTimer;
    private Timer _tinicio;

    public ScalesService()
    {
      _tinicio = new Timer();
      _tinicio.Elapsed += _tinicio_Elapsed;
      _tinicio.Interval = 20000;
      _tinicio.Enabled = false;
    }

    void _tinicio_Elapsed(object sender, ElapsedEventArgs e)
    {
      _tinicio.Enabled = false;
      LogUtility.LogInfo("INICIO: StartWindowsService!!");
      LogBase.LogMessageToFile("INICIO: StartWindowsService!!");
      GetSettings();
      StartScales();
        LogUtility.LogInfo("INICIO DE SERVICIO BASCULAS OK!!");
        try
      {
                LogUtility.LogInfo("INICIANDO SERVICIO WCF!!");
                StartWCFService();
                LogUtility.LogInfo("SERVICIO WCF OK!!");
            }
      catch (Exception ex)
      {
            LogUtility.LogError("Error: StartWCFService!!" + ex.ToString());
            LogBase.LogMessageToFile("Error: StartWCFService!!" + ex.ToString());
      }
     
      LogBase.LogMessageToFile("TERMINO: StartWindowsService!!");
    }

    public void StartWindowsService()
    {
      _tinicio.Enabled = true;
    }

    public void StopWindowsService()
		{
		  try
		  {
        ReleaseResources();
		  }
		  catch (Exception)
		  {
		    
		    //throw;
		  }
			
		}

    private void StartScales()
    {
        try
        {
            List<Scale> scales = SingletonScales.Instance;

            for (int i = 0; i < _scaleSettingsCollection.Count; i++)
            {
                var scale = new Scale(_scaleSettingsCollection[i].Code, _scaleSettingsCollection[i].Name,
                                      _scaleSettingsCollection[i].Protocol, _scaleSettingsCollection[i].CommunicationMode,
                                      _scaleSettingsCollection[i].WeightVariation, _scaleSettingsCollection[i].Enabled);

                scales.Add(scale);

                switch (scale.ComunicationMode)
                {
                    case ScaleConstants.ComunicationMode.Com:
                        scale.AddComPort(_scaleSettingsCollection[i].ComName,
                                         _scaleSettingsCollection[i].BaudRate,
                                         _scaleSettingsCollection[i].Parity,
                                         _scaleSettingsCollection[i].StopBits,
                                         _scaleSettingsCollection[i].DataBits,
                                         _scaleSettingsCollection[i].TransmissionType);

                        break;
                    case ScaleConstants.ComunicationMode.Socket:
                        scale.AddSocket(_scaleSettingsCollection[i].SocketIpAddress, _scaleSettingsCollection[i].SocketPort);
                        break;
                }

                if (!scale.Enabled) continue;

                scale.FrameReceivedError += scaleFrameReceivedError;

                // KGC AQUI DEBE IR EL LOG
            }

            // Delay Scales connection on stat
            _scalesConnectionTimer = new Timer(5000);
            _scalesConnectionTimer.Elapsed += ScalesConnectionTimerElapsed;
            _scalesConnectionTimer.Enabled = true;
            _scalesConnectionTimer.AutoReset = true;
        }
        catch (Exception ex)
        {
            LogBase.LogMessageToFile("Error Program.cs->" + ex.ToString());
        }
    }

    private void ScalesConnectionTimerElapsed(object sender, ElapsedEventArgs e)
    {
      _scalesConnectionTimer.Enabled = false;

      foreach (Scale scale in SingletonScales.Instance.Where(scale => scale.Enabled && !scale.IsConnected))
      {
        try
        {
          scale.Connect();
        }
        catch (Exception ex)
        {
          ExceptionHandler.HandleException(ex);
        }
      }

      int scalesNoConnected = SingletonScales.Instance.Count(scale => scale.Enabled && !scale.IsConnected);

      //Enable timer only if pending scales to connect
      if (scalesNoConnected > 0)
      {
            LogUtility.LogError("IMPOSIBLE CONECTAR CON LA BASCULA!!");
            _scalesConnectionTimer.Enabled = true;
      }
      else
        {
            LogUtility.LogInfo("CONEXION EXITOSA A LA BASCULA!!");
        }
    }

    private static void scaleFrameReceivedError(object sender)
    {
      if (sender == null)
        return;

      var ex = (Exception) sender;
      Task.Factory.StartNew(() => { ExceptionHandler.HandleException(ex); });
      LogBase.LogMessageToFile("Error Program.cs-> scaleFrameReceivedError-> " + ex.ToString());
    }


    private static void GetSettings()
    {
      try
      {
        _scaleConfiguration = ((ScaleConfiguration) (ConfigurationManager.GetSection(_configSectionName)));
        _scaleSettingsCollection = _scaleConfiguration.AllScales;
      }
      catch (Exception ex)
      {
        MessageDTO message = ExceptionHandler.HandleException(ex);
      }
    }

    private static void StartWCFService()
    {
      _wcfServiceHost = new ServiceHost(typeof (ScaleService));

      // Alexander Bautista: Pending if use another Endpoints
      //var tcpEndpoint = host.AddServiceEndpoint(typeof(IScaleService), new NetTcpBinding(), "net.tcp://localhost:9010/ScaleService");
      //var httpEndpoint = host.AddServiceEndpoint(typeof(IScaleService), new BasicHttpBinding(), "http://localhost:8080/ScaleService");

      _wcfServiceHost.Faulted += WcfHostFaulted;
      _wcfServiceHost.Closed += WcfHostClosed;

      _wcfServiceHost.Open();
    }

    private static void WcfHostClosed(object sender, EventArgs e)
    {
      // que hacemos aqui?
    }

    private static void WcfHostFaulted(object sender, EventArgs e)
    {
      // Que hacemos aqui???
    }


    private void ReleaseResources()
    {
      ReleaseSocketConnection();
      ReleaseWcfHost();
    }

    private void ReleaseSocketConnection()
    {
      foreach (var scale in SingletonScales.Instance)
      {
        scale.Disconnect();
      }

    }

    private void ReleaseWcfHost()
    {
      if (_wcfServiceHost == null)
        return;

      switch (_wcfServiceHost.State)
      {
        case CommunicationState.Faulted:
          _wcfServiceHost.Abort();
          break;
        case CommunicationState.Opened:
        case CommunicationState.Opening:
          _wcfServiceHost.Close();
          break;
      }
    }
  }
}