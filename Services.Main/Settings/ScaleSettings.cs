﻿using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.Services.Main.Settings
{
  /// <summary>
  /// SmtpServer encapsulates all the properties of an smtp server.
  /// </summary>
  public class ScaleSettings
  {
    private readonly int _baudRate;
    private readonly string _code;

    private readonly string _comName;
    private readonly ScaleConstants.ComunicationMode _communicationMode;
    private readonly int _dataBits;
    private readonly bool _enabled;
    private readonly int _executeOrder;
    private readonly string _name;
    private readonly string _parity = string.Empty;
    private readonly string _protocol;
    private readonly string _socketIpAddress;
    private readonly int _socketPort;
    private readonly string _stopBits = string.Empty;
    private readonly ComConstants.TransmissionType _transmissionType;
    private readonly int _weightVariation;

    public ScaleSettings(string code, string name, string protocol, int weightVariation,
                         ScaleConstants.ComunicationMode communicationMode,
                         ComConstants.TransmissionType transmissionType, string comName, int baudRate, string parity,
                         string stopBits, int dataBits, string socketIpAddress, int socketPort, bool enabled)
    {
      _code = code;
      _name = name;
      _protocol = protocol;
      _weightVariation = weightVariation;
      _communicationMode = communicationMode;
      _transmissionType = transmissionType;

      _comName = comName;
      _baudRate = baudRate;
      _parity = parity;
      _stopBits = stopBits;
      _dataBits = dataBits;
      _socketIpAddress = socketIpAddress;
      _socketPort = socketPort;

      _enabled = enabled;
    }

    public string Code
    {
      get { return _code; }
    }

    public string Name
    {
      get { return _name; }
    }

    public string Protocol
    {
      get { return _protocol; }
    }

    public ScaleConstants.ComunicationMode CommunicationMode
    {
      get { return _communicationMode; }
    }

    public ComConstants.TransmissionType TransmissionType
    {
      get { return _transmissionType; }
    }

    public int WeightVariation
    {
      get { return _weightVariation; }
    }


    public string ComName
    {
      get { return _comName; }
    }

    /// <summary>
    /// Property to hold the BaudRate
    /// of our manager class
    /// </summary>
    public int BaudRate
    {
      get { return _baudRate; }
    }

    /// <summary>
    /// property to hold the Parity
    /// of our manager class
    /// </summary>
    public string Parity
    {
      get { return _parity; }
    }

    /// <summary>
    /// property to hold the StopBits
    /// of our manager class
    /// </summary>
    public string StopBits
    {
      get { return _stopBits; }
    }

    /// <summary>
    /// property to hold the DataBits
    /// of our manager class
    /// </summary>
    public int DataBits
    {
      get { return _dataBits; }
    }

    public string SocketIpAddress
    {
      get { return _socketIpAddress; }
    }

    /// <summary>
    /// property to hold the DataBits
    /// of our manager class
    /// </summary>
    public int SocketPort
    {
      get { return _socketPort; }
    }


    public int ExecuteOrder
    {
      get { return _executeOrder; }
    }

    public bool Enabled
    {
      get { return _enabled; }
    }


    public override string ToString()
    {
      return _code;
    }
  }
}