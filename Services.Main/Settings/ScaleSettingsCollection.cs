﻿using System;
using System.Collections;

namespace Simplexity.Scales.Services.Main.Settings
{
  /// <summary>
  /// SmtpServerSettingsCollection is a collection of SmtpServerSettings.
  /// </summary>
  /// 
  [Serializable]
  public class ScaleSettingsCollection : CollectionBase
  {
    public ScaleSettings this[int index]
    {
      get { return (ScaleSettings) InnerList[index]; }
    }

    public void Add(ScaleSettings scale)
    {
      InnerList.Add(scale);
    }

    public void Sort(IComparer icomp)
    {
      InnerList.Sort(icomp);
    }
  }
}