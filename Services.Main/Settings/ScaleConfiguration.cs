﻿using Simplexity.Scales.Services.Main.Comparers;

namespace Simplexity.Scales.Services.Main.Settings
{
  public sealed class ScaleConfiguration
  {
    private readonly ScaleSettingsCollection m_allScales = new ScaleSettingsCollection();

    public ScaleSettingsCollection AllScales
    {
      get { return m_allScales; }
    }

    public void AddScaleSetting(ScaleSettings scale)
    {
      m_allScales.Add(scale);
      if (scale.ExecuteOrder > 0)
      {
        m_allScales.Sort(new ScaleSettingsComparer(ScaleSettingsComparer.ScaleSettingsSortColumns.ExecuteOrder));
      }
    }
  }
}