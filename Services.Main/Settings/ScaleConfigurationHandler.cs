﻿using System;
using System.Configuration;
using System.Xml;
using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.Services.Main.Settings
{
  /// <summary>
  /// EmailSettingsHandler converts the .config settings into an object.
  /// </summary>
  internal sealed class ScaleConfigurationHandler : IConfigurationSectionHandler
  {
    // The section handler interprets and processes the settings defined in 
    // XML tags within a specific portion of a Web.config file and returns an 
    // appropriate configuration object .... based on the configuration settings. 
    // ( above exert from http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpguide/html/cpconcreatingnewsectionhandlers.asp )
    // also see http://support.microsoft.com/default.aspx?scid=kb;en-us;309045  // Article ID : 309045 


    //private const string ScalesActiveXpath = "//scale[@enabled='true']";
    private const string ScalesActiveXpath = "//scale";

    //ScalesConfiguration attributes
    private const string ConfigfileAttributeNameSocketServerPort = "socketServerPort";

    // Scale Settings
    private const string ConfigfileAttributeCode = "code";
    private const string ConfigfileAttributeEnabled = "enabled";
    private const string ConfigfileAttributeName = "name";
    private const string ConfigfileAttributeProtocol = "protocol";
    private const string ConfigfileAttributeCommunicationMode = "communicationMode";
    private const string ConfigfileAttributeWeightVariation = "weightVariation";

    // Scale Com Settings
    private const string ConfigfileAttributeComName = "comName";
    private const string ConfigfileAttributeBaudRate = "baudRate";
    private const string ConfigfileAttributeParity = "parity";
    private const string ConfigfileAttributeStopBits = "stopBits";
    private const string ConfigfileAttributeDataBits = "dataBits";
    private const string ConfigfileAttributeTransmissionType = "transmissionType";


    // Socket Settings
    private const string ConfigfileAttributeSocketIpAddress = "socketIpAddress";
    private const string ConfigfileAttributeSocketPort = "socketPort";

    // Execution Order
    private const string ConfigfileAttributenameExecuteorder = "executeOrder";

    #region IConfigurationSectionHandler Members

    public object Create(object parent, object configContext, XmlNode section)
    {
      var returnSettings = new ScaleConfiguration();


      var root = ((XmlElement) section);


      string smptSocketServer = string.Empty;
      string masterPortNumber = string.Empty;

      if (null != root.Attributes[ConfigfileAttributeNameSocketServerPort])
      {
        smptSocketServer = root.Attributes[ConfigfileAttributeNameSocketServerPort].Value;
      }

      XmlNodeList nodeListScales = root.SelectNodes(ScalesActiveXpath);


      if (nodeListScales.Count <= 0)
      {
        throw new ArgumentException("No matches found in config file with Xpath : " + ScalesActiveXpath);
      }

      foreach (XmlNode nodeScale in nodeListScales)
      {
        //Keep the variable scope correct, for each specific scale
        string code = string.Empty;
        bool enabled;
        string name = string.Empty;
        string protocol = string.Empty;
        int weightVariation = 0;
        string communicationModeTemp = string.Empty;
        ScaleConstants.ComunicationMode communicationMode;

        string comName = string.Empty;
        int baudRate = 0;
        string parity = string.Empty;
        string stopBits = string.Empty;
        int dataBits = 0;
        string transmissionTypeTemp = string.Empty;
        ComConstants.TransmissionType transmissionType;

        string socketIpAddress = string.Empty;
        int socketPort = 0;

        int executeOrder = 0;

        //this code is actually setup to handle multiple items
        //(the "foreach" loop)
        //but only 1 RateQuoterSettings object will be returned, the last one it finds
        string currentXpath = string.Empty;
        try
        {
          currentXpath = ConfigfileAttributeCode;
          code = nodeScale.Attributes[currentXpath].Value;

          currentXpath = ConfigfileAttributeEnabled;
          enabled = nodeScale.Attributes[currentXpath].Value.ToLower() == "true";

          currentXpath = ConfigfileAttributeName;
          if (null != nodeScale.Attributes[currentXpath])
          {
            name = nodeScale.Attributes[currentXpath].Value;
          }

          currentXpath = ConfigfileAttributeProtocol;
          if (null != nodeScale.Attributes[currentXpath])
          {
            protocol = nodeScale.Attributes[currentXpath].Value;
          }

          currentXpath = ConfigfileAttributeWeightVariation;
          if (null != nodeScale.Attributes[currentXpath])
          {
            Int32.TryParse(nodeScale.Attributes[currentXpath].Value, out weightVariation);
          }

          currentXpath = ConfigfileAttributeCommunicationMode;
          if (null != nodeScale.Attributes[currentXpath])
          {
            communicationModeTemp = nodeScale.Attributes[currentXpath].Value;
          }
          communicationMode = DetermineCommunicationMode(communicationModeTemp);

          currentXpath = ConfigfileAttributeComName;
          if (null != nodeScale.Attributes[currentXpath])
          {
            comName = nodeScale.Attributes[currentXpath].Value;
          }

          currentXpath = ConfigfileAttributeBaudRate;
          if (null != nodeScale.Attributes[currentXpath])
          {
            Int32.TryParse(nodeScale.Attributes[currentXpath].Value, out baudRate);
          }

          currentXpath = ConfigfileAttributeParity;
          if (null != nodeScale.Attributes[currentXpath])
          {
            parity = DetermineParity(nodeScale.Attributes[currentXpath].Value);
          }

          currentXpath = ConfigfileAttributeStopBits;
          if (null != nodeScale.Attributes[currentXpath])
          {
            stopBits = DetermineStopBits(nodeScale.Attributes[currentXpath].Value);
          }

          currentXpath = ConfigfileAttributeDataBits;
          if (null != nodeScale.Attributes[currentXpath])
          {
            Int32.TryParse(nodeScale.Attributes[currentXpath].Value, out dataBits);
          }

          currentXpath = ConfigfileAttributeTransmissionType;
          if (null != nodeScale.Attributes[currentXpath])
          {
            transmissionTypeTemp = nodeScale.Attributes[currentXpath].Value;
          }
          transmissionType = DetermineTransmitionType(transmissionTypeTemp);


          currentXpath = ConfigfileAttributeSocketIpAddress;
          if (null != nodeScale.Attributes[currentXpath])
          {
            socketIpAddress = nodeScale.Attributes[currentXpath].Value;
          }

          currentXpath = ConfigfileAttributeSocketPort;
          if (null != nodeScale.Attributes[currentXpath])
          {
            Int32.TryParse(nodeScale.Attributes[currentXpath].Value, out socketPort);
          }

          currentXpath = ConfigfileAttributenameExecuteorder;
          if (null != nodeScale.Attributes[currentXpath])
          {
            executeOrder = Convert.ToInt32(nodeScale.Attributes[currentXpath].Value);
          }
        }
        catch (ArgumentException argex)
        {
          throw argex;
        }
        catch
        {
          //the currentXPath is just a way I communicate back up to the presentation what the misformed xpath statement is.
          throw new ArgumentException("Xml is incorrect in config file : " + ScalesActiveXpath + "/@" + currentXpath);
        }

        returnSettings.AddScaleSetting(new ScaleSettings(code, name, protocol, weightVariation, communicationMode,
                                                         transmissionType, comName, baudRate, parity, stopBits, dataBits,
                                                         socketIpAddress, socketPort, enabled));
      }


      return returnSettings;
    }

    #endregion

    private ScaleConstants.ComunicationMode DetermineCommunicationMode(string inputValue)
    {
      switch (inputValue.ToLower())
      {
        case "":
        case "com":
          return ScaleConstants.ComunicationMode.Com;
          //break;

        case "socket":
          return ScaleConstants.ComunicationMode.Socket;
          //break;

        default:
          throw new ArgumentException("Invalid Communication Mode Provided of '" + inputValue +
                                      "'.  Please provide an acceptable value of 'com', or 'socket' (or empty string).");
          //break;
      }
    }

    private ComConstants.TransmissionType DetermineTransmitionType(string inputValue)
    {
      switch (inputValue.ToLower())
      {
        case "":
        case "hex":
          return ComConstants.TransmissionType.Hex;
          //break;

        case "text":
          return ComConstants.TransmissionType.Text;
          //break;
        default:
          return ComConstants.TransmissionType.Hex;
      }
    }

    private string DetermineParity(string inputValue)
    {
      switch (inputValue.ToLower())
      {
        case "":
        case "none":
          return "None";
          //break;
        case "even":
          return "Even";
        case "odd":
          return "Odd";
        case "mark":
          return "Mark";
          //break;
        default:
          return "None";
      }
    }

    private string DetermineStopBits(string inputValue)
    {
      switch (inputValue.ToLower())
      {
        case "":
        case "one":
          return "One";
          //break;
          //break;
        default:
          return "One";
      }
    }
  }
}