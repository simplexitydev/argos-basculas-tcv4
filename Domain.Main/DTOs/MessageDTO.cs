﻿using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.Domain.Main.DTOs
{
    public class MessageDTO
    {
      private MessagesConstants.Types _typeEnum;

      public MessageDTO()
      {
        _typeEnum = MessagesConstants.Types.Information;
      }

      public MessagesConstants.Types TypeEnum
      {
        set { _typeEnum = value; }
        get { return _typeEnum; }
      }

      public int Type
      {
        get { return (int)_typeEnum; }
      }
      //
      public string Message { get; set; }

      public string TransactionNumber { get; set; }
    }
}
