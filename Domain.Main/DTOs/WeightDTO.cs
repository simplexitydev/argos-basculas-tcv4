﻿namespace Simplexity.Scales.Domain.Main.DTOs
{
  public class WeightDTO
  {
    public double Weight { get; set; }
    public int State { get; set; }
    public MessageDTO Message { get; set; }

    public WeightDTO()
    {
      Message = new MessageDTO();
    }
  }
}
