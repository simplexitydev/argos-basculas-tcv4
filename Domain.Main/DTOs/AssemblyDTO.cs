﻿namespace Simplexity.Scales.Domain.Main.DTOs
{
  public class AssemblyDTO
  {
    public string Name {  get ;  set ;}
    public string FullName { get; set; }
    public string AssemblyVersion { get; set; }
    public string FileVersion { get; set; }
  }
}
