﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplexity.Scales.Domain.Main.DTOs
{
  public class ComPortDTO
  {
    public string PortName { get; set; }
    public int BaudRate { get; set; }
    public string Parity { get; set; }
    public string StopBits { get; set; }
    public int DataBits { get; set; }
    public string TransmissionType { get; set; }
  }
}
