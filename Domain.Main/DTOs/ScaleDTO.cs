﻿using System.Collections.Generic;

namespace Simplexity.Scales.Domain.Main.DTOs
{
  public class ScaleDTO
  {
    public string Code { get; set; }
    public string NewCode { get; set; }
    public string Name { get; set; }
    public string ComunicationMode { get; set; }
    public string CurrentProtocol { get; set; }
    public ComPortDTO ComPortInfo { get; set; }
    public SocketDTO SocketInfo { get; set; }
    public List<string> AvailableProtocols { get; set; }
    public double WeightVariation { get; set; }
    public bool Enabled { get; set; }


  }
}
