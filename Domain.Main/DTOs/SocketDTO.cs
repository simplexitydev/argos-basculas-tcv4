﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Simplexity.Scales.Domain.Main.DTOs
{
  public class SocketDTO
  {
    public string Address { get; set; }
    public string Port { get; set; }
  }
}
