﻿using System;

namespace Simplexity.Scales.Domain.Main.Common
{
  public static class Utility
  {
    public static string HexadecimalToString(string data)
    {
      lock (data)
      {

        var sData = "";

        while (data.Length > 0)
        //first take two hex value using substring.
        //then  convert Hex value into ascii.
        //then convert ascii value into character.
        {
          var data1 = Convert.ToChar(Convert.ToUInt32(data.Substring(0, 2), 16)).ToString();
          sData = sData + data1;
          data = data.Substring(2, data.Length - 2);
        }
        return sData;
      }
    }
    public static string StringToHexadecimal(string data)
    {
      lock (data)
      {
        //first take each charcter using substring.
        //then  convert character into ascii.
        //then convert ascii value into Hex Format

        string sValue;
        string sHex = string.Empty;
        foreach (char c in data.ToCharArray())
        {
          sValue = String.Format("{0:X}", Convert.ToUInt32(c));
          sHex = sHex + sValue;
        }
        return sHex;
        
      }
    }

    public static string PrintHexadecimal(byte[] data)
    {
      lock (data)
      {
        var value = string.Empty;
        for (var i = 0; i < data.Length; i++)
        {
          value += "{" + data[i].ToString("X2") + "}";
        }

        return value;
      }
    }

    /// <summary>
    /// Remove trailing nulls (0) from a byte array
    /// </summary>
    /// <param name="source"></param>
    /// <returns></returns>
    public static byte[] RemoveTrailingNullsFromByteArray(byte[] source)
    {
      lock (source)
      {
        // populate foo
        int i = source.Length - 1;
        while (source[i] == 0)
          --i;
        // now foo[i] is the last non-zero byte
        byte[] result = new byte[i + 1];
        Array.Copy(source, result, i + 1);

        return result;
      }
    }

  }
}
