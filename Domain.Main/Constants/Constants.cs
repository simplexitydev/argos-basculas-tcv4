﻿using System.Collections.Generic;

namespace Simplexity.Scales.Domain.Main.Constants
{
  public class MessagesConstants
  {
    #region Types enum

    public enum Types
    {
      None = 0,
      Information = 1,
      Warning = 2,
      Error = 3
    }

    #endregion
  }

  public static class ComConstants
  {
    public enum TransmissionType { Text, Hex }
  }
  public static class ScaleConstants
  {
    #region ComunicationMode enum

    public enum ComunicationMode
    {
      Com = 0,
      Socket = 1,
    }

    #endregion

    #region ModeEnum enum

    public enum ModeEnum
    {
      Gross,
      Tare,
      Net,
      Undefined
    }

    #endregion

    #region State enum

    public enum State
    {
      Unstable = 0,
      Stable = 1,
      Overloaded = 2,
      Disabled = 3,
      Undefined
    }

    #endregion

    #region UnitEnum enum

    public enum UnitEnum
    {
      Gram,
      Ounce,
      Kilogram,
      Pound,
      Ton,
      Undefined
    }

    #endregion

    public const string SpringContext = "ScaleSpringContext";

    // solo en caso en que las tramas no tengan definido el concepto de "Estable"
    public static string DefaultStableValue
    {
      get { return "ST"; }
    }
  }
}