﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Cardinal788 : IDecryptable
  {
    private readonly Regex _regex;
    private readonly Regex _regex2;

    public Regex Regex { get { return _regex; } }

    public Cardinal788()
    {
      EndByte = 0x0D;
      _regex = new Regex(@" *(\d*) *(kg|KG|lb|LB) *(G|T|N) *( |CZ|BZ|MO) *\r", RegexOptions.Compiled);
      _regex2 = new Regex(@" *(\d*) *(kg|KG|lb|LB) *\r", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public Frame Decrypt(byte[] data)
    {
      //(Ascii)"     00 KG G CZ $0D"
      //(Ascii)"     00 KG G BZ $0D"
      //(Ascii)"     00 KG G MO $0D"

      //(Hexa)"$20$20$20$20$20$30$30$20$4B$47$20$47$20$43$5A$20$0D"

      // 00 => Peso
      // KG => Unidad de peso

      // CZ | MO | BZ => ??

      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        // Alexander Bautista, Enero 09 de 2014: Se hizo un segundo regular expression por que a veces llega colado en la trama lo siguiente:
        //            17060 kg {0D}
        // una cadena con muchos espacios al principio, el peso y la unidad y despues nada.
        // Esto estaba generando LOGS de errores en tramas bastantes grandes, por eso lo hicimos así aunque debería arreglarse el indicador
        // Esto debería ser temporal y debe arreglarse el indicador.

        Match dataMatch2 = _regex2.Match(dataString);
        if (!dataMatch2.Success)
        {
          //return null;
          // se comentario para revisar el problema en rio claro. a ver si por aqui s el problema
          //throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString, Utility.StringToHexadecimal(dataString), GetType().Name));
          throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
                                                  Utility.PrintHexadecimal(data), GetType().Name));
        }
        else
        {
          return null;
        }
      }

      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[1].Value),
          Unit = dataMatch.Groups[2].Value.ToLower() == "kg"
                   ? ScaleConstants.UnitEnum.Kilogram
                   : dataMatch.Groups[2].Value.ToLower() == "lb"
                       ? ScaleConstants.UnitEnum.Pound
                       : ScaleConstants.UnitEnum.Undefined,
          Mode = dataMatch.Groups[3].Value == "G"
                   ? ScaleConstants.ModeEnum.Gross
                   : dataMatch.Groups[3].Value == "N"
                       ? ScaleConstants.ModeEnum.Net
                       : dataMatch.Groups[3].Value == "T"
                           ? ScaleConstants.ModeEnum.Tare
                           : ScaleConstants.ModeEnum.Undefined,
          State =
            (dataMatch.Groups[4].Value == " " || dataMatch.Groups[4].Value == "CZ")
              ? ScaleConstants.State.Stable
              : (dataMatch.Groups[4].Value == "MO" || dataMatch.Groups[4].Value == "BZ")
                  ? ScaleConstants.State.Unstable
                  : ScaleConstants.State.Undefined,
        };

      return frame;
    }

    public byte EndByte { get; set; }

    #endregion
  }
}