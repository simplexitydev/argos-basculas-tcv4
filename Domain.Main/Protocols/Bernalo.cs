﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Resources;
using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.Domain.Main.Protocols
{
	public class Bernalo : IDecryptable
	{
		private readonly Regex _regex;
		public Regex Regex { get { return _regex; } }

		public Bernalo()
		{
			EndByte = 0x3D;
			/*trama 
			 * =-000090
			 * {3D}{20}{30}{30}{30}{30}{30}{30}
			 * */
			_regex = new Regex(@" (\d*)\x3D", RegexOptions.Compiled);
		}
		#region IDecryptable Members

		public byte EndByte { get; set; }

		public Frame Decrypt(byte[] data)
		{
			string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

			Match dataMatch = _regex.Match(dataString);
			if (!dataMatch.Success)
			{
				throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat,
																Utility.StringToHexadecimal(dataString), GetType().Name));					
			}

			//double value1 = Double.Parse(dataMatch.Groups[0].Value);
			double value2 = Double.Parse(dataMatch.Groups[1].Value);
			//double value3 = Double.Parse(dataMatch.Groups[2].Value);

			var frame = new Frame
			{
				Weight = Double.Parse(dataMatch.Groups[1].Value),
				Unit = ScaleConstants.UnitEnum.Kilogram,
				Mode = ScaleConstants.ModeEnum.Gross,
				State = ScaleConstants.State.Stable
				//DateTime = DateTime.Now
			};
			return frame; 
		}

		#endregion
	}
}
