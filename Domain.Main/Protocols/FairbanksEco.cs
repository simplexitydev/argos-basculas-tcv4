﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
    public class FairbanksEco : IDecryptable
    {
        private readonly Regex _regex;

    public Regex Regex { get { return _regex; } }

    public FairbanksEco()
    {
      EndByte = 0x0D;
      string re1 = ".*?";	// Non-greedy match on filler
      string re2 = "\\d+";	// Uninteresting: int
      string re3 = ".*?";	// Non-greedy match on filler
      string re4 = "(\\d+)";	// Integer Number 1
      _regex = new Regex(re1 + re2 + re3 + re4, RegexOptions.IgnoreCase | RegexOptions.Singleline);
    }

    #region IDecryptable Members

    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      byte bConfig = Convert.ToByte(dataMatch.Groups[1].Value[0]);

      var Weight = (bConfig & 2) == 0 ? Double.Parse(dataMatch.Groups[1].Value) : -Double.Parse(dataMatch.Groups[1].Value);

      var frame = new Frame
        {
            Weight =
              (bConfig & 2) == 0 ? Double.Parse(dataMatch.Groups[1].Value) : -Double.Parse(dataMatch.Groups[1].Value),
            Unit = (bConfig & 16) == 0 ? ScaleConstants.UnitEnum.Pound : ScaleConstants.UnitEnum.Kilogram,
            Mode = (bConfig & 1) == 0 ? ScaleConstants.ModeEnum.Gross : ScaleConstants.ModeEnum.Net,
            State = (bConfig & 4) != 0
                      ? ScaleConstants.State.Overloaded
                      : (bConfig & 8) == 0
                          ? ScaleConstants.State.Stable
                          : ScaleConstants.State.Unstable
        };

      return frame;
    }

    #endregion
    }
}
