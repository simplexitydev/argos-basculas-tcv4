﻿using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class MassValueSanAntonio : IDecryptable
  {
    public MassValueSanAntonio()
    {
      EndByte = 0x0D;
    }

    #region IDecryptable Members

    public Regex Regex { get; private set; }
    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length - 1).Replace(' ', '0');

      dataString = dataString.Remove(0, 1);

      return new Frame
        {
          Weight = double.Parse(dataString),
          Unit = ScaleConstants.UnitEnum.Undefined,
          Mode = ScaleConstants.ModeEnum.Undefined,
          State = ScaleConstants.State.Undefined
        };
    }

    #endregion
  }
}