﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class ConsolidatedControls : IDecryptable
  {
    private readonly Regex _regex;

    public Regex Regex { get { return _regex; } }

    public ConsolidatedControls()
    {
      EndByte = 0x0D;
      _regex = new Regex(@"\x02 *(-? *[\.\d]*)(K|L|G|O)( |M)\r", RegexOptions.Compiled);

      // La regular expresión debería ser asi, por que puede traer un indicador de error (O|I) pero si lo trae, preferimos que genere exepción
      // "\x02 *(-? *[\.\d]*)(K|L|G|O)(O|I)?( |M)\r"
    }

    #region IDecryptable Members

    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[1].Value),
          Unit = dataMatch.Groups[2].Value == "K"
                   ? ScaleConstants.UnitEnum.Kilogram
                   : dataMatch.Groups[2].Value == "L"
                       ? ScaleConstants.UnitEnum.Pound
                       : dataMatch.Groups[2].Value == "G"
                           ? ScaleConstants.UnitEnum.Gram
                           : ScaleConstants.UnitEnum.Ounce,
          Mode = ScaleConstants.ModeEnum.Undefined,
          State = dataMatch.Groups[1].Value == " " ? ScaleConstants.State.Stable : ScaleConstants.State.Unstable
        };

      return frame;
    }

    #endregion
  }
}