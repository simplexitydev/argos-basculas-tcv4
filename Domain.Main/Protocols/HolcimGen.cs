﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
    public class HolcimGen : IDecryptable
    {
        private readonly Regex _regex;

        public Regex Regex { get { return _regex; } }

        public HolcimGen()
        {
            EndByte = 0x0D;
            _regex = new Regex(@" *[+-] *([ \.\d]*) *\x0D", RegexOptions.Compiled);
        }

        #region IDecryptable Members

        public byte EndByte { get; set; }

        public Frame Decrypt(byte[] data)
        {
            string dataString = Encoding.ASCII.GetString(data, 0, data.Length);
            Match dataMatch = _regex.Match(dataString);

            if (!dataMatch.Success)
            {
                throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat,
                                                        Utility.StringToHexadecimal(dataString), GetType().Name));
            }

            var frame = new Frame
            {
                Weight = Double.Parse(dataMatch.Groups[1].Value.Replace('*', '0')),
                Unit = ScaleConstants.UnitEnum.Kilogram,//dataMatch.Groups[3].Value == "kg" ? ScaleConstants.UnitEnum.Kilogram : ScaleConstants.UnitEnum.Pound,
                Mode = ScaleConstants.ModeEnum.Gross,//dataMatch.Groups[4].Value == "g" ? ScaleConstants.ModeEnum.Gross : ScaleConstants.ModeEnum.Net,
                State = ScaleConstants.State.Stable//dataMatch.Groups[2].Value == " "
                          //? ScaleConstants.State.Stable
                          //: dataMatch.Groups[2].Value == "m"
                           //   ? ScaleConstants.State.Unstable
                           //   : ScaleConstants.State.Overloaded
            };

            return frame;
        }

        #endregion
    }
}
