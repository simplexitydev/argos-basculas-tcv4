﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Ricelake : IDecryptable
  {
    private readonly Regex _regex;

    public Regex Regex { get { return _regex; } }

    public Ricelake()
    {
      EndByte = 0x0a;
      _regex = new Regex(@"\x02 *(-? *[\.\d]*)(L|K|G|T| )(G|N)( |I|M|O)\r\n", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[1].Value),
          Unit = dataMatch.Groups[2].Value == "K"
                   ? ScaleConstants.UnitEnum.Kilogram
                   : dataMatch.Groups[2].Value == "L"
                       ? ScaleConstants.UnitEnum.Pound
                       : dataMatch.Groups[2].Value == "G"
                           ? ScaleConstants.UnitEnum.Gram
                           : ScaleConstants.UnitEnum.Ounce,
          Mode = dataMatch.Groups[3].Value == "G"
                   ? ScaleConstants.ModeEnum.Gross
                   : dataMatch.Groups[3].Value == "N" ? ScaleConstants.ModeEnum.Net : ScaleConstants.ModeEnum.Undefined,
          State = dataMatch.Groups[4].Value == " "
                    ? ScaleConstants.State.Stable
                    : dataMatch.Groups[4].Value == "M" ? ScaleConstants.State.Unstable : ScaleConstants.State.Undefined
        };

      return frame;
    }

    #endregion
  }
}