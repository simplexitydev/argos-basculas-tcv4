﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Fairbanks2200 : IDecryptable
  {
    private readonly Regex _regex;

    public Regex Regex { get { return _regex; } }

    public Fairbanks2200()
    {
      //EndByte = 0x0A;
      EndByte = 0x04;
      _regex = new Regex(@" *(\d*) *(kg|lb|oz)? *(gr|nt|GR|NT) *\r\n", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[1].Value),
          Unit = dataMatch.Groups[2].Value == "kg"
                   ? ScaleConstants.UnitEnum.Kilogram
                   : dataMatch.Groups[2].Value == "lb"
                       ? ScaleConstants.UnitEnum.Pound
                       : dataMatch.Groups[2].Value == "oz"
                           ? ScaleConstants.UnitEnum.Ounce
                           : ScaleConstants.UnitEnum.Undefined,
          Mode =
            dataMatch.Groups[3].Value == "gr" || dataMatch.Groups[3].Value == "GR"
              ? ScaleConstants.ModeEnum.Gross
              : ScaleConstants.ModeEnum.Net,
          State =
            dataMatch.Groups[3].Value == "GR" || dataMatch.Groups[3].Value == "NT"
              ? ScaleConstants.State.Stable
              : ScaleConstants.State.Unstable
        };

      return frame;
    }

    #endregion
  }
}