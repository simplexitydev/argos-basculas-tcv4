﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class AD4321F1Tolu : IDecryptable
  {
    private readonly Regex _regex;



    public AD4321F1Tolu()
    {
      EndByte = 0x0A;
      _regex = new Regex(@"\d+", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[0].Value),
          Unit = ScaleConstants.UnitEnum.Kilogram,
          Mode = ScaleConstants.ModeEnum.Undefined,
          State = ScaleConstants.State.Undefined
        };


      return frame;
    }

    public Regex Regex { get { return _regex; }  }
    public byte EndByte { get; set; }

    #endregion
  }
}