﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Ricelake920i : IDecryptable
  {
    private readonly Regex _regex;
    private readonly Regex _regexB;
    public Regex Regex { get { return _regex; } }

    public Ricelake920i()
    {
      EndByte = 0x0D;
      _regex = new Regex(@"\x02.(.).(\d{5})", RegexOptions.Compiled);
	  _regexB = new Regex(@" *(\d*) *(kg|lb|oz)? *(gr|nt|GR|NT) *\r\n", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);
      bool band = false;

      if (!dataMatch.Success)
      {
          dataMatch = _regexB.Match(dataString);
          if (!dataMatch.Success)
          {
              throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
          }
          else
          {
              band = true;
          }
        
      }

      if (!band)
      {
          byte bConfig = Convert.ToByte(dataMatch.Groups[1].Value[0]);

          var frame = new Frame
            {
                Weight =
                  (bConfig & 2) == 0 ? Double.Parse(dataMatch.Groups[2].Value) : -Double.Parse(dataMatch.Groups[2].Value),
                Unit = (bConfig & 16) == 0 ? ScaleConstants.UnitEnum.Pound : ScaleConstants.UnitEnum.Kilogram,
                Mode = (bConfig & 1) == 0 ? ScaleConstants.ModeEnum.Gross : ScaleConstants.ModeEnum.Net,
                State = (bConfig & 4) != 0
                          ? ScaleConstants.State.Overloaded
                          : (bConfig & 8) == 0
                              ? ScaleConstants.State.Stable
                              : ScaleConstants.State.Unstable
            };
          return frame;
      }
      else
      {
          var frame = new Frame
          {
              Weight = Double.Parse(dataMatch.Groups[1].Value),
              Unit = dataMatch.Groups[2].Value == "kg"
                       ? ScaleConstants.UnitEnum.Kilogram
                       : dataMatch.Groups[2].Value == "lb"
                           ? ScaleConstants.UnitEnum.Pound
                           : dataMatch.Groups[2].Value == "oz"
                               ? ScaleConstants.UnitEnum.Ounce
                               : ScaleConstants.UnitEnum.Undefined,
              Mode =
                dataMatch.Groups[3].Value == "gr" || dataMatch.Groups[3].Value == "GR"
                  ? ScaleConstants.ModeEnum.Gross
                  : ScaleConstants.ModeEnum.Net,
              State =
                dataMatch.Groups[3].Value == "GR" || dataMatch.Groups[3].Value == "NT"
                  ? ScaleConstants.State.Stable
                  : ScaleConstants.State.Unstable
          };
          return frame;
      }

      
    }

    #endregion
  }
}