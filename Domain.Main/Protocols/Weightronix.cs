﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Weightronix : IDecryptable
  {
    private readonly Regex _regex;

    public Weightronix()
    {
      EndByte = 0x0A;
      _regex = new Regex(@" ([GNTgnt]). *(\d*) *(kg)\r\n", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public Regex Regex{get { return _regex; }}
    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(Encoding.ASCII.GetString(data, 0, data.Length));
      if (!dataMatch.Success)
      {
        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      var frame = new Frame {Weight = Double.Parse(dataMatch.Groups[2].Value)};
      frame.SetMode(dataMatch.Groups[1].Value);
      frame.SetState(dataMatch.Groups[1].Value);
      frame.SetUnit(dataMatch.Groups[3].Value);
      return frame;
    }

    #endregion
  }
}