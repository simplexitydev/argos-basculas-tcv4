﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Cardinal225 : IDecryptable
  {
    private readonly Regex _regex;

    public Regex Regex { get { return _regex; } }

    public Cardinal225()
    {
      EndByte = 0x0D;
      _regex = new Regex(@" *(\d*)(kg|lb)(GG|GR)\r", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[1].Value),
          Unit = dataMatch.Groups[2].Value == "kg"
                   ? ScaleConstants.UnitEnum.Kilogram
                   : dataMatch.Groups[2].Value == "lb"
                       ? ScaleConstants.UnitEnum.Pound
                       : ScaleConstants.UnitEnum.Undefined,
          Mode = ScaleConstants.ModeEnum.Gross,
          State = ScaleConstants.State.Stable
        };

      return frame;
    }

    #endregion
  }
}