﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class AD4321F1 : IDecryptable
  {
    
    //private TraceSource ts;
    //private TextWriterTraceListener tr;
    public AD4321F1()
    {
      EndByte = 0x0A;
      _regex = new Regex(@"(ST|OL|US)*\,(GS|NT)*\,[+-]? \d{6}(Kg|Lb) *[\x02\x03]\r", RegexOptions.Compiled);
      //_regex = new Regex(@"*(\d*)\. *(kg|lb) *([GgNn]) *[\x02\x03]\r", RegexOptions.Compiled);
    }

    #region IDecryptable Members
    
     /* OLD VERSION*/
    public Frame Decrypt(byte[] data)
    {
      ////#caracteres 18
      ////H1,H2, W	E	I	G	H	T	.	0	K	G	CR	LF 
      ////XX,XX,+12345.0KG\r\n
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      //ts.TraceInformation(dataString);

      string[] frames = dataString.Split(',');

      // 1. Validate length. Must be 3 positions
      if (frames.Length < 3)
      {
        return null;
        //
        //throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, Utility.StringToHexadecimal(dataString), GetType().Name));
      }
      // 2. Check every position data

      string weightstr = frames[2].Remove(8);
      string units = frames[2].Remove(0, 8);
      units = units.Remove(2);
      string mode = frames[1];
      string state = frames[0];

      const char space = ' ';
      weightstr = weightstr.Replace(space, '0');

      double weight;
      if (!double.TryParse(weightstr, out weight))
      {
        // Los datos no son numericos
        //throw .....
        return null;
      }

      var frameData = new Frame {Weight = weight};

      // 2 La segunda posición trae laqs unidades
      // Alexander bautista: lo cambiamos explicitamente por solicitud de argos....
      units = "kg";
      frameData.SetUnit(units);
      if (frameData.Unit == ScaleConstants.UnitEnum.Undefined)
      {
        //throw .....
        return null;
      }

      // 3 La tercera posición debe ser un valor con el modo
      frameData.SetMode(mode);
      if (frameData.Mode == ScaleConstants.ModeEnum.Undefined)
      {
        //throw .....
        return null;
      }

      frameData.SetState(state);

      if (frameData.State == ScaleConstants.State.Undefined)
      {
        //throw .....
        return null;
      }

      return frameData;
    }

		//public Frame Decrypt(byte[] data)
		//{
		//	string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

		//	Match dataMatch = _regex.Match(dataString);

		//	if (!dataMatch.Success)
		//	{
		//		throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString,
		//																						Utility.StringToHexadecimal(dataString), GetType().Name));
		//	}
		//	var frame = new Frame
		//	{
		//		Weight = Double.Parse(dataMatch.Groups[6].Value),
		//		Unit = ScaleConstants.UnitEnum.Kilogram,
		//		Mode = dataMatch.Groups[2].Value ==  "GS"?ScaleConstants.ModeEnum.Gross:dataMatch.Groups[2].Value == "NT"?ScaleConstants.ModeEnum.Net:ScaleConstants.ModeEnum.Undefined,
		//		State = dataMatch.Groups[0].Value == "ST" ? ScaleConstants.State.Stable : dataMatch.Groups[2].Value == "OL" ? ScaleConstants.State.Overloaded : dataMatch.Groups[2].Value == "US"?ScaleConstants.State.Unstable:ScaleConstants.State.Undefined
		//	};

		//	return frame;
		//}

    private readonly Regex _regex;
    public Regex Regex
    {
      get { return _regex; }
    }
    public byte EndByte { get; set; }

    #endregion
  }
}