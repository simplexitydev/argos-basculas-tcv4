﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Marinilla : IDecryptable
  {
    private readonly Regex _regex;

    public Regex Regex { get { return _regex; } }

    public Marinilla()
    {
      EndByte = 0x6b;
      _regex = new Regex(@"\d+", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        return null;
        //throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat, dataString, Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[0].Value),
          Unit = ScaleConstants.UnitEnum.Kilogram,
          Mode = ScaleConstants.ModeEnum.Gross,
          State = ScaleConstants.State.Undefined
        };


      return frame;
    }

    public byte EndByte { get; set; }

    #endregion
  }
}