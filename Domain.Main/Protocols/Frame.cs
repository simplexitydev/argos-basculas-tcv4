﻿using System;
using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Frame

  {
    private ScaleConstants.ModeEnum _mode;
    private ScaleConstants.State _state;
    private ScaleConstants.UnitEnum _unit;

    public Frame()
    {
      DateTime = new DateTime();
      DateTime = DateTime.Now;
    }

    public double Weight { get; set; }

    public ScaleConstants.UnitEnum Unit
    {
      get { return _unit; }
      set { _unit = value; }
    }

    public DateTime DateTime { get; set; }

    public ScaleConstants.State State
    {
      get { return _state; }
      set { _state = value; }
    }

    public ScaleConstants.ModeEnum Mode
    {
      get { return _mode; }
      set { _mode = value; }
    }

    public void SetUnit(string unit)
    {
      _unit = ScaleConstants.UnitEnum.Undefined;

      switch (unit.ToLower())
      {
        case "g":
          _unit = ScaleConstants.UnitEnum.Gram;
          break;
        case "o":
          _unit = ScaleConstants.UnitEnum.Ounce;
          break;
        case "l":
          _unit = ScaleConstants.UnitEnum.Pound;
          break;
        case "k":
        case "kg":
          _unit = ScaleConstants.UnitEnum.Kilogram;
          break;
        case "t":
          _unit = ScaleConstants.UnitEnum.Ton;
          break;
      }
    }

    public void SetState(string state)
    {
      _state = ScaleConstants.State.Undefined;

      switch (state)
      {
        case "ST":
        case "GR":
        case "NT":
        case "G":
        case "N":
        case "T":
        case "":
          _state = ScaleConstants.State.Stable;
          break;
        case "o":
          _state = ScaleConstants.State.Overloaded;
          break;
        case "US":
        case "gr":
        case "nt":
        case "I":
        case "M":
        case "CZ":
        case "BZ":
        case "MO":
        case "g":
        case "n":
        case "t":
          _state = ScaleConstants.State.Unstable;
          break;
      }
    }

    public void SetMode(string mode)
    {
      _mode = ScaleConstants.ModeEnum.Undefined;

      switch (mode)
      {
        case "GS":
        case "G":
        case "g":
        case "":
        case "NT":
        case " ":
          _mode = ScaleConstants.ModeEnum.Gross;
          break;
        case "T":
        case "t":
        case "O":
          _mode = ScaleConstants.ModeEnum.Tare;
          break;
        case "N":
        case "n":
          _mode = ScaleConstants.ModeEnum.Net;
          break;
      }
    }
  }
}