﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Cardinal220 : IDecryptable
  {
    private readonly Regex _regex;
    private readonly Regex _regexB;

    public Regex Regex { get { return _regex; } }
    public Regex RegexB { get { return _regexB; } }

    public Cardinal220()
    {
      EndByte = 0x0D;
      _regex = new Regex(@" *(\d*)\. *(kg|lb) *([GgNn]) *[\x02\x03]\r", RegexOptions.Compiled);
      _regexB = new Regex(@" *(\d*)\.M *(kg|lb) *([GgNn]) *[\x02\x03]\r", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);
      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        dataMatch = _regexB.Match(dataString);
        if (!dataMatch.Success)
        {
          throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
        }
        
      }

      string statusMode = dataMatch.Groups[3].Value;
      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[1].Value),
          Unit = dataMatch.Groups[2].Value == "kg" ? ScaleConstants.UnitEnum.Kilogram : ScaleConstants.UnitEnum.Pound,
          Mode = statusMode == "g" ? ScaleConstants.ModeEnum.Gross : ScaleConstants.ModeEnum.Net,
          State = (statusMode == "G" || statusMode == "N") ? ScaleConstants.State.Stable : ScaleConstants.State.Unstable
        };

      return frame;
    }

    #endregion
  }

  
}