﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public class Gse450A : IDecryptable
  {
    private readonly Regex _regex;

    public Regex Regex { get { return _regex; } }

    public Gse450A()
    {
      EndByte = 0x20;
      _regex = new Regex(@" *(\d*) *kg *(Gross|Net|Tare)", RegexOptions.Compiled);
    }

    #region IDecryptable Members

    public byte EndByte { get; set; }

    public Frame Decrypt(byte[] data)
    {
      string dataString = Encoding.ASCII.GetString(data, 0, data.Length);

      Match dataMatch = _regex.Match(dataString);

      if (!dataMatch.Success)
      {
        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat,
                                                Utility.StringToHexadecimal(dataString), GetType().Name));
      }

      string mode = dataMatch.Groups[2].Value;

      var frame = new Frame
        {
          Weight = Double.Parse(dataMatch.Groups[1].Value),
          Unit = ScaleConstants.UnitEnum.Kilogram,
          State = ScaleConstants.State.Stable,
          Mode = mode == "Gross"
                   ? ScaleConstants.ModeEnum.Gross
                   : mode == "Net"
                       ? ScaleConstants.ModeEnum.Net
                       : ScaleConstants.ModeEnum.Tare
        };


      if (frame.Mode != ScaleConstants.ModeEnum.Gross)
        return null;

      return frame;
    }

    #endregion
  }
}