﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Resources;
using Simplexity.Scales.Domain.Main.Common;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Resources;

namespace Simplexity.Scales.Domain.Main.Protocols
{
	public class Cardinal205 : IDecryptable
	{
		private readonly Regex _regex;
        private readonly Regex _regexB;
        private readonly Regex _regexC;

		public Cardinal205()
		{
		  EndByte = 0x0A;
          _regex = new Regex(@"Z1G  *(\d*)(kg) \r\n", RegexOptions.Compiled);
          _regexB = new Regex(@" 1G  *(\d*)(kg) \r\n", RegexOptions.Compiled);
          _regexC = new Regex(@" 1GM  *(\d*)(kg) \r\n", RegexOptions.Compiled);
		}

		#region IDecryptable Members

		public Regex Regex { get { return _regex; } }
		public byte EndByte { get; set; }

		public Frame Decrypt(byte[] data)
		{
			string dataString = Encoding.ASCII.GetString(data, 0, data.Length);
            
			Match dataMatch = _regex.Match(Encoding.ASCII.GetString(data, 0, data.Length));
            Match temMt = dataMatch;

			if (!dataMatch.Success)
			{
                Match dataMatch1 = _regexB.Match(Encoding.ASCII.GetString(data, 0, data.Length));
                temMt = dataMatch1;
                if (!dataMatch1.Success)
                {
                    Match dataMatch2 = _regexC.Match(Encoding.ASCII.GetString(data, 0, data.Length));
                    temMt = dataMatch2;
                    if (!dataMatch2.Success)
                    {
                        throw new FormatException(string.Format(Messages.exception_InvalidFrameFormat,
                                                                Utility.StringToHexadecimal(dataString), GetType().Name));
                    }
                }
            }
            

            var frame = new Frame
            {
                Weight = Double.Parse(temMt.Groups[1].Value),
                Unit =  ScaleConstants.UnitEnum.Kilogram,
                Mode = ScaleConstants.ModeEnum.Gross,
                State = ScaleConstants.State.Stable,
                DateTime=DateTime.Now
            };
            return frame;
		}

		#endregion
	}
}
