﻿using System.Text.RegularExpressions;

namespace Simplexity.Scales.Domain.Main.Protocols
{
  public interface IDecryptable
  {
    Regex Regex { get;}
    byte EndByte { get; set; }
    Frame Decrypt(byte[] data);
  }
}