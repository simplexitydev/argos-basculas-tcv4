﻿using System.Configuration;

namespace Simplexity.Scales.Domain.Main.Settings
{
  public class ScalesConfigurationSection : ConfigurationSection
  {
    public static readonly ScalesConfigurationSection Current = (ScalesConfigurationSection)ConfigurationManager.GetSection("ScalesConfigurationSection");
    
    [ConfigurationProperty("StandardHandlingFee", DefaultValue = "4.95")]
    public decimal StandardHandlingFee
    {
      get { return (decimal)base["StandardHandlingFee"]; }
      set { base["StandardHandlingFee"] = value; }
    }

    [ConfigurationProperty("PageSize", DefaultValue = "10")]
    public int PageSize
    {
      get { return (int)base["PageSize"]; }
      set { base["PageSize"] = value; }
    }
  }
}
