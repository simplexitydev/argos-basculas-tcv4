﻿using System.Configuration;

namespace Simplexity.Scales.Domain.Main.Settings
{
  public class CommunicationPort : ConfigurationElement
  {
    [ConfigurationProperty("Port", IsRequired = true)]
    public string Port
    {
      get
      {
        return this["Port"] as string;
      }
    }

    [ConfigurationProperty("BaudRate", IsRequired = true)]
    public string BaudRate
    {
      get
      {
        return this["BaudRate"] as string;
      }
    }

    [ConfigurationProperty("Parity", IsRequired = true)]
    public string Parity
    {
      get
      {
        return this["Parity"] as string;
      }
    }

    [ConfigurationProperty("StopBits", IsRequired = true)]
    public string StopBits
    {
      get
      {
        return this["StopBits"] as string;
      }
    }
    [ConfigurationProperty("DataBits", IsRequired = true)]
    public string DataBits
    {
      get
      {
        return this["DataBits"] as string;
      }
    }
    [ConfigurationProperty("Mode", IsRequired = true)]
    public string Mode
    {
      get
      {
        return this["Mode"] as string;
      }
    }

  }
}
