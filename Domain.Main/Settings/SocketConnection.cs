﻿using System.Configuration;

namespace Simplexity.Scales.Domain.Main.Settings
{
 
  public class SocketConnection : ConfigurationElement
  {
    [ConfigurationProperty("Port", IsRequired = true)]
    public string Port
    {
      get
      {
        return this["Port"] as string;
      }
    }

    [ConfigurationProperty("IpAddress", IsRequired = true)]
    public string IpAddress
    {
      get
      {
        return this["IpAddress"] as string;
      }
    }

  }
}
