﻿using System.Configuration;

namespace Simplexity.Scales.Domain.Main.Settings
{
  public class Scales : ConfigurationElementCollection
  {
    public ScaleElement this[int index]
    {
      get
      {
        return base.BaseGet(index) as ScaleElement;
      }
      set
      {
        if (base.BaseGet(index) != null)
        {
          base.BaseRemoveAt(index);
        }
        this.BaseAdd(index, value);
      }      
    }

    public new ScaleElement this[string responseString]
    {
      get { return (ScaleElement)BaseGet(responseString); }
      set
      {
        if (BaseGet(responseString) != null)
        {
          BaseRemoveAt(BaseIndexOf(BaseGet(responseString)));
        }
        BaseAdd(value);
      }
    }
    protected override System.Configuration.ConfigurationElement CreateNewElement()
    {
      return new ScaleElement();
    }
    protected override object GetElementKey(System.Configuration.ConfigurationElement element)
    {
      return ((ScaleElement)element).Name;
    }
  }
}
