﻿using System.Configuration;

namespace Simplexity.Scales.Domain.Main.Settings
{
  public class ScaleElement : ConfigurationElement
  {
    [ConfigurationProperty("Code", IsRequired = true)]
    public string Code
    {
      get
      {
        return this["Code"] as string;
      }
    }
    [ConfigurationProperty("Name", IsRequired = true)]
    public string Name
    {
      get
      {
        return this["Name"] as string;
      }
    }

  }
}
