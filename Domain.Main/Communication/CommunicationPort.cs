﻿using System;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using Simplexity.Scales.Domain.Main.Constants;

namespace Simplexity.Scales.Domain.Main.Communication
{
  public class CommunicationPort
  {
    #region Enums

    #region Delegates

    public delegate void FrameReceivedEvent(byte[] frame);

    #endregion

    private const int InputStreamBufferSize = 128;
    private readonly SerialPort _comPort;

    /// <summary>
    /// enumeration to hold our transmission types
    /// </summary>

    #endregion

    #region Members

    //property variables
    private int _baudRate;

    private int _dataBits;
    private byte _endByte;
    private string _parity = string.Empty;
    private string _portName = string.Empty;
    private string _stopBits = string.Empty;

    //global manager variables

    private Thread _threadRead;

    public event FrameReceivedEvent FrameReceived;

    #endregion

    #region Manager Properties

    /// <summary>
    /// Property to hold the BaudRate
    /// of our manager class
    /// </summary>
    public int BaudRate
    {
      get { return _baudRate; }
      set { _baudRate = value; }
    }

    /// <summary>
    /// property to hold the Parity
    /// of our manager class
    /// </summary>
    public string Parity
    {
      get { return _parity; }
      set { _parity = value; }
    }

    /// <summary>
    /// property to hold the StopBits
    /// of our manager class
    /// </summary>
    public string StopBits
    {
      get { return _stopBits; }
      set { _stopBits = value; }
    }

    /// <summary>
    /// property to hold the DataBits
    /// of our manager class
    /// </summary>
    public int DataBits
    {
      get { return _dataBits; }
      set { _dataBits = value; }
    }

    /// <summary>
    /// property to hold the PortName
    /// of our manager class
    /// </summary>
    public string PortName
    {
      get { return _portName; }
      set { _portName = value; }
    }

    /// <summary>
    /// property to hold our TransmissionType
    /// of our manager class
    /// </summary>
    public ComConstants.TransmissionType CurrentTransmissionType { get; set; }

    public byte EndByte
    {
      get { return _endByte; }
      set { _endByte = value; }
    }

    #endregion

    #region Manager Constructors

    /// <summary>
    /// Constructor to set the properties of our Manager Class
    /// </summary>
    /// <param portName="baud">Desired BaudRate</param>
    /// <param portName="parity">Desired Parity</param>
    /// <param portName="stopBits">Desired StopBits</param>
    /// <param portName="dataBits">Desired DataBits</param>
    /// <param portName="portName">Desired PortName</param>
    public CommunicationPort(string portName, int baudRate, string parity, string stopBits, int dataBits,
                             ComConstants.TransmissionType tranmissionType)
    {
      _baudRate = baudRate;
      _parity = parity;
      _stopBits = stopBits;
      _dataBits = dataBits;
      _portName = portName;
      CurrentTransmissionType = tranmissionType;
      _comPort = new SerialPort();
      //now add an event handler
      //_comPort.DataReceived += comPort_DataReceived;
    }

    /// <summary>
    /// Comstructor to set the properties of our
    /// serial port communicator to nothing
    /// </summary>
    public CommunicationPort()
    {
      _comPort = new SerialPort();
      //add event handler
      //_comPort.DataReceived += comPort_DataReceived;
    }

    #endregion

    #region WriteData

    public void WriteData(string msg)
    {
      switch (CurrentTransmissionType)
      {
        case ComConstants.TransmissionType.Text:
          //first make sure the port is open
          //if its not open then open it
          if (_comPort.IsOpen != true) _comPort.Open();
          //send the message to the port
          _comPort.Write(msg);
          //display the message
          SetMessage(MessagesConstants.Types.Information, msg);
          break;
        case ComConstants.TransmissionType.Hex:
          try
          {
            //convert the message to byte array
            byte[] newMsg = HexToByte(msg);
            //send the message to the port
            _comPort.Write(newMsg, 0, newMsg.Length);
            //convert back to hex and display
            SetMessage(MessagesConstants.Types.Information, ByteToHex(newMsg));
          }
          catch (FormatException ex)
          {
            //display error message
            SetMessage(MessagesConstants.Types.Error, ex.Message);
          }
          break;
        default:
          //first make sure the port is open
          //if its not open then open it
          if (_comPort.IsOpen != true) _comPort.Open();
          //send the message to the port
          _comPort.Write(msg);
          //display the message
          SetMessage(MessagesConstants.Types.Information, msg);
          break;
      }
    }

    #endregion

    #region HexToByte

    /// <summary>
    /// method to convert hex string into a byte array
    /// </summary>
    /// <param portName="msg">string to convert</param>
    /// <returns>a byte array</returns>
    private byte[] HexToByte(string msg)
    {
      //remove any spaces from the string
      msg = msg.Replace(" ", "");
      //create a byte array the length of the
      //divided by 2 (Hex is 2 characters in length)
      var comBuffer = new byte[msg.Length/2];
      //loop through the length of the provided string
      for (int i = 0; i < msg.Length; i += 2)
        //convert each set of 2 characters to a byte
        //and add to the array
        comBuffer[i/2] = Convert.ToByte(msg.Substring(i, 2), 16);
      //return the array
      return comBuffer;
    }

    #endregion

    #region ByteToHex

    /// <summary>
    /// method to convert a byte array into a hex string
    /// </summary>
    /// <param portName="comByte">byte array to convert</param>
    /// <returns>a hex string</returns>
    private string ByteToHex(byte[] comByte)
    {
      //create a new StringBuilder object
      var builder = new StringBuilder(comByte.Length*3);
      //loop through each byte in the array
      foreach (byte data in comByte)
        //convert the byte to a string and add to the stringbuilder
        builder.Append(Convert.ToString(data, 16).PadLeft(2, '0').PadRight(3, ' '));
      //return the converted value
      return builder.ToString().ToUpper();
    }

    #endregion

    #region SetMessage

    /// <summary>
    /// method to display the data to & from the port
    /// on the screen
    /// </summary>
    /// <param portName="type">MessageType of the message</param>
    /// <param portName="msg">Message to display</param>
    private void SetMessage(MessagesConstants.Types type, string msg)
    {
      //_message.TypeEnum = type;
      //_message.Message = msg;
    }

    #endregion

    #region Connect

    public bool Connect()
    {
      try
      {
        // 1. Check if port really exist 
        string[] avalilablePorts = SerialPort.GetPortNames();

        string portFound = avalilablePorts.FirstOrDefault(p => p == _portName);

        if (portFound == null)
        {
          throw new InvalidOperationException(
            string.Format("Port ({0}) is avalilable or nor configured in local machine.", _portName));
        }

        var data = new byte[InputStreamBufferSize];

        //first check if the port is already open
        //if its open then close it
        if (_comPort.IsOpen) _comPort.Close();

        //set the properties of our SerialPort Object
        _comPort.BaudRate = _baudRate; //BaudRate
        _comPort.DataBits = _dataBits; //DataBits
        _comPort.StopBits = (StopBits) Enum.Parse(typeof (StopBits), _stopBits); //StopBits
        _comPort.Parity = (Parity) Enum.Parse(typeof (Parity), _parity); //Parity
        _comPort.PortName = _portName; //PortName
        //now open the port
        _comPort.Open();

        _threadRead = new Thread(Read) {Priority = ThreadPriority.Highest, Name = "ThreadReadSerialCom"};
        _threadRead.Start();

        return true;
      }
      catch (Exception ex)
      {
        SetMessage(MessagesConstants.Types.Error, ex.Message);
        return false;
      }
    }

    #endregion

    #region comPort_DataReceived

    /// <summary>
    /// method that will be called when theres data waiting in the buffer
    /// </summary>
    /// <param portName="sender"></param>
    /// <param portName="e"></param>
    private void comPort_DataReceived(object sender, SerialDataReceivedEventArgs e)
    {
      //determine the mode the user selected (binary/string)
      switch (CurrentTransmissionType)
      {
          //user chose string
        case ComConstants.TransmissionType.Text:
          //read data waiting in the buffer
          string msg = _comPort.ReadExisting();
          //display the data to the user
          SetMessage(MessagesConstants.Types.Information, msg);
          break;
          //user chose binary
        case ComConstants.TransmissionType.Hex:
          //retrieve number of bytes in the buffer
          int bytes = _comPort.BytesToRead;
          //create a byte array to hold the awaiting data
          var comBuffer = new byte[bytes];
          //read the data and store it
          _comPort.Read(comBuffer, 0, bytes);
          //display the data to the user
          SetMessage(MessagesConstants.Types.Information, ByteToHex(comBuffer));
          break;
        default:
          //read data waiting in the buffer
          string str = _comPort.ReadExisting();
          //display the data to the user
          SetMessage(MessagesConstants.Types.Information, str);
          break;
      }
    }

    #endregion

    private void Read()
    {
      int count = 0;
      int length = 0;

      var data = new byte[InputStreamBufferSize];

      while (!_comPort.IsOpen && count < 10)
      {
        Thread.Sleep(10000); //Esper un tiempo mientras el puerto es abierto.
        count++;
      }
      while (_comPort.IsOpen)
      {
        if (_comPort.BytesToRead < 1)
        {
          Thread.Sleep(1);
          continue;
        }
        while (_comPort.BytesToRead != 0) //Tomamos todos los datos del buffer
        {
          data[length] = (byte) _comPort.ReadByte();
          length += 1;

          if (data[length - 1] == _endByte) // El paquete Contiene el Fin de la Trama.
          {
            var frame = new byte[length];
            Buffer.BlockCopy(data, 0, frame, 0, length);
            Array.Clear(data, 0, data.Length);
            length = 0;

            // ya se tiene una trama. dispare el evento
            if (FrameReceived != null)
            {
              FrameReceived(frame);
            }
          }

          if (length == InputStreamBufferSize)
          {
            //Exepcion La alongitud de la data supero el maximo permitido
            data = new byte[InputStreamBufferSize];
            length = 0;
          }
        }
      }

      //Exepcion puerto cerrado.
    }


    public void Close()
    {
      var metodo = new DelegateClose(CloseAsync);
      metodo.Invoke();
    }

    private void CloseAsync()
    {
      while (_comPort.IsOpen)
      {
        _comPort.Close();
        Thread.Sleep(1000);
      }
    }

    public bool IsOpen()
    {
      return _comPort.IsOpen;
    }

    #region Nested type: DelegateClose

    private delegate void DelegateClose();

    #endregion
  }
}