﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Reactive;
using System.Reactive.Linq;
using System.Linq;

namespace Simplexity.Scales.Domain.Main.Communication
{
  public class SocketConnection
  {
    private TcpClient _tcpClient;
    private const int InputStreamBufferSize = 128;
    private const int ConnectionRetryDelay = 1000;

    public delegate void FrameReceivedEvent(byte[] frame);
    public event FrameReceivedEvent FrameReceived;


    public int Port { get; set; }

    public string IpAddress { get; set; }

    public byte EndByte { get; set; }

    public SocketConnection(string ipAddress, int port)
    {
      IpAddress = ipAddress;
      Port = port;
      
    }

    private void ReadData()
    {
      var buffer = new byte[InputStreamBufferSize];

      IObservable<Stream> streams = Observable.Defer(() =>
      {
        if (_tcpClient != null) _tcpClient.Close();
        _tcpClient = new TcpClient();

        Func<string, int, IObservable<Unit>> connectFactory = Observable.FromAsyncPattern<string, int>(_tcpClient.BeginConnect, _tcpClient.EndConnect);

        var connectionFactory = connectFactory(IpAddress, Port).Select(_ => _tcpClient.GetStream());

        return connectionFactory;

      });

      IObservable<byte[]> rawMessages = streams
          .Select(stream => Observable.FromAsyncPattern<byte[], int, int, int>(stream.BeginRead, stream.EndRead))
          .SelectMany(readFactory => Observable.Defer(() => readFactory(buffer, 0, buffer.Length)).Repeat())
          .Retry(TimeSpan.FromMilliseconds(ConnectionRetryDelay))
          .TakeWhile(length => length != 0)
          .Repeat()
          .SelectMany(buffer.Take)
          .WindowUntil(EndByte)
          .SelectMany(Observable.ToArray);

      rawMessages.Subscribe(FireFrameReceived);

    }

    void FireFrameReceived(byte[] frame)
    {
      if (FrameReceived != null)
      {
        FrameReceived(frame);
      }
    }

    public void Connect()
    {
      ReadData();
    }

    public void Disconnect()
    {
      if (_tcpClient!=null)
      {
        //_tcpClient.Client.Disconnect(false);
        _tcpClient.GetStream().Close();
        _tcpClient.Close();
      }
    }


    public bool IsConnected()
    {
      try
      {
        var par1 = _tcpClient.Client.Poll(0, SelectMode.SelectRead);
        var part2 = (_tcpClient.Available == 0);

        var result = !(par1 && part2);
        return result;
      }
      catch (Exception)
      {
        return false;
      }

    }

    public bool IsConnectedToServer()
    {
      try
      {
        return _tcpClient.Connected;
      }
      catch (Exception)
      {
        return false;
      }
    }

    public bool IsReceivingDataFromServer()
    {
      try
      {
        var par1 = _tcpClient.Client.Poll(0, SelectMode.SelectRead);
        var par2 = _tcpClient.Available != 0;

        var result = (par1 && par2);
        return result;
      }
      catch (Exception)
      {
        return false;
      }
    }


  }

  public static class ObservableExtensions
  {
    public static IObservable<IObservable<T>> WindowUntil<T>(this IObservable<T> source, T separator) where T : IEquatable<T>
    {
      IObservable<T> src = source.Publish().RefCount();
      return src.Window(src.Where(it => it.Equals(separator)));
    }

    public static IObservable<T> Retry<T>(this IObservable<T> source, TimeSpan delay)
    {
      return source
          .Catch<T, Exception>(ex => Observable.Empty<T>().Delay(delay).Concat(Observable.Throw<T>(ex)))
          .Retry();
    }
  }

}