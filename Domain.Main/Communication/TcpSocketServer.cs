﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using System.Net.Sockets;
using System.Text;

namespace Simplexity.Scales.Domain.Main.Communication
{
    public class TcpSocketServer
    {
        private readonly List<Socket> _listaSockets = new List<Socket>();
        private Socket _objSocketServidor;
        private readonly byte[] _bytesDatos = new byte[2048];

        #region "EVENTOS"
        public delegate void EventoDatosRecibidos(string txtIpCliente, int puertoCliente, string txtDatosRecibidos);
        /// <summary>
        /// Evento que recoge los datos recibidos
        /// </summary>
        public event EventoDatosRecibidos DatosRecibidos;

        public delegate void EventoDatosEnviados(string txtIpCliente, int puertoCliente, string txtDatosEnviados);
        /// <summary>
        /// Evento que recoge los datos enviados
        /// </summary>
        public event EventoDatosEnviados DatosEnviados;

        public delegate void EventoNuevaConexion(string txtIpCliente, int puertoCliente);
        /// <summary>
        /// Evento que se lanzara cuando se conecte un nuevo cliente
        /// </summary>
        public event EventoNuevaConexion NuevaConexion;

        public delegate void EventoFinConexion(string txtIpCliente, int puertoCliente);
        /// <summary>
        /// Evento que se lanzara cuando se cierre una conexion
        /// </summary>
        public event EventoFinConexion FinConexion;

        public delegate void EventoError(string txtMetodo, string txtError);
        public event EventoError DatosErrores;
        #endregion

        #region "CONSTRUCTORES"

        /// <summary>
        /// Constructor sobrecargado
        /// </summary>
        /// <param name="puertoEscucha">Puerto en el que escuchara el servidor</param>
        public TcpSocketServer(int puertoEscucha)
        {
            ServidorActivo = false;
            if (puertoEscucha < 1280)
                throw new Exception("El puerto no puede ser menor de 1280");

            PuertoEscucha = puertoEscucha;
            IpEscucha = "127.0.0.1";    

       
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="puertoEscucha">Puerto en el que escuchara el servidor</param>
        /// <param name="ipEscucha">Direccion IP en el que escuchara el servidor</param>
        public TcpSocketServer(int puertoEscucha, string ipEscucha)
        {
            ServidorActivo = false;
            if (puertoEscucha < 1280)
                throw new Exception("El puerto no puede ser menor de 1280");

            PuertoEscucha = puertoEscucha;
            IpEscucha = ipEscucha;
        }

        public TcpSocketServer()
        {
            ServidorActivo = false;
            IpEscucha = string.Empty;
        }

        #endregion

        #region "PROPIEDADES"
        /// <summary>
        /// Puerto donde escucha el servidor
        /// </summary>
        public int PuertoEscucha { get; set; }
        /// <summary>
        /// IpAddress donde esta presente el servidor
        /// </summary>
        public string IpEscucha { get; set; }
        /// <summary>
        /// Indica si el servidor esta activo
        /// </summary>
        public bool ServidorActivo { get; private set; }
        #endregion

        #region "METODOS"
        /// <summary>
        /// Metodo que inicia la escucha del servidor
        /// </summary>
        public void IniciarEscucha()
        {
            if (PuertoEscucha < 1280)
                throw new Exception("El puerto no puede ser menor de 1280");
            _objSocketServidor = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

            _objSocketServidor.Bind(IpEscucha != string.Empty
                                        ? new IPEndPoint(IPAddress.Parse(IpEscucha), PuertoEscucha)
                                        : new IPEndPoint(IPAddress.Any, PuertoEscucha));

            _objSocketServidor.Listen(10);
            _objSocketServidor.BeginAccept(ConexionRealizada, null);
            ServidorActivo = true;
        }
        private void ConexionRealizada(IAsyncResult iAr)
        {
            var objSocket = _objSocketServidor.EndAccept(iAr);
            try
            {
                _listaSockets.Add(objSocket);
                _objSocketServidor.BeginAccept(ConexionRealizada, null);
                //objSocket.BeginReceive(_bytesDatos, 0, _bytesDatos.Length, SocketFlags.None, DatosLlegados, objSocket);

                if (NuevaConexion != null)
                {
                    var ipClienteConectado = ((IPEndPoint)objSocket.RemoteEndPoint).Address.ToString();
                    var puertoClienteConectado = ((IPEndPoint)objSocket.RemoteEndPoint).Port;
                    NuevaConexion(ipClienteConectado, puertoClienteConectado);
                }
            }
            catch (Exception ex)
            {
                if (DatosErrores != null)
                    DatosErrores(MethodBase.GetCurrentMethod().Name, ex.ToString());
                EliminarCliente(objSocket);
            }
        }
        private void DatosLlegados(IAsyncResult iAr)
        {
            var objSocket = (Socket)iAr.AsyncState;
            try
            {
                var tamanioDatosRecibidos = objSocket.EndReceive(iAr);
                if (tamanioDatosRecibidos > 0)
                {
                    var bytesDatosReales = new byte[tamanioDatosRecibidos];
                    Array.Copy(_bytesDatos, bytesDatosReales, tamanioDatosRecibidos);
                    var txtDatosReales = Encoding.ASCII.GetString(bytesDatosReales);

                    //EnviarDatosTodos(bytesDatosReales);

                    if (DatosRecibidos != null)
                    {
                        var ipClienteConectado = ((IPEndPoint)objSocket.RemoteEndPoint).Address.ToString();
                        var puertoClienteConectado = ((IPEndPoint)objSocket.RemoteEndPoint).Port;
                        DatosRecibidos(ipClienteConectado, puertoClienteConectado, txtDatosReales);
                    }

                    objSocket.BeginReceive(_bytesDatos, 0, _bytesDatos.Length, SocketFlags.None, DatosLlegados, objSocket);
                }
                else
                {
                    EliminarCliente(objSocket);
                }
            }
            catch (Exception ex)
            {
                if (DatosErrores != null)
                    DatosErrores(MethodBase.GetCurrentMethod().Name, ex.ToString());
                EliminarCliente(objSocket);
            }
        }
        private void EliminarCliente(Socket objSocket)
        {
            if (FinConexion != null)
            {
                var ipClienteConectado = ((IPEndPoint)objSocket.RemoteEndPoint).Address.ToString();
                var puertoClienteConectado = ((IPEndPoint)objSocket.RemoteEndPoint).Port;
                FinConexion(ipClienteConectado, puertoClienteConectado);
            }
            objSocket.Close();
            _listaSockets.Remove(objSocket);
        }
        public void EnviarDatosTodos(string mensaje)
        {
            try
            {
                foreach (var objSocket in _listaSockets)
                {
                    EnviarDatos(objSocket, Encoding.ASCII.GetBytes(mensaje));
                }
            }
            catch (Exception ex)
            {
                if (DatosErrores != null)
                    DatosErrores(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
        }
        private void EnviarDatos(Socket objSocket, byte[] bytesMensaje)
        {
            try
            {
                if (!objSocket.Connected)
                {
                    if (DatosErrores != null)
                        DatosErrores(MethodBase.GetCurrentMethod().Name, "El cliente se encuentra desconectado");
                    EliminarCliente(objSocket);
                    return;
                }
                objSocket.BeginSend(bytesMensaje, 0, bytesMensaje.Length, SocketFlags.None, DatosEnviadosAr, new Object[] { objSocket, bytesMensaje });
            }
            catch (Exception ex)
            {

                if (DatosErrores != null)
                    DatosErrores(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
         }
        private void DatosEnviadosAr(IAsyncResult iAr)
        {
            try
            {
                var objDatos = (Object[])iAr.AsyncState;
                var objSocket = (Socket)objDatos[0];
                var bytesEnviados = (byte[])objDatos[1];

                objSocket.EndSend(iAr);

                var ipClienteConectado = ((IPEndPoint)objSocket.RemoteEndPoint).Address.ToString();
                var puertoClienteConectado = ((IPEndPoint)objSocket.RemoteEndPoint).Port;
                var txtDatosEnviados = Encoding.ASCII.GetString(bytesEnviados);


                if (DatosEnviados != null)
                    DatosEnviados(ipClienteConectado, puertoClienteConectado, txtDatosEnviados);
            }
            catch (Exception ex)
            {
                if (DatosErrores != null)
                    DatosErrores(MethodBase.GetCurrentMethod().Name, ex.ToString());
            }
        }
        public void Cerrar()
        {
            foreach (var objSocket in _listaSockets)
            {
                EliminarCliente(objSocket);
            }
            _objSocketServidor.Close();
        }
        #endregion
    }

}
