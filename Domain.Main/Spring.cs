﻿using Spring.Context;
using Spring.Context.Support;

namespace Simplexity.Scales.Domain.Main
{
  public static class Spring
  {
    public static object GetObject(string objectName, string context)
    {
      //IApplicationContext ctx1 = new XmlApplicationContext("file://SpringConfiguration.xml");
      IApplicationContext ctx = ContextRegistry.GetContext();
      return ctx.GetObject(objectName);
    }
  }
}
