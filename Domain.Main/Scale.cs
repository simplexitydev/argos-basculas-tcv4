﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simplexity.Scales.Domain.Main.Communication;
using Simplexity.Scales.Domain.Main.Constants;
using Simplexity.Scales.Domain.Main.Protocols;

namespace Simplexity.Scales.Domain.Main
{

  public class Scale
  {
    #region Members

    private double _grossWeight;
    private double _netWeight;
    private double _tareWeight;
    private IDecryptable _protocol;
   
    private double _weightVariation; 
    
    private Queue<Frame> _frameList;
    private string _name;
    private bool _enabled;
    private string _code;
    private string _currentProtocol;
    private ScaleConstants.ComunicationMode _comunicationMode;
    private const int MaxSecondsWithoutNewFrames = 10;

    #endregion

    #region Properties

    public string Name
    {
      get { return _name; }
      set
      {
        if (_name != value)
        {
          _name = value;
        }
      }
    }

    public bool Enabled
    {
      get { return _enabled; }
      set { _enabled = value; }
    }

    public string Code
    {
      get { return _code; }
      set { _code = value; }
    }

    public double GrossWeight
    {
      get { return _grossWeight; }
    }

    public double NetWeight
    {
      get { return _netWeight; }
    }

    public double TareWeight
    {
      get { return _tareWeight; }
    }

    public List<string> AvailableProtocols { get; set; }
    
    public string CurrentProtocol
    {
      get { return _currentProtocol; }
      set { _currentProtocol = value; }
    }

    public ScaleConstants.State State { get; set; }
    
    public ScaleConstants.ComunicationMode ComunicationMode
    {
      get { return _comunicationMode; }
      set { _comunicationMode = value; }
    }

    public CommunicationPort CommunicationPort { get; set; }
    public SocketConnection SocketConnection { get; set; }

    public IDecryptable Protocol
    {
      get { return _protocol; }
      set { _protocol = value; }
    }

    public double WeightVariation
    {
      get { return _weightVariation; }
      set { _weightVariation = value; }
    }

    public bool IsConnected
    {
      get
      {
        switch (_comunicationMode)
        {
          case ScaleConstants.ComunicationMode.Com:
            return CommunicationPort.IsOpen();

          case ScaleConstants.ComunicationMode.Socket:
            return SocketConnection.IsConnected();
        }

        return false;
      }
    }

    #endregion

    #region Delegates

    public event OnFrameReceivedErrorHandler FrameReceivedError;
    public delegate void OnFrameReceivedErrorHandler(object sender);

    #endregion

    #region Ctor

    public Scale(string code, string name, string currentProtocol, ScaleConstants.ComunicationMode communicationMode, int weightWeightVariation, bool enabled)
    {
      _code = code;
      _name = name;
      _currentProtocol = currentProtocol;
      _comunicationMode = communicationMode;
      _weightVariation = weightWeightVariation;
      _enabled = enabled;
      _frameList = new Queue<Frame>();
      if (_enabled)
      {
        _protocol = (IDecryptable)Spring.GetObject(_currentProtocol, ScaleConstants.SpringContext);
      }
    }
    #endregion

    #region Public Methods
    public void AddComPort(string portName, int baudRate, string parity, string stopBits, int dataBits, ComConstants.TransmissionType tranmissionType)
    {
      CommunicationPort = new CommunicationPort(portName, baudRate, parity, stopBits,  dataBits, tranmissionType);
      
      if (!_enabled) return;
      
      CommunicationPort.EndByte = _protocol.EndByte;
      CommunicationPort.FrameReceived += FrameReceived;

    }

    public void AddSocket(string ipAddress, int port)
    {
      SocketConnection = new SocketConnection(ipAddress, port);
      
      if (!_enabled) return;

      SocketConnection.EndByte = _protocol.EndByte;
      SocketConnection.FrameReceived += FrameReceived;
    }

    public void Connect()
    {
      switch (_comunicationMode)
      {
        case ScaleConstants.ComunicationMode.Com:
          CommunicationPort.Connect();

          break;
        case ScaleConstants.ComunicationMode.Socket:
          SocketConnection.Connect();

          break;
      }
    }

    public void Disconnect()
    {
      switch (_comunicationMode)
      {
        case ScaleConstants.ComunicationMode.Socket:
          SocketConnection.Disconnect();

          break;
      }
    }


    public double GetWeight()
    {
      double weight = 0;

      if (!Enabled)
      {
        State = ScaleConstants.State.Disabled;
        _netWeight = weight;
        return weight;
      }

      // Check if scales is Conected
      if (ComunicationMode == ScaleConstants.ComunicationMode.Socket)
      {
        if (!SocketConnection.IsConnectedToServer())
        {
          throw new InvalidOperationException(string.Format("Scale {0} is not connected to socket at {1}:{2}. This error is due to a connection loss. Please, try again in a moment. If the error persist contact your administrator.", Code, SocketConnection.IpAddress, SocketConnection.Port));
        }

        if (!SocketConnection.IsConnected())
        {
          throw new InvalidOperationException(string.Format("Scale {0} is connected to socket at {1}:{2} but no data in being received. Please, try again in a moment. If the error persist contact your administrator.", Code, SocketConnection.IpAddress, SocketConnection.Port));
        }

      }

      // Check if frames received in MaxSecondsWithoutNewFrames 10 seconds. 
      var now = DateTime.Now;
      if (_frameList.All(frameItem => (new TimeSpan(now.Ticks - frameItem.DateTime.Ticks).TotalSeconds) > MaxSecondsWithoutNewFrames))
      {
        throw new InvalidOperationException(String.Format("No data received in last {0} seconds. Please, try again in a moment. If the error persist contact your administrator.", MaxSecondsWithoutNewFrames));
      }
      
      // No frames
      if (_frameList == null || _frameList.Count == 0)
      {
        throw new InvalidOperationException("No frames are registered. Please, try again in a moment. If the error persist contact your administrator.");
      }

      // No minimum frames (10)
      // Unstable
      // Return last weight in queue
      if (_frameList.Count < 10)
      {
        State = ScaleConstants.State.Unstable;
        _netWeight = _frameList.Last().Weight;
        return _netWeight;
      }

      var averageWeight = _frameList.Sum(frame => frame.Weight) / _frameList.Count; ;

      // All weights average < 0
      // Unstable
      // Return 0
      if (averageWeight < 0)
      {
        State = ScaleConstants.State.Unstable;
        _netWeight = weight;
        return weight;
      }

      var limitUp = _frameList.Last().Weight * (1 + (_weightVariation / 100));
      var limitDown = _frameList.Last().Weight * (1 - (_weightVariation / 100));

      // Weight under variation tolerance
      // Stable
      // Return last Weight
      if (averageWeight <= limitUp && averageWeight >= limitDown)
      {
        State = ScaleConstants.State.Stable;
        // Alexander Bautista: anteriomente estaba david devolviendo el promedio. Lo cambié para que devuelva el último si
        // lo parametros son correctos
        //_netWeight = averageWeight;
        _netWeight = _frameList.Last().Weight;

        return _netWeight;
      }
      // In Any other case
      // Unstable
      // Return LastWeight
      State = ScaleConstants.State.Unstable;
      _netWeight = _frameList.Last().Weight;
      return _netWeight;

    }
    #endregion

    #region Handler Methods
    void FrameReceived(byte[] rawFrame)
    {
      Frame frame = null;

      try
      {
        frame = _protocol.Decrypt(rawFrame);
      }
      catch (Exception ex)
      {

        if (FrameReceivedError != null)
        {
          FrameReceivedError(ex);
        }
      }


      if (frame == null)
        return;

      if (_frameList == null)
        _frameList = new Queue<Frame>();

      if (_frameList.Count == 10)
      {
        _frameList.Dequeue();
      }

      frame.DateTime = DateTime.Now;
      _frameList.Enqueue(frame);
    }

    #endregion

    #region Private Methods
    private void ClearFrames()
    {
      if (_frameList !=null && _frameList.Count > 0)
      {
        _frameList.Clear();
      }
    }
    #endregion

  }
}